<?php
ob_start();
$template = new TemplateForm();

?>


	<div id="content-center">
		<div id="who-we-are" class="content-wrapper">
			<div class="row">
				<div class="col-sm-6 wow fadeInUp">
					<h1 class="page-header"><?php echo $template->content('title_who_block');?></h1>
					<?php echo $template->content('content_who_block');?>
					<a href="<?php echo $template->content('link_who_block');?>" class="btn btn-default more"><?php echo $template->content('button_who_block');?></a>
				</div>
				<div class="col-sm-6 wow fadeInUp"><img src="<?php echo $template->image_url('image_who_block');?>"></div>
			</div>
		</div>
	
		<div id="catalog-home" class="content-wrapper">
				<div class="row wow fadeInUp">
					<div class="col-sm-12"><h1 class="page-header"><?php echo $template->content('title_catalog_block');?></h1></div>
					<div class="col-sm-12">
					<div id="slide-catalog">	
					
					<?php
					$blog_news=$template->getModulData(
					$modul_table="catalogdata",
					$filter_var=array(),
					$filter_val=array(),//disini value untuk filter
					$field="id,filename,title,publish,isbest,harganormal",
					$path_images="catalog/small",
					$limit=$template->content('product_limit'),
					$order="id ASC",
					$kondisi_tambahan="isbest=1"
					);
					
					foreach($blog_news as $i => $v){
					list($filename)=explode(":",$v['filename']);
					$titleurl = array();
					$titleurl["pid"] = $v['title'];
					
				
					?>				
					<div class="col-sm-4">
						<div class="product-col">
							<div class="image-product">
								<a href="<?php echo $urlfunc->makePretty("?p=catalog&action=detail&pid=".$v['id'], $titleurl);?>">
									<img src="<?php echo "$cfg_app_url/file/catalog/large/".$filename;?>" alt="">
								</a>
							</div>
							<div class="caption">
							<div class="title-product">
								<h4>
									<a href="<?php echo $urlfunc->makePretty("?p=catalog&action=detail&pid=".$v['id'], $titleurl);?>"><?php echo $v['title'];?></a>
								</h4>
							</div>
							<div class="price"><span class="price-now">Rp <?php echo number_format($v['harganormal'], 0, ',', '.');?></span></div>
							</div>
						</div>
					</div>
					<?php
					}
					?>
					</div>
					</div>
				</div>
		</div>	
	</div>


<?php
$display_main_content_block .= ob_get_clean();
?>
