<?php

$data["header"]=array(
		"logo"=>array("label"=>"Logo","type"=>"inputImage"),
);

$data["slide1"]=array(
		"slide1_img"=>array("label"=>"Slide 1","type"=>"inputImage"),
		"slidemobile1_img"=>array("label"=>"Slide Mobile 1","type"=>"inputImage"),
);
$data["slide2"]=array(
		"slide2_img"=>array("label"=>"Slide 2","type"=>"inputImage"),
		"slidemobile2_img"=>array("label"=>"Slide Mobile 2","type"=>"inputImage"),
);
$data["slide3"]=array(
		"slide3_img"=>array("label"=>"Slide 3","type"=>"inputImage"),
		"slidemobile3_img"=>array("label"=>"Slide Mobile 3","type"=>"inputImage"),
);
$data["slide4"]=array(
		"slide4_img"=>array("label"=>"Slide 4","type"=>"inputImage"),
		"slidemobile4_img"=>array("label"=>"Slide Mobile 4","type"=>"inputImage"),
);

$data["Who We Are"]=array(
		"bg_who_block"=>array("label"=>"Background","type"=>"inputImage"),
		"image_who_block"=>array("label"=>"Image","type"=>"inputImage"),
		"title_who_block"=>array("label"=>"Title","type"=>"inputText"),
		"content_who_block"=>array("label"=>"Content","type"=>"inputTiny"),
		"button_who_block"=>array("label"=>"Teks Button","type"=>"inputText"),
		"link_who_block"=>array("label"=>"Link Url","type"=>"inputText"),
);
$data["Produk"]=array(
		"title_catalog_block"=>array("label"=>"Title","type"=>"inputText"),
		"product_limit"=>array("label"=>"Limit","type"=>"inputText"),
);
$data["Brands"]=array(
		"title_brands_block"=>array("label"=>"Title","type"=>"inputText"),
		"content_brands_block"=>array("label"=>"Content","type"=>"inputTiny"),
);

$data["Footer"]=array(
		"bg_footerlink"=>array("label"=>"Background","type"=>"inputImage"),
		"bg_footerlink_mobile"=>array("label"=>"Background mobile","type"=>"inputImage"),
		"footerlink_1"=>array("label"=>"Footer 1","type"=>"inputTiny"),
		"footerlink_2"=>array("label"=>"Footer 2","type"=>"inputTiny"),
		"footerlink_3"=>array("label"=>"Footer 3","type"=>"inputTiny"),
		"copyright"=>array("label"=>"Copyright ","type"=>"inputTiny"),
);

$data["Mobile Link"]=array(
		"phone_number"=>array("label"=>"Telpon","type"=>"inputText"),
		"wa_number"=>array("label"=>"WA","type"=>"inputText"),
		"email_link"=>array("label"=>"Email","type"=>"inputText"),
		"map_link"=>array("label"=>"Map link","type"=>"inputText"),
		"line_link"=>array("label"=>"Line","type"=>"inputText"),
		"bbm_link"=>array("label"=>"BBM","type"=>"inputText"),
);

?>
