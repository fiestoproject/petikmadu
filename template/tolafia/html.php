<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $config_site_metadescription;?>">
    <meta name="keyword" content="<?php echo $config_site_metakeyword;?>">
    <meta name="author" content="fiesto.com">
	<link href="<?php echo $favicon ?>" rel="SHORTCUT ICON" />

    <title><?php echo $config_site_titletag;?></title>
	
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300&display=swap" rel="stylesheet">
	<?php echo $style_css ?>
    <!-- Bootstrap Core CSS -->
    <link href="<<<TEMPLATE_URL>>>/css/bootstrap.min.css" rel="stylesheet">
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">-->
    <!-- Custom CSS -->
	<link type="text/css" rel="stylesheet" href="<<<TEMPLATE_URL>>>/font-awesome/css/font-awesome.min.css">
    <link href="<<<TEMPLATE_URL>>>/css/basic.css?v=1.6" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/animate.css" rel="stylesheet">
	<link href="<<<TEMPLATE_URL>>>/css/owl.carousel.min.css" rel="stylesheet"/>
    <link href="<<<TEMPLATE_URL>>>/css/owl.theme.default.css" rel="stylesheet"/>
    <link href="<<<TEMPLATE_URL>>>/css/owl.transitions.css" rel="stylesheet"/>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php
$template = new TemplateForm();
?>
<style>

<?php if(!$_GET['p']) {?>
	
<?php } else {?>
h1.page-header{font-size:27pt}
.content-wrapper {
    padding: 50px 15px 100px;
}

@media(min-width:768px){
}
@media(max-width:767px){
	nav#header-site {padding-bottom:0;}
	h1.page-header {
		font-size: 22pt !important;
		margin-bottom: 10px;
	}
}
<?php } ?>

<?php if ($_GET['p'] == 'catalog' && ($_GET['action'] == 'detail')) {?>
div#content-center {
    position: relative;
}
	#description_product > table > tbody > tr > td,#description_product > table > tbody > tr,#description_product > table > tbody,#description_product > table {display:block}
	#description_product > table > tbody > tr > td {padding-top:10px;padding-bottom:0px}
	#description_product > table:first-child{background:transparent}
	#description_product > table:first-child > tbody > tr > td{padding:0;}
	#description_product > table > tbody{margin:0 auto;padding:0 15px}
@media (max-width: 767px){
	#description_product > table > tbody {
		padding: 0 5px;
	}
}
<?php } ?>
</style>
</head>

<body>
<div id="fixed">

<?php
// $template = new TemplateForm();
include "header.php"; ?>

<div id="slide-home" class="owl-carousel owl-theme">	
	<div class="item">
		<img class="mobile-hidden" src="<?php echo $template->image_url('slide1_img');?>" alt="Slide Image">
		<img class="dekstop-hidden" src="<?php echo $template->image_url('slidemobile1_img');?>" alt="Slide Image">
	</div>
	<div class="item">
		<img class="mobile-hidden" src="<?php echo $template->image_url('slide2_img');?>" alt="Slide Image">
		<img class="dekstop-hidden" src="<?php echo $template->image_url('slidemobile2_img');?>" alt="Slide Image">
	</div>
	<div class="item">
		<img class="mobile-hidden" src="<?php echo $template->image_url('slide3_img');?>" alt="Slide Image">
		<img class="dekstop-hidden" src="<?php echo $template->image_url('slidemobile3_img');?>" alt="Slide Image">
	</div>
	<div class="item">
		<img class="mobile-hidden" src="<?php echo $template->image_url('slide4_img');?>" alt="Slide Image">
		<img class="dekstop-hidden" src="<?php echo $template->image_url('slidemobile4_img');?>" alt="Slide Image">
	</div>
</div>
<?php
echo '<div class="container">';
echo'<div class="col-md-9">';
if(!$_GET['p']) {
	
} else {
	 echo '<div id="content-center">';
}
echo $display_main_content_block;
if(!$_GET['p']) {
	
} else {
	 echo '</div>';
}
echo '</div>';
echo'<div class="col-md-3 widget-right">
		<div class="row">';
echo $display_widget_A;
echo'	</div>
	</div>';
echo '</div>';
include "footer.php";
?>	
</div>
    <script src="<<<TEMPLATE_URL>>>/js/jquery.min.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/bootstrap.min.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/owl.carousel.min.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/wow.js"></script>
	<?php
	echo $script_js;
	?>
	<script>
		$(function() {
			var sync1 = $("#sync1");
			  var sync2 = $("#sync2");

			  sync1.owlCarousel({
				items:1 ,
				slideSpeed : 1000,
				navigation: true,
				pagination:false,
				afterAction : syncPosition,
				responsiveRefreshRate : 200,
			  });

			  sync2.owlCarousel({
				items : 9,
				itemsDesktop      : [1199,9],
				itemsDesktopSmall     : [979,9],
				itemsTablet       : [768,7],
				itemsMobile       : [479,7],
				pagination:false,
				responsiveRefreshRate : 100,
				afterInit : function(el){
				  el.find(".owl-item").eq(0).addClass("synced");
				}
			  });

			  function syncPosition(el){
				var current = this.currentItem;
				$("#sync2")
				  .find(".owl-item")
				  .removeClass("synced")
				  .eq(current)
				  .addClass("synced")
				if($("#sync2").data("owlCarousel") !== undefined){
				  center(current)
				}

			  }

			  $("#sync2").on("click", ".owl-item", function(e){
				e.preventDefault();
				var number = $(this).data("owlItem");
				sync1.trigger("owl.goTo",number);
			  });

			  function center(number){
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

				var num = number;
				var found = false;
				for(var i in sync2visible){
				  if(num === sync2visible[i]){
					var found = true;
				  }
				}

				if(found===false){
				  if(num>sync2visible[sync2visible.length-1]){
					sync2.trigger("owl.goTo", num - sync2visible.length+2)
				  }else{
					if(num - 1 === -1){
					  num = 0;
					}
					sync2.trigger("owl.goTo", num);
				  }
				} else if(num === sync2visible[sync2visible.length-1]){
				  sync2.trigger("owl.goTo", sync2visible[1])
				} else if(num === sync2visible[0]){
				  sync2.trigger("owl.goTo", num-1)
				}
			  }
			$('#slide-home').owlCarousel({
				loop:true,
				margin:0,
				nav:true,
				dots:false,
				autoplay:true,
				items:1,
				navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
			})
			$('#close-menu').click( function() {
				$(".navbar-collapse.in").removeClass("in");
				$("html").removeClass("menu-active");
			} );
			$('.navbar-toggle').click( function() {
				$("html").addClass("menu-active");
			} );
			$('.bg-black').click( function() {
				$(".navbar-collapse.in").removeClass("in");
				$("html").removeClass("menu-active");
			} );
			$('.navbar-inverse .navbar-toggle').click( function() {
				$("html").addClass("menu-active");
			} );
			$('#content-blocker').click( function() {
				$("html").removeClass("menu-active");
			} );
		});
		wow = new WOW(
		  {
			animateClass: 'animated',
			offset:       100,
			callback:     function(box) {
			  console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
			}
		  }
		);
		wow.init();
	</script>

<?php echo $config_site_gacode; ?>
</body>

</html>
