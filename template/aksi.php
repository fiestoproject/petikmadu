<?php

use PDFMerger\PDFMerger;
//$_REQUEST tak ganti $_GET karena nga-bug seperti kemarin
$action = fiestolaundry($_GET['action'],30);
$txid = fiestolaundry($_GET['pid'],32);

//die($action);
$sql = "SELECT judulfrontend FROM module WHERE nama='order'";
$result = $mysqli->query($sql);
list($title) = $result->fetch_row();

$txid = base64_decode($txid);
$status_quo = fiestolaundry($_GET['status_quo'], 1);

$get_member_parent = get_member_parent($_SESSION['member_uid']);

if ($action == '' || $action=='view') {
	
	$sql = "SELECT tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher,kurir FROM sc_transaction WHERE transaction_id='$txid'";
	$result = $mysqli->query($sql);
	list($tanggal_buka_transaksi, $tanggal_tutup_transaksi, $customer_id, $receiver_id, $metode_pembayaran, $shipping, $status, $catatan, $no_pengiriman, $latestupdate, $kodevoucher, $tipe_voucher, $nominal_voucher,$kurir) = $result->fetch_row();
	
	$couriers 		= json_decode($kurir, true);
	$kecamatan 		= $couriers['kota'];
	$kurir 			= $couriers['tipe'] . ' ' . $couriers['kota'];
	$kurir_tipe		= $couriers['tipe'];
	$ongkos_kirim 	= number_format($shipping, 0, ',', '.');
	$estimasi 		= $couriers['estimasi'];
	
	$content .= "<h2>"._ORDERSTATUS."</h2>\r\n";
	$content .= "<p>";
	switch ($status) {
		case 0:
		$content .= _STATUS0;
		break;
		case 1:
		$content .= _STATUS1;
		break;
		case 2:
		$content .= _STATUS2;
		break;
		case 3:
		$content .= _STATUS3;
		break;
		case 4:
		$content .= _STATUS4;
		break;
		case 5:
		$content .= _STATUS5." $no_pengiriman";
		break;
	}
	// $content .= " ("._LATESTUPDATE.": ".tglformat($latestupdate,true).")</p>\r\n";
	
	$content .= "<h2>"._ORDERDETAILS."</h2>\r\n";
	$content .= "<table border=\"0\">\r\n";
	$content .= "<tr><td>"._CURRENTORDERTITLE."</td><td>:</td><td>$txid</td></tr>\r\n";
	$content .= "<tr><td>"._ORDERDATE."</td><td>:</td><td>".tglformat($tanggal_buka_transaksi,true)."</td></tr>\r\n";
	$content .= "</table><br />\r\n";				
	$content .= "<div id=\"checkout-form\">\r\n";				
	$content .= "<div class=\"table_block table-responsive table-small-gap\">\r\n";				
	$content .= "<table border=\"0\" cellpadding=\"3\" class=\"table table-bordered\" width=\"100%\">"; //format the cart using a HTML table
	//iterate through the cart, the $product_id is the key and $quantity is the value
	$content .= "<thead><tr style=\"background: #eee;font-weight: bold;\">";
	$content .= "<th>"._THUMBNAIL."</th>";
	$content .= "<th>"._PRODUCTNAME."</th>";
	$content .= "<th>"._PRICE."</th>";
	$content .= "<th>"._DISCOUNT."</th>";
	$content .= "<th>"._QUANTITY."</th>";
	$content .= "<th>"._LINECOST."</th>";
	$content .= "</tr></thead>";
	$totalweight = 0;
	
	// $sql = "SELECT jumlah, harga, product_name,filename FROM sc_transaction_detail WHERE transaction_id='$txid'";
	$sql = "SELECT jumlah,td.harga,product_name,td.filename, c.idmerek, c.keterangan, c.sku, c.matauang, td.harganormal, td.diskon FROM sc_transaction_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE transaction_id='$txid' ORDER BY transaction_detail_id";
	$result = $mysqli->query($sql);
	while (list($quantity, $price, $productname,$filename, $idmerek, $keterangan, $product_code, $matauang, $harganormal, $diskon) = $result->fetch_row()) {
		$brand_name = get_brand_name($idmerek);
		$line_cost = $quantity * $price;  //work out the line cost
		$total = $total + $line_cost;   //add to the total cost
		$content .= "<tr>";
		$content .= "<td>";
		if(file_exists("$cfg_thumb_path/$filename")) {
			$content.="<img src=\"$cfg_thumb_url/$filename\" width=\"50px\" class=\"thumb_small\" >";
		} else {
			$content.="<img src=\"$cfg_app_url/images/none.gif\" width=\"50px\" class=\"thumb_small\" >";
		}
		$content .="</td>";
		$content .= "<td><p class=\"product-name\">$brand_name $product_code</p><small class=\"description-product\">$productname</small></td>";
		$content .= "<td align=\"right\">".number_format($harganormal, 0, ',', '.')."</td>";
		$content .= '<td class="cart_unit" align="right">';
		if ($diskon != '') {
			if (substr_count($diskon, "%") == 1) {
				$label_disc = $diskon;
			} else {
				$label_disc = number_format($diskon, 0, ',', '.');
			}
			$content .= '<span class="price-nowx">'. $label_disc .'</span>';
		}
		// $content .= '</span>';
		$content .= '</td>';
		/* $content .= "<td align=\"right\">";
		if ($diskon != '') {
			$content .= '<div class="price-old" id="tHarga">'.$matauang.' '.number_format($harganormal, 0, ',', '.').'</div>';
			$content .= '<div class="price-now" id="tHargaDiskon">'.$matauang.' '.number_format($price, 0, ',', '.').'</div>';

			if (substr_count($diskon, "%") == 1) {
				$label_disc = $diskon;
			} else {
				$label_disc = $matauang.' '.number_format($diskon, 0, ',', '.');
			}
			$content .= '<span class="reduction"><span>-'.$label_disc.'</span></span>';
		} else {
			$content .= '<span class="price-now" id="tHarga">'.$matauang.' '.number_format($harganormal, 0, ',', '.').'</span>';
		}
		$content .= "</td>"; */
		$content .= "<td align=\"center\">".number_format($quantity, 0, ',', '.')."</td>";
		$content .= "<td align=\"right\">".number_format($line_cost, 0, ',', '.')."</td>";
		$content .= "</tr>";
	}
	
	if ($pakaiadditional || $pakaiongkir || $pakaivoucher) {
		$content .= "<tr>";
		$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;border-top:1px solid #ddd\">"._SUBTOTAL."</td>";
		$content .= "<td align=\"right\" style=\"border-top:1px solid #ddd\">".number_format($total, 0, ',', '.')."</td>";
		$content .= "</tr>";
	}

	if ($pakaivoucher) {
		if ($nominal_voucher > 0) {
			
			if ($tipe_voucher == 1) {
				$voucher = number_format($nominal_voucher, 0, ',', '.');
				$disc = $total - $nominal_voucher;
				$total = $disc;
			} else if ($tipe_voucher == 2) {
				$disc = 100 - $nominal_voucher;
				$temp = (($disc/100) * $total);
				$voucher = (int)$nominal_voucher . '%';
				$total = $temp;
			}
		} else {
			$voucher = 0;
		}
		$content .= "<tr>";
		$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._DISCOUNT."</td>";
		$content .= "<td align=\"right\">".$voucher."</td>";
		$content .= "</tr>";
	}
	
	if ($pakaiadditional) {
		if ($insurancevalue > 0) $total += $insurancevalue;
		$content .= "<tr>";
		$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._USEINSURANCE."</td>";
		$content .= "<td align=\"right\">".number_format($insurancevalue, 0, ',', '.')."</td>";
		$content .= "</tr>";
		
		if ($packingvalue > 0) $total += $packingvalue;
		$content .= "<tr>";
		$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._USEPACKING."</td>";
		$content .= "<td align=\"right\">".number_format($packingvalue, 0, ',', '.')."</td>";
		$content .= "</tr>";
	}
		
	//START MENENTUKAN PENGGUNAAN ONGKOS KIRIM
	if ($pakaiongkir) {
		$content .= "<tr>";
		// $content .= "<td colspan=\"4\" align=\"right\">"._POSTAGE." ($kurir_tipe)</td>";
		$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._POSTAGE."</td>";
		$content .= "<td align=\"right\">".number_format($shipping, 0, ',', '.')."</td>";
		$content .= "</tr>";
		$total = $total + $shipping;
	}
	//END MENENTUKAN PENGGUNAAN ONGKOS KIRIM
	$content .= "<tr>";
	$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._TOTAL."</td>";
	$content .= "<td align=\"right\">".number_format($total, 0, ',', '.')."</td>";
	$content .= "</tr>";
	$content .= "</table>";
	$content .= "</div>";
	$content .= "</div>";

	// $sql = "SELECT nama_lengkap, email, alamat, alamat2, kota, provinsi, zip, telepon, cellphone1, cellphone2 FROM sc_customer WHERE customer_id='$customer_id'";
	// $result = $mysqli->query($sql);
	// list($nama_lengkap, $email, $alamat, $alamat2, $kota, $provinsi, $zip, $telepon, $cellphone1, $cellphone2) = $result->fetch_row();
	// $content .= "<h2>"._PERSONALDETAILS."</h2>\r\n";
	// $content .= "<table border=\"0\">\r\n";
	// $content .= "<tr><td>"._FULLNAME."</td><td>:</td><td>$nama_lengkap</td></tr>\r\n";
	// $content .= "<tr><td>"._EMAIL."</td><td>:</td><td>$email</td></tr>\r\n";
	// $content .= "<tr valign=\"top\"><td>"._ADDRESS."</td><td>:</td><td>$alamat";
	// $content .= ($alamat2!='') ? "<br />$alamat2" : '';
	// $content .= "</td></tr>\r\n";
	// $content .= "<tr><td>"._STATE."</td><td>:</td><td>$provinsi</td></tr>\r\n";
	// $content .= "<tr><td>"._CITY."</td><td>:</td><td>$kota</td></tr>\r\n";
	// $content .= "<tr><td>"._ZIP."</td><td>:</td><td>$zip</td></tr>\r\n";
	// $content .= "<tr><td>"._TELEPHONE."</td><td>:</td><td>$telepon</td></tr>\r\n";
	// $content .= "<tr><td>"._CELLPHONE1."</td><td>:</td><td>$cellphone1</td></tr>\r\n";
	// $content .= "<tr><td>"._CELLPHONE2."</td><td>:</td><td>$cellphone2</td></tr>\r\n";
	// $content .= "</table>\r\n";

	$sql = "SELECT nama_lengkap, email, alamat, alamat2, kota, provinsi, zip, telepon, cellphone1, cellphone2, nama_pengirim, telp_pengirim, email_pengirim, company, npwp  FROM sc_receiver WHERE receiver_id='$receiver_id'";
	$result = $mysqli->query($sql);
	list($nama_lengkap, $email, $alamat, $alamat2, $kota, $provinsi, $zip, $telepon, $cellphone1, $cellphone2, $sendername, $senderphone, $senderemail,$company, $npwp) = $result->fetch_row();
	
	// <th>" . _PERSONALDETAILS . "</th>
	// $content .= "<table border=\"1\" cellpadding=\"3\" style=\"margin-top:15px;color:#000;border-collapse: collapse;border: 1px solid #bbb;\" class=\"table table-bordered\" width=\"100%\"><tr style=\"background: #eee;font-weight: bold;\"><th>" . _RECEIVERDETAILS . "</th>\r\n";
	// if ($sendername != '') {
		// $content .= "<th>"._DATASENDER."</th>\r\n";
	// }
	// $content .= "</tr>";
	// $content .= "<tr>";
	// // $content .= "<td valign=\"top\">\r\n";
	// // $content .= "$fullname<br/>\r\n";
	// // $content .= "$email<br/>\r\n";
	// // $content .= "$address<br/>";
	// // $content .= ($address2 != '') ? "$address2<br/>" : '';
	// // $content .= "$state<br/>\r\n";
	// // $content .= "$city<br/>\r\n";
	// // $content .= "$zip<br/>\r\n";
	// // $content .= "$phone<br/>\r\n";
	// // $content .= "$cellphone1<br/>\r\n";
	// // $content .= "$cellphone2</td>\r\n";
	// $content .= "<td valign=\"top\">$nama_lengkap<br/>\r\n";
	// // $content .= "$email<br/>\r\n";
	// $content .= "$alamat<br/>";
	// $content .= ($alamat2 != '') ? "$alamat2<br/>" : '';
	// $content .= "$kecamatan<br/>\r\n";
	// $content .= "$provinsi<br/>\r\n";
	// $content .= "$kota<br/>\r\n";
	// $content .= "$zip<br/>\r\n";
	// // $content .= "$phoneReceiver<br/>\r\n";
	// $content .= "$cellphone1<br/>\r\n";
	// $content .= "$cellphone2</td>\r\n";
	
	// if ($sendername != '') {
		// $content .= "<td valign=\"top\">\r\n";
		// $content .= "<table border=\"0\" class=\"table-pengirim\">\r\n";
		// $content .= "<tr><td>"._FULLNAME."</td><td>:</td><td>$sendername</td></tr>\r\n";
		// $content .= "<tr><td>"._TELEPHONE."</td><td>:</td><td>$senderphone</td></tr>\r\n";
		// if (!isset($_SESSION['member_uid'])) {
			// $content .= "<tr><td>"._EMAIL."</td><td>:</td><td>$senderemail</td></tr>\r\n";
		// }
		// $content .= "</table></td>\r\n";
	// }
	// $content .= "</tr></table>\r\n";
	
	$content .= "<table border=\"1\" cellpadding=\"3\" style=\"margin-top:15px;color:#000;border-collapse: collapse;border: 1px solid #bbb;\" class=\"table table-bordered\" width=\"100%\">";
	$content .= "<tr style=\"background: #eee;font-weight: bold;\">";
	$content .= "<th>"._RECEIVERDETAILS."</th>\r\n";
	if ($sendername != '') {
	$content .= "<th>"._DATASENDER."</th>\r\n";
	}
	$content .= "</tr><tr>\r\n";
	$content .= "<td>$nama_lengkap<br/>\r\n";
	$content .= "$company<br/>\r\n";
	// $content .= "<tr><td>"._EMAIL."</td><td>:</td><td>$email</td></tr>\r\n";
	$content .= "$alamat<br>";
	$content .= ($alamat2!='') ? "$alamat2<br>" : '';
	if ($kecamatan = '') $content .= "$kecamatan<br/>\r\n";
	if ($provinsi != "") $content .= "$provinsi<br/>\r\n";
	if ($kota != "") $content .= "$kota<br/>\r\n";
	$content .= "$zip<br/>\r\n";
	// $content .= "<tr><td>"._TELEPHONE."</td><td>:</td><td>$telepon</td></tr>\r\n";
	$content .= "$cellphone1<br/>\r\n";
	if ($cellphone2 != "") $content .= "$cellphone2<br/>\r\n";
	if ($npwp != "") $content .= "$npwp\r\n";
	$content .= "</td>\r\n";

	
	if ($sendername != '') {
		$content .= "<td>$sendername<br/>\r\n";
		$content .= "$senderphone<br/>\r\n";
		if ($senderemail != '') {
			$content .= "$senderemail\r\n";
		}
		$content .= "</td>\r\n";
	}
	
	$content .= "</tr>\r\n";
	$content .= "</table>\r\n";
	
	$sql = "SELECT logo,bank,cabang,norekening,atasnama FROM sc_payment WHERE id = '$metode_pembayaran'";
	$result = $mysqli->query($sql);
	list($logo,$bank,$cabang,$norekening,$atasnama) = $result->fetch_row();
	
	// $content .= "<table border=\"1\" cellpadding=\"3\" style=\"margin-top:15px;color:#000;border-collapse: collapse;border: 1px solid #bbb;\" class=\"table table-bordered\" width=\"100%\"><tr style=\"background: #eee;font-weight: bold;\"><th>"._POSTAGE."</th><th>"._PAYMENTINFO."</th>\r\n";
	
	// if ($catatan!='') {
		// $content .= "<th>"._NOTE."</th>\r\n";
	// }
	// $content .= "</tr>";
	// $content .= "<tr>";
	// $content .= "<td class='rekening_info'>
	// "._COURIER.": $kurir<br/>
	// "._POSTAGE.": $ongkos_kirim<br/>
	// "._SHIPPINGTIME.": $estimasi<br/>
	// </td>";
	// $content .= "<td valign=\"top\">";
	// $content .= "<table  class='rekening'>";
	// $content .= "<tr>";
	// if ($logo != '' && file_exists("$cfg_payment_path/$logo")) {
		// $content .= "<td class='rekening_logo'><img src='".$cfg_payment_logo."/$logo' alt='$bank'></td>";
	// }
	// $content .= "<td class='rekening_info'>
	// $bank $cabang<br/>
	// $norekening<br/>
	// $atasnama<br/>
	// </td>";
	// $content .= "</tr></table>";
	// $content .= "</td>";

	// if ($catatan!='') {
		// $content .= "<td valign=\"top\"><div id=\"ordernote\" style=\"color:#000\">$catatan</div></td>\r\n";
	// }
	// $content .= "</tr></table>\r\n";
	
	$content .= "<table border=\"1\" cellpadding=\"3\" style=\"margin-top:15px;color:#000;border-collapse: collapse;border: 1px solid #bbb;\" class=\"table table-bordered\" width=\"100%\">\r\n";
	$content .= "<tr style=\"background: #eee;font-weight: bold;\">";
	if ($pakaiongkir) {
		$content .= "<th>"._POSTAGE."</th>\r\n";
	}
	$content .= "<th>"._PAYMENTINFO."</th>\r\n";
	if ($catatan!='') {
		$content .= "<th>"._NOTE."</th>\r\n";
	}
	$content .= "</tr>\r\n";
	$content .= "<tr>\r\n";
	if ($pakaiongkir) {
		if ($catatan!='') {
			$content .= "<td width=\"33.333%\">";
		}else{
			$content .= "<td width=\"50%\">";
		}
		$content .= "
		$kurir<br/>
		$ongkos_kirim<br/>
		$estimasi
		</td>";
	}
	if ($catatan!='') {
		$content .= "<td width=\"33.333%\">";
	}else{
		$content .= "<td width=\"50%\">";
	}
	$content .= "<table class='rekening'>";
	$content .= "<tr>";
	if ($logo != '' && file_exists("$cfg_payment_path/$logo")) {
		$content .= "<td class='rekening_logo'><img src='".$cfg_payment_logo."/$logo' alt='$bank'></td>";
	}
	$content .= "<td class='rekening_info'>
	$bank $cabang<br/>
	No. Rek $norekening<br/>
	a/n $atasnama<br/>
	</td>";
	$content .= "</tr>";
	$content .= "</table>";
	$content .= "</td>";
	
	if ($catatan!='') {
		$content .= "<td width=\"33.333%\">$catatan</td>\r\n";
	}
	$content .= "</tr>\r\n";
	$content .= "</table>\r\n";
	
	// if (!isset($_SESSION['member_uid']) && $status <=1) {
		// $content .= "<div id=\"confirmPayment\" class=\"row\"><div class=\"col-sm-12\">".payment_confirmation()."</div></div>";
	// }
	

}

if ($action=='view_quotation') {
	$title = "e-Quotation";
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		$quotation_id = fiestolaundry($_POST['quotation_id'], 11);
		$status_quotation = fiestolaundry($_POST['quotation_status'], 11);
		
		if (isset($_POST['simpan'])) {
			$customer = fiestolaundry($_POST['customer'], 50); 
			$nama_pic = fiestolaundry($_POST['namapic'], 50); 
			$email_pic = fiestolaundry($_POST['emailpic'], 50); 
			$nomor_po = fiestolaundry($_POST['nomorpo'], 50); 
			$nomor_job_order = fiestolaundry($_POST['nomorjoborder'], 50);
			$detail_produk = fiestolaundry($_POST['detailproduk'], 0, false); 
			$nominaltransaksi = fiestolaundry($_POST['nominaltransaksi'], 13);
			
			$is_valid = true;
			
			if ($customer == '') {
				$errors[] = _CUSTOMER;
				$is_valid = false;
			}
			if ($nama_pic == '') {
				$errors[] = _NAMAPIC;
				$is_valid = false;
			}
			if ($email_pic == '') {
				$errors[] = _EMAILPIC;
				$is_valid = false;
			}
			if ($nomor_po == '') {
				$errors[] = _NOMORPO;
				$is_valid = false;
			}
			if ($nomor_job_order == '') {
				$errors[] = _NOMORJOBORDER;
				$is_valid = false;
			}
			if ($detail_produk == '') {
				$errors[] = _DETAILPRODUK;
				$is_valid = false;
			}
			if ($nominaltransaksi == '') {
				$errors[] = _NOMINALTRANSAKSI;
				$is_valid = false;
			}

			if (count($errors) > 0) {
				$content .= '<ul class="list-group alert alert-danger">';
				foreach($errors as $error) {
					$content .= '<li class="list-group-item">'.$error.' '._MUSTBEFILLED.'</li>';
				}
				$content .= '</ul>';
			}
			
			if ($is_valid) {
				$sql = "UPDATE sc_quotation SET status_quotation='$status_quotation', customer='$customer', nama_pic='$nama_pic', email_pic='$email_pic', nomor_po='$nomor_po', 
						nomor_job_order='$nomor_job_order', detail_produk='$detail_produk', nilai_transaksi='$nominaltransaksi' WHERE quotation_id='$quotation_id'";
				$result = $mysqli->query($sql);
			}
		}
		
		if (isset($_POST['simpanlost'])) {
			$sql = "UPDATE sc_quotation SET status_quotation='$status_quotation' WHERE quotation_id='$quotation_id'";
			$mysqli->query($sql);
		}
	}
	
	// $txid=$_GET['pid'];
	$sql = "SELECT tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher,kurir, discount, termofpayment, deliverytime, quotationvalidity, status_quotation, customer, nama_pic, email_pic, nomor_po, nomor_job_order, detail_produk, nilai_transaksi, siap_kirim, level2, level1, termandcondition, is_from_template, tc_warranty, tc_delivery_franco FROM sc_quotation WHERE quotation_id='$txid'";

	$result = $mysqli->query($sql);
	list($tanggal_buka_transaksi, $tanggal_tutup_transaksi, $customer_id, $receiver_id, $metode_pembayaran, $shipping, $status, $catatan, $no_pengiriman, $latestupdate, $kodevoucher, $tipe_voucher, $nominal_voucher,$kurir, $order_discount, $customTermOfPayment, $customDeliveryTime, $customQuotationValidity, $quotation_status, $customer_pic, $nama_pic, $email_pic, $nomor_po, $nomor_job_order, $detail_produk, $nilai_transaksi, $siap_kirim, $level2, $level1, $customQuotationTC, $is_from_template, $tc_warranty, $tc_delivery_franco) = $result->fetch_row();
	
	$couriers 		= json_decode($kurir, true);
	$kecamatan 		= $couriers['kota'];
	$kurir 			= $couriers['tipe'] . ' ' . $couriers['kota'];
	$kurir_tipe		= $couriers['tipe'];
	$ongkos_kirim 	= number_format($shipping, 0, ',', '.');
	$estimasi 		= $couriers['estimasi'];
	
	if ($level2 == 1 || $level1 == 1) {
		$content .= '<div class="alert alert-danger"><i class="fa fa-warning"></i> '._NEEDAPPROVAL.'</div>';
	} else {
		
	}
	// $content .= "<h2>"._EQUOTATION."</h2>\r\n";
	
	$sql = "SELECT nama_lengkap, email, company, alamat, alamat2, kota, provinsi, zip, telepon, cellphone1, cellphone2, nama_pengirim, telp_pengirim, email_pengirim, pic  FROM sc_receiver WHERE receiver_id='$receiver_id'";
	$result = $mysqli->query($sql);
	list($nama_lengkap, $email, $company, $alamat, $alamat2, $kota, $provinsi, $zip, $telepon, $cellphone1, $cellphone2, $sendername, $senderphone, $senderemail, $pic) = $result->fetch_row();
	$temp_address = ($alamat2 != '') ? "$alamat<br>$alamat2" : $alamat;
	
	if ($pic != '') {
		$temp_pic = '
		<tr>
			<td valign="top">'._QUOTATIONPREPAREBY.'</td>
			<td valign="top">:</td>
			<td>'.$pic.'</td>
		</tr>';
	} else {
		$temp_pic = '';
	}
	
	if ($_SESSION['member_level'] <=3 && ($level2 == 1 || $level1 == 1)) {
		$trans_id = $quotation_number_pending;
	} else {
		$trans_id = $txid;
	}
	$content .= '
	<div class="table_block table-responsive table-small-gap">
	<table class="header-table-quotation">
		<tr>
			<td width="48%">
				<table>
					<tr valign="top">
						<td width="10%">'._QUOTATIONTO.'</td>
						<td width="8%" align="left">:</td>
						<td width="82%">
							'.$company.'<br>
							'.$temp_address.'<br>
							'.$kota.'<br>
							'.$zip.'<br>
							Phone: '.$cellphone1.'
							<p style="line-height:0.8;">&nbsp;</p>
						</td>
					</tr>
					<tr valign="top">
						<td>'._QUOTATIONATTN.'</td>
						<td>:</td>
						<td>
							'.$nama_lengkap.'<br>
							'.$email.'
						</td>
					</tr>
				</table>
			</td>
			<td width="4%">&nbsp;</td>
			<td width="50%" valign="top">
				<table>
					<tr>
						<td width="27%">'._QUOTATIONDATE.'</td>
						<td width="3%">:</td>
						<td width="70%">'.tglformat($tanggal_buka_transaksi).'</td>
					</tr>
					<tr>
						<td>'._QUOTATIONNUMBER.'</td>
						<td>:</td>
						<td>'.$trans_id.'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					'.$temp_pic.'
					<!--<tr>
						<td>'._EMAIL.'</td>
						<td>:</td>
						<td>'.$email.'</td>
					</tr>-->
				</table>
			</td>
		</tr>
	</table>
	</div>
	';
	
	$content .= '
	<p style="line-height:0.6;">&nbsp;</p>
	<div class="table_block table-responsive table-small-gap">
	<table border="0" class="table table-bordered" width="100%" cellpadding="7">
		<tr style="background-color:#f5f5f5;border-bottom: 1px solid #aaa;font-weight:bold">
			<th style="text-align:center" width="8%" style="border-bottom: 1px solid #aaa;">'._ITEM.'</th>
			<th style="text-align:center" width="35%" style="border-bottom: 1px solid #aaa;">'._PARTNUMBER.'</th>
			<th style="text-align:center" width="20%" style="border-bottom: 1px solid #aaa;">'._UNITPRICE.'</th>
			<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._DISCOUNT.'</th>
			<!--<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._QTY.'</th>-->
			<th style="text-align:center" width="7%" style="border-bottom: 1px solid #aaa;">Qty</th>
			<th style="text-align:right" width="20%" style="border-bottom: 1px solid #aaa;">'._TOTAIDR.'</th>
		</tr>
	';
	$item = 0;
	$total = 0;
	
	// if ($is_from_template == 1) {
		// $sql = "SELECT jumlah,harga,product_id,product_name, '' as merek, filename, diskon, disc_per_item FROM sc_quotation_detail qd INNER JOIN sc_quotation q ON qd.quotation_id=q.quotation_id WHERE qd.quotation_id='$txid' AND is_from_template=1 ORDER BY quotation_detail_id";
		// $result = $mysqli->query($sql);
		// while (list($quantity, $price, $product_id, $productname,$merek,$filename, $discount, $price_dics, $disc_per_item) = $result->fetch_row()) {
			// $brand_name = get_brand_name($idmerek);
			// $line_cost = $price * $quantity;
			// $total = $total + $line_cost;   //add to the total cost
			// $product_id = ($product_id == 0) ? '' : $product_id;
			// $content .= '
			// <tr>
			// </tr>
			// ';
		// }
	// } else {
		// $sql = "SELECT jumlah,td.harga,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id<>'$cat_loose_item' ORDER BY quotation_detail_id";
		
		// $sql = "SELECT jumlah,td.harga,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id<>'$cat_loose_item' ORDER BY quotation_detail_id";
		
		if ($is_from_template == 1) {
			$sql = "SELECT jumlah,td.harga,product_name,td.filename, '' as merek, td.diskon,  '' as keterangan, '' as sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, parent FROM sc_quotation_detail td WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
		} else {
			$sql = "SELECT jumlah,td.harga,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, parent FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id <> $cat_loose_item ORDER BY quotation_detail_id";
		}
		
		$no_parent = 1;
		$result = $mysqli->query($sql);
		while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $parent) = $result->fetch_row()) {
			
			$brand_name = get_brand_name($idmerek);
			$parent = $parent == 0 ? $no_parent++ : "";
			// $line_cost = $quantity * $price;  //work out the line cost
			// $total = $total + $line_cost;   //add to the total cost
			
			// 05/03/2020 14:57
			if ($is_from_template == 1) {
				if ($discount != '') {
					$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
					// $line_cost = $quantity * $price;  //work out the line cost
					// $disc = $price * (int)$discount / 100;
					// $harga_setelah_disc = $disc * $quantity;
					
					$line_cost = ($quantity * $price) - (($quantity * $price) * (int)$discount / 100);
					$total += $line_cost;
				} else {
					$line_cost = $quantity * $price;  //work out the line cost
					$total += $line_cost;
				}
			} else {
				$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
				$line_cost = $quantity * $price;  //work out the line cost
				$disc = $price * (int)$discount / 100;
				$harga_setelah_disc = $disc * $quantity;
				
				// $price_dics = $line_cost - $harga_setelah_disc;
				if ($disc_per_item != '') {
					$price_dics = $line_cost - $harga_setelah_disc;
					$line_cost = $price_dics;
				} /* else {
					// $price_dics = 0;
				} */
				$total += $price_dics;
			}
			
			// $brand_name = get_brand_name($idmerek);
			// // $line_cost = $quantity * $price;  //work out the line cost
			// // $total = $total + $line_cost;   //add to the total cost
			
			// if ($discount != '' && $disc_per_item == '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;
				
			// } else if ($discount != '' && $disc_per_item != '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;
				
			// } else if ($discount == '' && $disc_per_item != '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;
				
			// } else if ($discount == '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = '';
			// } else {
				// $line_cost = $price_dics * $quantity;  //work out the line cost
			// }
			// if ($discount != '') {
				// $discount = "$discount%";
			// }
			// $total = $total + $line_cost;   //add to the total cost
			// $total = (int)$total;
			
			// $item++;
			// $content .= '
			// <tr>
				// <td style="text-align:center">'.$item.'</td>
				// <td>
					// <p class="product-name"><strong>'.$brand_name. ' ' .$product_code.'</strong></p>
					// <span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
				// </td>
				// <td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
				// <td style="text-align:center">'.$discount.'</td>
				// <td style="text-align:center">'.$quantity.'</td>
				// <td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
			// </tr>
			// ';
			
			$suffix_disc = (substr_count($discount, '%') == 1) ? "$discount" : "$discount%";
			if ($discount != '') $discount = $suffix_disc;
		
			$item++;
			// $price = ($price > 0) ? $price : '';
			// (negative_check($line_cost)) ? number_format($line_cost, 0, ',', '.') : ""
			$product_price = ($price > 0) ? number_format($price, 0, ',', '.') : (negative_check($price)) ? number_format($price, 0, ',', '.') : "";
			// $line_cost = ($line_cost > 0) ? $line_cost : "";
			$line_cost = ($line_cost > 0) ? $line_cost : (negative_check($line_cost)) ? number_format($line_cost, 0, ',', '.') : "";
			$is_product_code = ($product_code != '') ? '<p class="product-name"><strong>'.$product_code.'</strong></p>' : '';
			
			if ($is_from_template == 1) {
				$no = $parent;
			} else {
				$no = $item;
			}
			$red_class = negative_check($price) ? ';color:red' : '';

			$content .= '
			<tr>
				<td style="text-align:center">'.$no.'</td>
				<td>
					'.$is_product_code.'
					<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
				</td>
				<td style="text-align:right'.$red_class.'">'.$product_price.'</td>
				<td style="text-align:center">'.$discount.'</td>
				<td style="text-align:center">'.$quantity.'</td>
				<td style="text-align:right'.$red_class.'">'.$line_cost.'</td>
			</tr>
			';
			
		}
		
		$sql = "SELECT jumlah,td.harga,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id='$cat_loose_item' ORDER BY quotation_detail_id";
		$result = $mysqli->query($sql);
		if ($result->num_rows > 0) {
			$content .= "<tr style=\"background: #eee;font-weight: bold;\"><td colspan=\"6\">"._LOOSEITEM."</td></tr>";
			while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item) = $result->fetch_row()) {
				$brand_name = get_brand_name($idmerek);
				// $line_cost = $quantity * $price;  //work out the line cost
				// $total = $total + $line_cost;   //add to the total cost
				
				if ($discount != '' && $disc_per_item == '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount != '' && $disc_per_item != '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount == '' && $disc_per_item != '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount == '') {
					$line_cost = $pricenormal * $quantity;
					$discount = '';
				} else {
					$line_cost = $price_dics * $quantity;  //work out the line cost
				}
				if ($discount != '') {
					$discount = $discount.'%';
				}
				$total = $total + $line_cost;   //add to the total cost
				// $total = (int)$total;
				
				$item++;
				$content .= '
				<tr>
					<td style="text-align:center">'.$item.'</td>
					<td>
						<p class="product-name"><strong>'.$product_code.'</strong></p>
						<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
					</td>
					<td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
					<td style="text-align:center">'.$discount.'</td>
					<td style="text-align:center">'.$quantity.'</td>
					<td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
				</tr>
				';
			}
		}
	// }
	
	$discount = ($total*10)/100;
	$total_price = ($discount > 0) ? $total+round($discount) : $total;
	$total_price = round($total_price);
	
	$termofpayment = ($customTermOfPayment != '') ? $customTermOfPayment : '';
	$deliverytime = ($customDeliveryTime != '') ? $customDeliveryTime : '';
	$deliveryfranco = ($deliveryfranco != '') ? $deliveryfranco : '';
	$warranty = ($warranty != '') ? $warranty : '';
	$quotationvalidity = ($quotationvalidity != '') ? $quotationvalidity : '';
	// $quotationvalidity = ($customQuotationValidity != '') ? $customQuotationValidity : '';
	$termandcondition = ($customQuotationTC != '') ? $customQuotationTC : '';
	
	$content .= '
	<tr>
		<td style="border-top: 1px solid #aaa;font-weight: bold;" colspan="5" align="right">'._TOTALLISTPRICE.'</td>
		<td align="right" style="border-top: 1px solid #aaa;">'.number_format($total, 0, ',', '.').'</td>
	</tr>
	';
	
	if ($order_discount != '') {
		// $voucher = number_format($nominal_voucher, 0, ',', '.');
		// $total_price = $total_price - $nominal_voucher;
		
		if (substr_count($order_discount, '%') == 1) {
			$voucher = $order_discount;
			$disc = $total*$voucher/100;
			$total = $total - $disc;
			$subtotal = $total;
			$discount_type = _DISCOUNTPERCENT;
		} else {
			$voucher = number_format($order_discount, 0, ',', '.');
			$total = $total - $order_discount;
			$subtotal = $total;
			$discount_type = _DISCOUNTIDR;
		}
		
		$discount = ($subtotal*10)/100;
		$total_price = ($discount > 0) ? $subtotal+$discount : $subtotal;
		$total_price = round($total_price);
		
		$content .= '
		<tr>
			<td colspan="5" align="right" style="font-weight: bold;">'.$discount_type.'</td>
			<td align="right">('.((substr_count($order_discount, '%') == 1) ? $order_discount : number_format($order_discount, 0, ',', '.')).')</td>
		</tr>
		';
		
		$content .= '
		<tr>
			<td colspan="5" align="right" style="font-weight: bold;">'._SUBTOTAL.'</td>
			<td align="right">'.number_format($subtotal, 0, ',', '.').'</td>
		</tr>
		';
	}
	$terbilang = convertNumber($total_price);	//bilangan($total_price);
	
	$content .= '
	<tr>
		<td colspan="5" align="right" style="font-weight: bold;">'._VATIDR.'</td>
		<td align="right">'.number_format($discount, 0, ',', '.').'</td>
	</tr>
	<tr>
		<td colspan="5" align="right" style="font-weight: bold;">'._TOTAIDR.'</td>
		<td align="right">'.number_format($total_price, 0, ',', '.').'</td>
	</tr>
	<tr style="background-color:#f5f5f5;">
		<td colspan="6" style="border-top:1px solid #aaa; font-weight:bold;solid #aaa">'._TERBILANG.': '.$terbilang.'</td>
	</tr>
	</table>
	</div>
	';
	

	// if ($catatan != '') {
		// $content .= '
		// <p>
			// <u><b>'._NOTE.'</b></u><br/>
			// '.$catatan.'<br/>'.$additional_note.'
		// </p>
		// ';
	// } else {
		// $content .= '<p>'.$additional_note.'<br/></p>';
	// }
	
	$content .= '<p><u><b>'._NOTE.'</b></u><br/>';
	if ($catatan != '') {
		
		$content .= $catatan.'<br/>'.$additional_note;
	} else {
		$content .= $additional_note.'<br/>';
	}
	$content .= '</p>';
	
	$berca_estore = (isset($_SESSION['member_uid']) && typeCompany($txid) == $berca_id) ? '<img src="'.$cfg_app_url.'/images/berca-estore.png" style="height:23px" >' : '<img src="'.$cfg_app_url.'/images/berca-estore-bnm.png" style="height:32px" >';
	
	if ($is_from_template == 1) {
		$content .= '
		<p>
			<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
			<table>
				<tr>
					<td style="width: 150px;">Delivery Time</td>
					<td style="width: 550px;">: '.$deliverytime.'</td>
				</tr>
				<tr>
					<td>Term of Payment</td>
					<td>: '.$termofpayment.'</td>
				</tr>
				<tr>
					<td>Quotation Validity</td>
					<td>: '.$quotationvalidity.'</td>
				</tr>
				<tr>
					<td>Delivery Franco</td>
					<td>: '.$tc_delivery_franco.'</td>
				</tr>
				<tr>
					<td>Warranty</td>
					<td>: '.$tc_warranty.'</td>
				</tr>
			</table>
		</p>';
	} else {
		$content .= '
		<p>
			<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
			'.$termandcondition.'
		</p>';
	}
	$content .= '
	<!--<p>
		<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
		'._TERMOFPAYMENT.': '.$termofpayment.'<br/>
		'._DELIVERYTIME.': '.$deliverytime.'<br/>
		'._DELIVERYFRANCO.': '.$deliveryfranco.'<br/>
		'.(($warranty != '') ? _WARRANTY.': '.$warranty.'<br/>' : '').'
		'._QUOTATIONVALIDITY.': '.$quotationvalidity.'
	</p>
	
	<p>
		<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
		'.$termandcondition.'
	</p>-->
	

	<p>'._SINCERELYYOURS.',</p>
	<!--<img src="'.$cfg_app_url.'/images/berca-estore.png" height="60px" >-->
	<p>'.$berca_estore.'</p>
	
	<div class="automatically-generate" style="background-color:#eee;margin-bottom:30px;margin-top:30px;text-align:center;line-height:35px">'.$quotationvalidity.'</div>
	';
	
	$_pid = base64_encode($txid);
	
	if ($level2 == 1 || $level1 == 1) {
		
	} else {
		$content .= '<div class="downloadpdf"><a class="btn btn-default more downloadpdf-button" href="'.$urlfunc->makePretty("?p=order&action=downloadpdf&pid=$_pid").'" target="_blank"><img src="'.$cfg_app_url.'/images/pdf-icon.svg">'._DOWNLOADPDF.'</a></div>';
	}
	// die($siap_kirim);
	if ($siap_kirim == 1) {
		if ($quotation_status == 0) {
			$content .= '
			<br>
			<form id="quotatoiniswon" method="POST" action="#">
				<div class="panel panel-default">
				  <div class="panel-heading">
					Status Quotation:
					<select id="quotation_status" name="quotation_status">';
					foreach($quotation_status_arr as $idx => $status) {
						if ($idx == $quotation_status) {
							$content .= '<option value="'.$idx.'" selected>'.$status.'</option>';
						} else {
							$content .= '<option value="'.$idx.'">'.$status.'</option>';
						}
					}
					
					$is_won = ($quotation_status == 1) ? 'block' : 'none';
					$is_lost = ($quotation_status == 2 || $quotation_status == 3) ? '' : 'none';
			$content .= '
					</select>
					<button type="submit" name="simpanlost" class="btn btn-primary btn-default more" style="display:'.$is_lost.'">'._SAVE.'</button>
				</div>
				<div id="quotation_body" class="panel-body" style="display:'.$is_won.'">
					<input type="hidden" name="quotation_id" value="'.$txid.'">
					<div class="row">
					<div class="col-sm-4">
					<div class="form-group">
						<label for="customer" class="control-label">'._CUSTOMER.'</label>
						<input type="text" name="customer" id="customer" class="form-control" value="'.$customer_pic.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="namapic" class="control-label">'._NAMAPIC.'</label>
						<input type="text" name="namapic" class="form-control" value="'.$nama_pic.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="emailpic" class="control-label">'._EMAILPIC.'</label>
						<input type="text" name="emailpic" class="form-control" value="'.$email_pic.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="nomorpo" class="control-label">'._NOMORPO.'</label>
						<input type="text" name="nomorpo" class="form-control" value="'.$nomor_po.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="nomorjoborder" class="control-label">'._NOMORJOBORDER.'</label>
						<input type="text" name="nomorjoborder" class="form-control" value="'.$nomor_job_order.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="nominaltransaksi" class="control-label">'._NOMINALTRANSAKSI.'</label>
						<input type="text" name="nominaltransaksi" class="form-control" value="'.(($nilai_transaksi > 0) ? number_format($nilai_transaksi, 0, ',', '.') : '').'">
					</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group">
						<label for="detailproduk" class="control-label">'._DETAILPRODUK.'</label>
						<textarea name="detailproduk" cols="5" rows="10" class="form-control">'.$detail_produk.'</textarea>
					</div>
					</div>
					<div class="col-sm-12">
					<button type="submit" name="simpan" class="btn btn-primary btn-default more">'._SAVE.'</button>
					</div>
					</div>
				  </div>
				</div>
			</form>
			';
		} else {
			$content .= '
			<div class="panel panel-default">
				<div class="panel-heading">
					'._STATUSQUOTATION.': <strong>'.$quotation_status_arr[$quotation_status].'</strong>
				</div>';
			if ($quotation_status == 1) {
				$content .= '
					<div id="quotation_body" class="panel-body" style="display:'.$is_won.'">
						<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="customer" class="control-label">'._CUSTOMER.'</label>
								<p>'.$customer_pic.'</p>
							</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="namapic" class="control-label">'._NAMAPIC.'</label>
							<p>'.$nama_pic.'</p>
						</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="emailpic" class="control-label">'._EMAILPIC.'</label>
							<p>'.$email_pic.'</p>
						</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="nomorpo" class="control-label">'._NOMORPO.'</label>
							<p>'.$nomor_po.'</p>
						</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="nomorjoborder" class="control-label">'._NOMORJOBORDER.'</label>
							<p>'.$nomor_job_order.'</p>
						</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="nominaltransaksi" class="control-label">'._NOMINALTRANSAKSI.'</label>
							<p>'.(($nilai_transaksi > 0) ? number_format($nilai_transaksi, 0, ',', '.') : '').'</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="form-group">
							<label for="detailproduk" class="control-label">'._DETAILPRODUK.'</label>
							<p>'.$detail_produk.'</p>
						</div>
						</div>
						</div>
					  </div>
					</div>
				';
			}
		}
	}
	
	
}

if ($action=='view_price_information') {
	$title = "e-Price Information";
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		$quotation_id = fiestolaundry($_POST['quotation_id'], 11);
		$status_quotation = fiestolaundry($_POST['quotation_status'], 11);
		
		if (isset($_POST['simpan'])) {
			$customer = fiestolaundry($_POST['customer'], 50); 
			$nama_pic = fiestolaundry($_POST['namapic'], 50); 
			$email_pic = fiestolaundry($_POST['emailpic'], 50); 
			$nomor_po = fiestolaundry($_POST['nomorpo'], 50); 
			$nomor_job_order = fiestolaundry($_POST['nomorjoborder'], 50);
			$detail_produk = fiestolaundry($_POST['detailproduk'], 0, false); 
			$nominaltransaksi = fiestolaundry($_POST['nominaltransaksi'], 13);
			
			$is_valid = true;
			
			if ($customer == '') {
				$errors[] = _CUSTOMER;
				$is_valid = false;
			}
			if ($nama_pic == '') {
				$errors[] = _NAMAPIC;
				$is_valid = false;
			}
			if ($email_pic == '') {
				$errors[] = _EMAILPIC;
				$is_valid = false;
			}
			if ($nomor_po == '') {
				$errors[] = _NOMORPO;
				$is_valid = false;
			}
			if ($nomor_job_order == '') {
				$errors[] = _NOMORJOBORDER;
				$is_valid = false;
			}
			if ($detail_produk == '') {
				$errors[] = _DETAILPRODUK;
				$is_valid = false;
			}
			if ($nominaltransaksi == '') {
				$errors[] = _NOMINALTRANSAKSI;
				$is_valid = false;
			}

			if (count($errors) > 0) {
				$content .= '<ul class="list-group alert alert-danger">';
				foreach($errors as $error) {
					$content .= '<li class="list-group-item">'.$error.' '._MUSTBEFILLED.'</li>';
				}
				$content .= '</ul>';
			}
			
			if ($is_valid) {
				$sql = "UPDATE sc_quotation SET status_quotation='$status_quotation', customer='$customer', nama_pic='$nama_pic', email_pic='$email_pic', nomor_po='$nomor_po', 
						nomor_job_order='$nomor_job_order', detail_produk='$detail_produk', nilai_transaksi='$nominaltransaksi' WHERE quotation_id='$quotation_id'";
				$result = $mysqli->query($sql);
			}
		}
		
		if (isset($_POST['simpanlost'])) {
			$sql = "UPDATE sc_quotation SET status_quotation='$status_quotation' WHERE quotation_id='$quotation_id'";
			$mysqli->query($sql);
		}
	}
	
	// $txid=$_GET['pid'];
	$sql = "SELECT tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher,kurir, discount, termofpayment, deliverytime, quotationvalidity, status_quotation, customer, nama_pic, email_pic, nomor_po, nomor_job_order, detail_produk, nilai_transaksi, siap_kirim, level2, level1, termandcondition, is_from_template, tc_warranty, tc_delivery_franco FROM sc_price_information WHERE price_information_id='$txid'";

	$result = $mysqli->query($sql);
	list($tanggal_buka_transaksi, $tanggal_tutup_transaksi, $customer_id, $receiver_id, $metode_pembayaran, $shipping, $status, $catatan, $no_pengiriman, $latestupdate, $kodevoucher, $tipe_voucher, $nominal_voucher,$kurir, $order_discount, $customTermOfPayment, $customDeliveryTime, $customQuotationValidity, $quotation_status, $customer_pic, $nama_pic, $email_pic, $nomor_po, $nomor_job_order, $detail_produk, $nilai_transaksi, $siap_kirim, $level2, $level1, $customQuotationTC, $is_from_template, $tc_warranty, $tc_delivery_franco) = $result->fetch_row();
	
	$couriers 		= json_decode($kurir, true);
	$kecamatan 		= $couriers['kota'];
	$kurir 			= $couriers['tipe'] . ' ' . $couriers['kota'];
	$kurir_tipe		= $couriers['tipe'];
	$ongkos_kirim 	= number_format($shipping, 0, ',', '.');
	$estimasi 		= $couriers['estimasi'];
	
	if ($level2 == 1 || $level1 == 1) {
		$content .= '<div class="alert alert-danger"><i class="fa fa-warning"></i> '._NEEDAPPROVAL.'</div>';
	} else {
		
	}
	// $content .= "<h2>"._EQUOTATION."</h2>\r\n";
	
	$sql = "SELECT nama_lengkap, email, company, alamat, alamat2, kota, provinsi, zip, telepon, cellphone1, cellphone2, nama_pengirim, telp_pengirim, email_pengirim, pic  FROM sc_receiver WHERE receiver_id='$receiver_id'";
	$result = $mysqli->query($sql);
	list($nama_lengkap, $email, $company, $alamat, $alamat2, $kota, $provinsi, $zip, $telepon, $cellphone1, $cellphone2, $sendername, $senderphone, $senderemail, $pic) = $result->fetch_row();
	$temp_address = ($alamat2 != '') ? "$alamat<br>$alamat2" : $alamat;
	
	if ($pic != '') {
		$temp_pic = '
		<tr>
			<td valign="top">'._QUOTATIONPREPAREBY.'</td>
			<td valign="top">:</td>
			<td>'.$pic.'</td>
		</tr>';
	} else {
		$temp_pic = '';
	}
	
	if ($_SESSION['member_level'] <=3 && ($level2 == 1 || $level1 == 1)) {
		$trans_id = $quotation_number_pending;
	} else {
		$trans_id = $txid;
	}
	$content .= '
	<div class="table_block table-responsive table-small-gap">
	<table class="header-table-quotation">
		<tr>
			<td width="48%">
				<table>
					<tr valign="top">
						<td width="10%">'._QUOTATIONTO.'</td>
						<td width="8%" align="left">:</td>
						<td width="82%">
							'.$company.'<br>
							'.$temp_address.'<br>
							'.$kota.'<br>
							'.$zip.'<br>
							Phone: '.$cellphone1.'
							<p style="line-height:0.8;">&nbsp;</p>
						</td>
					</tr>
					<tr valign="top">
						<td>'._QUOTATIONATTN.'</td>
						<td>:</td>
						<td>
							'.$nama_lengkap.'<br>
							'.$email.'
						</td>
					</tr>
				</table>
			</td>
			<td width="4%">&nbsp;</td>
			<td width="50%" valign="top">
				<table>
					<tr>
						<td width="27%">'._QUOTATIONDATE.'</td>
						<td width="3%">:</td>
						<td width="70%">'.tglformat($tanggal_buka_transaksi).'</td>
					</tr>
					<tr>
						<td>'._QUOTATIONNUMBER.'</td>
						<td>:</td>
						<td>'.$trans_id.'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					'.$temp_pic.'
					<!--<tr>
						<td>'._EMAIL.'</td>
						<td>:</td>
						<td>'.$email.'</td>
					</tr>-->
				</table>
			</td>
		</tr>
	</table>
	</div>
	';
	
	$content .= '
	<p style="line-height:0.6;">&nbsp;</p>
	<div class="table_block table-responsive table-small-gap">
	<table border="0" class="table table-bordered" width="100%" cellpadding="7">
		<tr style="background-color:#f5f5f5;border-bottom: 1px solid #aaa;font-weight:bold">
			<th style="text-align:center" width="8%" style="border-bottom: 1px solid #aaa;">'._ITEM.'</th>
			<th style="text-align:center" width="35%" style="border-bottom: 1px solid #aaa;">'._PARTNUMBER.'</th>
			<th style="text-align:center" width="20%" style="border-bottom: 1px solid #aaa;">'._UNITPRICE.'</th>
			<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._DISCOUNT.'</th>
			<!--<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._QTY.'</th>-->
			<th style="text-align:center" width="7%" style="border-bottom: 1px solid #aaa;">Qty</th>
			<th style="text-align:right" width="20%" style="border-bottom: 1px solid #aaa;">'._TOTAIDR.'</th>
		</tr>
	';
	$item = 0;
	$total = 0;
	
	if ($is_from_template == 1) {
		$sql = "SELECT jumlah,harga,product_id,product_name,merek, filename, diskon, disc_per_item FROM sc_price_information_detail qd INNER JOIN sc_price_information q ON qd.price_information_id=q.price_information_id WHERE qd.price_information_id='$txid' AND is_from_template=1 ORDER BY price_information_detail_id";
		$result = $mysqli->query($sql);
		
		while (list($quantity, $price, $product_id, $productname,$merek,$filename, $discount, $price_dics, $disc_per_item) = $result->fetch_row()) {
			$brand_name = get_brand_name($idmerek);
			$line_cost = $price * $quantity;
			$total = $total + $line_cost;   //add to the total cost
			$product_id = ($product_id == 0) ? '' : $product_id;
			$content .= '
			<tr>
			</tr>
			';
		}
	} else {
		$sql = "SELECT jumlah,td.harga,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, price_information FROM sc_price_information_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE price_information_id='$txid' AND c.cat_id<>'$cat_loose_item' ORDER BY price_information_detail_id";
		$result = $mysqli->query($sql);
		while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $price_information) = $result->fetch_row()) {
			$brand_name = get_brand_name($idmerek);
			// $line_cost = $quantity * $price;  //work out the line cost
			// $total = $total + $line_cost;   //add to the total cost
			$pricenormal = $price_information;
			
			if ($discount != '' && $disc_per_item == '') {
				$line_cost = $pricenormal * $quantity;
				$discount = $disc_per_item;
				
				$nominal = str_replace('%', '', $disc_per_item);
				$price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				$disc = 100 - $nominal;
				$temp = (($disc/100) * $line_cost);
				$line_cost = $temp;
				
			} else if ($discount != '' && $disc_per_item != '') {
				$line_cost = $pricenormal * $quantity;
				$discount = $disc_per_item;
				
				$nominal = str_replace('%', '', $disc_per_item);
				$price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				$disc = 100 - $nominal;
				$temp = (($disc/100) * $line_cost);
				$line_cost = $temp;
				
			} else if ($discount == '' && $disc_per_item != '') {
				$line_cost = $pricenormal * $quantity;
				$discount = $disc_per_item;
				
				$nominal = str_replace('%', '', $disc_per_item);
				$price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				$disc = 100 - $nominal;
				$temp = (($disc/100) * $line_cost);
				$line_cost = $temp;
				
			} else if ($discount == '') {
				$line_cost = $pricenormal * $quantity;
				$discount = '';
			} else {
				$line_cost = $price_dics * $quantity;  //work out the line cost
			}
			if ($discount != '') {
				$discount = "$discount%";
			}
			$total = $total + $line_cost;   //add to the total cost
			// $total = (int)$total;
			
			$item++;
			$content .= '
			<tr>
				<td style="text-align:center">'.$item.'</td>
				<td>
					<p class="product-name"><strong>'.$brand_name. ' ' .$product_code.'</strong></p>
					<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
				</td>
				<td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
				<td style="text-align:center">'.$discount.'</td>
				<td style="text-align:center">'.$quantity.'</td>
				<td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
			</tr>
			';
		}
		
		$sql = "SELECT jumlah,td.harga,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id='$cat_loose_item' ORDER BY quotation_detail_id";
		$result = $mysqli->query($sql);
		if ($result->num_rows > 0) {
			$content .= "<tr style=\"background: #eee;font-weight: bold;\"><td colspan=\"6\">"._LOOSEITEM."</td></tr>";
			while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item) = $result->fetch_row()) {
				$brand_name = get_brand_name($idmerek);
				// $line_cost = $quantity * $price;  //work out the line cost
				// $total = $total + $line_cost;   //add to the total cost
				
				if ($discount != '' && $disc_per_item == '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount != '' && $disc_per_item != '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount == '' && $disc_per_item != '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount == '') {
					$line_cost = $pricenormal * $quantity;
					$discount = '';
				} else {
					$line_cost = $price_dics * $quantity;  //work out the line cost
				}
				if ($discount != '') {
					$discount = $discount.'%';
				}
				$total = $total + $line_cost;   //add to the total cost
				// $total = (int)$total;
				
				$item++;
				$content .= '
				<tr>
					<td style="text-align:center">'.$item.'</td>
					<td>
						<p class="product-name"><strong>'.$product_code.'</strong></p>
						<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
					</td>
					<td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
					<td style="text-align:center">'.$discount.'</td>
					<td style="text-align:center">'.$quantity.'</td>
					<td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
				</tr>
				';
			}
		}
	}
	
	$discount = ($total*10)/100;
	$total_price = ($discount > 0) ? $total+round($discount) : $total;
	$total_price = round($total_price);
	
	$termofpayment = ($customTermOfPayment != '') ? $customTermOfPayment : '';
	$deliverytime = ($customDeliveryTime != '') ? $customDeliveryTime : '';
	$deliveryfranco = ($deliveryfranco != '') ? $deliveryfranco : '';
	$warranty = ($warranty != '') ? $warranty : '';
	$quotationvalidity = ($quotationvalidity != '') ? $quotationvalidity : '';
	// $quotationvalidity = ($customQuotationValidity != '') ? $customQuotationValidity : '';
	$termandcondition = ($customQuotationTC != '') ? $customQuotationTC : '';
	
	// $content .= '
	// <tr>
		// <td style="border-top: 1px solid #aaa;font-weight: bold;" colspan="5" align="right">'._TOTALLISTPRICE.'</td>
		// <td align="right" style="border-top: 1px solid #aaa;">'.number_format($total, 0, ',', '.').'</td>
	// </tr>
	// ';
	
	if ($order_discount != '') {
		// $voucher = number_format($nominal_voucher, 0, ',', '.');
		// $total_price = $total_price - $nominal_voucher;
		
		if (substr_count($order_discount, '%') == 1) {
			$voucher = $order_discount;
			$disc = $total*$voucher/100;
			$total = $total - $disc;
			$subtotal = $total;
			$discount_type = _DISCOUNTPERCENT;
		} else {
			$voucher = number_format($order_discount, 0, ',', '.');
			$total = $total - $order_discount;
			$subtotal = $total;
			$discount_type = _DISCOUNTIDR;
		}
		
		$discount = ($subtotal*10)/100;
		$total_price = ($discount > 0) ? $subtotal+$discount : $subtotal;
		$total_price = round($total_price);
		
		$content .= '
		<tr>
			<td colspan="5" align="right" style="font-weight: bold;">'.$discount_type.'</td>
			<td align="right">('.((substr_count($order_discount, '%') == 1) ? $order_discount : number_format($order_discount, 0, ',', '.')).')</td>
		</tr>
		';
		
		// $content .= '
		// <tr>
			// <td colspan="5" align="right" style="font-weight: bold;">'._SUBTOTAL.'</td>
			// <td align="right">'.number_format($subtotal, 0, ',', '.').'</td>
		// </tr>
		// ';
	}
	$terbilang = convertNumber($total_price);	//bilangan($total_price);
	
	$content .= '
	<!--<tr>
		<td colspan="5" align="right" style="font-weight: bold;">'._VATIDR.'</td>
		<td align="right">'.number_format($discount, 0, ',', '.').'</td>
	</tr>-->
	<tr>
		<td colspan="5" align="right" style="font-weight: bold;">'._TOTAIDR.'</td>
		<td align="right">'.number_format($total_price, 0, ',', '.').'</td>
	</tr>
	<tr style="background-color:#f5f5f5;">
		<td colspan="6" style="border-top:1px solid #aaa; font-weight:bold;solid #aaa">'._TERBILANG.': '.$terbilang.'</td>
	</tr>
	</table>
	</div>
	';
	

	// if ($catatan != '') {
		// $content .= '
		// <p>
			// <u><b>'._NOTE.'</b></u><br/>
			// '.$catatan.'<br/>'.$additional_note.'
		// </p>
		// ';
	// } else {
		// $content .= '<p>'.$additional_note.'<br/></p>';
	// }
	
	$content .= '<p><u><b>'._NOTE.'</b></u><br/>';
	if ($catatan != '') {
		
		$content .= $catatan.'<br/>'.$additional_note;
	} else {
		$content .= $additional_note.'<br/>';
	}
	$content .= '</p>';
	
	$berca_estore = (isset($_SESSION['member_uid']) && typeCompany($txid) == $berca_id) ? '<img src="'.$cfg_app_url.'/images/berca-estore.jpeg" style="height:23px" >' : '<img src="'.$cfg_app_url.'/images/berca-estore-bnm.png" style="height:32px" >';
	
	if ($is_from_template == 1) {
		$content .= '
		<p>
			<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
			<table>
				<tr>
					<td style="width: 150px;">Delivery Time</td>
					<td style="width: 550px;">: '.$deliverytime.'</td>
				</tr>
				<tr>
					<td>Term of Payment</td>
					<td>: '.$termofpayment.'</td>
				</tr>
				<tr>
					<td>Quotation Validity</td>
					<td>: '.$quotationvalidity.'</td>
				</tr>
				<tr>
					<td>Delivery Franco</td>
					<td>: '.$tc_delivery_franco.'</td>
				</tr>
				<tr>
					<td>Warranty</td>
					<td>: '.$tc_warranty.'</td>
				</tr>
			</table>
		</p>';
	} else {
		$content .= '
		<p>
			<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
			'.$termandcondition.'
		</p>';
	}
	$content .= '
	<!--<p>
		<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
		'._TERMOFPAYMENT.': '.$termofpayment.'<br/>
		'._DELIVERYTIME.': '.$deliverytime.'<br/>
		'._DELIVERYFRANCO.': '.$deliveryfranco.'<br/>
		'.(($warranty != '') ? _WARRANTY.': '.$warranty.'<br/>' : '').'
		'._QUOTATIONVALIDITY.': '.$quotationvalidity.'
	</p>
	
	<p>
		<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
		'.$termandcondition.'
	</p>-->
	

	<p>'._SINCERELYYOURS.',</p>
	<!--<img src="'.$cfg_app_url.'/images/berca-estore.png" height="60px" >-->
	<p>'.$berca_estore.'</p>
	
	<div class="automatically-generate" style="background-color:#eee;margin-bottom:30px;margin-top:30px;text-align:center;line-height:35px">'.$quotationvalidity.'</div>
	';
	
	$_pid = base64_encode($txid);
	
	if ($level2 == 1 || $level1 == 1) {
		
	} else {
		$content .= '<div class="downloadpdf"><a class="btn btn-default more downloadpdf-button" href="'.$urlfunc->makePretty("?p=order&action=downloadpdf&pid=$_pid").'" target="_blank"><img src="'.$cfg_app_url.'/images/pdf-icon.svg">'._DOWNLOADPDF.'</a></div>';
	}
	// die($siap_kirim);
	if ($siap_kirim == 1) {
		if ($quotation_status == 0) {
			$content .= '
			<br>
			<form id="quotatoiniswon" method="POST" action="#">
				<div class="panel panel-default">
				  <div class="panel-heading">
					Status Quotation:
					<select id="quotation_status" name="quotation_status">';
					foreach($quotation_status_arr as $idx => $status) {
						if ($idx == $quotation_status) {
							$content .= '<option value="'.$idx.'" selected>'.$status.'</option>';
						} else {
							$content .= '<option value="'.$idx.'">'.$status.'</option>';
						}
					}
					
					$is_won = ($quotation_status == 1) ? 'block' : 'none';
					$is_lost = ($quotation_status == 2 || $quotation_status == 3) ? '' : 'none';
			$content .= '
					</select>
					<button type="submit" name="simpanlost" class="btn btn-primary btn-default more" style="display:'.$is_lost.'">'._SAVE.'</button>
				</div>
				<div id="quotation_body" class="panel-body" style="display:'.$is_won.'">
					<input type="hidden" name="quotation_id" value="'.$txid.'">
					<div class="row">
					<div class="col-sm-4">
					<div class="form-group">
						<label for="customer" class="control-label">'._CUSTOMER.'</label>
						<input type="text" name="customer" id="customer" class="form-control" value="'.$customer_pic.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="namapic" class="control-label">'._NAMAPIC.'</label>
						<input type="text" name="namapic" class="form-control" value="'.$nama_pic.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="emailpic" class="control-label">'._EMAILPIC.'</label>
						<input type="text" name="emailpic" class="form-control" value="'.$email_pic.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="nomorpo" class="control-label">'._NOMORPO.'</label>
						<input type="text" name="nomorpo" class="form-control" value="'.$nomor_po.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="nomorjoborder" class="control-label">'._NOMORJOBORDER.'</label>
						<input type="text" name="nomorjoborder" class="form-control" value="'.$nomor_job_order.'">
					</div>
					</div>
					<div class="col-sm-4">
					<div class="form-group">
						<label for="nominaltransaksi" class="control-label">'._NOMINALTRANSAKSI.'</label>
						<input type="text" name="nominaltransaksi" class="form-control" value="'.(($nilai_transaksi > 0) ? number_format($nilai_transaksi, 0, ',', '.') : '').'">
					</div>
					</div>
					<div class="col-sm-12">
					<div class="form-group">
						<label for="detailproduk" class="control-label">'._DETAILPRODUK.'</label>
						<textarea name="detailproduk" cols="5" rows="10" class="form-control">'.$detail_produk.'</textarea>
					</div>
					</div>
					<div class="col-sm-12">
					<button type="submit" name="simpan" class="btn btn-primary btn-default more">'._SAVE.'</button>
					</div>
					</div>
				  </div>
				</div>
			</form>
			';
		} else {
			$content .= '
			<div class="panel panel-default">
				<div class="panel-heading">
					'._STATUSQUOTATION.': <strong>'.$quotation_status_arr[$quotation_status].'</strong>
				</div>';
			if ($quotation_status == 1) {
				$content .= '
					<div id="quotation_body" class="panel-body" style="display:'.$is_won.'">
						<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="customer" class="control-label">'._CUSTOMER.'</label>
								<p>'.$customer_pic.'</p>
							</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="namapic" class="control-label">'._NAMAPIC.'</label>
							<p>'.$nama_pic.'</p>
						</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="emailpic" class="control-label">'._EMAILPIC.'</label>
							<p>'.$email_pic.'</p>
						</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="nomorpo" class="control-label">'._NOMORPO.'</label>
							<p>'.$nomor_po.'</p>
						</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="nomorjoborder" class="control-label">'._NOMORJOBORDER.'</label>
							<p>'.$nomor_job_order.'</p>
						</div>
						</div>
						<div class="col-sm-4">
						<div class="form-group">
							<label for="nominaltransaksi" class="control-label">'._NOMINALTRANSAKSI.'</label>
							<p>'.(($nilai_transaksi > 0) ? number_format($nilai_transaksi, 0, ',', '.') : '').'</p>
						</div>
						</div>
						<div class="col-sm-12">
						<div class="form-group">
							<label for="detailproduk" class="control-label">'._DETAILPRODUK.'</label>
							<p>'.$detail_produk.'</p>
						</div>
						</div>
						</div>
					  </div>
					</div>
				';
			}
		}
	}
	
	
}

/* if ($action == 'history') {
	$title = _ORDERHISTORY;
	if ($_SESSION['member_uid']!='') {
		
		$sql = "SELECT tx.transaction_id, tx.tanggal_buka_transaksi, tx.status, tx.ongkir,tx.no_pengiriman, status_pembayaran FROM sc_transaction tx, sc_customer c WHERE c.web_user_id='".$_SESSION['member_uid']."' AND c.customer_id=tx.customer_id ORDER BY autoid DESC";
		
		$result = $mysqli->query($sql);
		$content .= "<table class=\"systemtable table-striped\" border=\"1\" width=\"100%\">\r\n";
		$content .= "<tr><th>"._CURRENTORDERTITLE."</th><th>"._STATUSPEMBAYARAN."</th><th>"._ORDERDATE."</th><th>"._TOTAL."</th><th>"._POSTAGE."</th><th>"._ORDERSTATUS."</th></tr>\r\n";
		while (list($txid, $txdate, $status, $ongkir,$no_pengiriman, $status_pembayaran) = $result->fetch_row()) {
			$sql1 = "SELECT jumlah, harga FROM sc_transaction_detail WHERE transaction_id='$txid'";
			$result1 = $mysqli->query($sql1);
			$total = 0;
			while (list($qty, $harga) = mysql_fetch_row($result1)) {
				$total = $total + $qty * $harga;
			}
			switch ($status) {
				case 0:
					$temp = _STATUS0;
					break;
				case 1:
					$temp = _STATUS1;
					break;
				case 2:
					$temp = _STATUS2;
					break;
				case 3:
					$temp = _STATUS3;
					break;
				case 4:
					$temp = _STATUS4." $no_pengiriman";
					break;
			}
			
			if ($status_pembayaran == 0) {
				$link_pembayaran = "<a href=\"".$urlfunc->makePretty("?p=pembayaran_gadget&action=add&id=$txid")."\">".$array_status_pembayaran[$status_pembayaran]."</a>";
			} else {
				$link_pembayaran = ($status_pembayaran < 3) ? "<a href=\"".$urlfunc->makePretty("?p=pembayaran_gadget&action=edit&id=$txid")."\">".$array_status_pembayaran[$status_pembayaran]."</a>" : $array_status_pembayaran[$status_pembayaran];
			}
			$content .= "<tr valign=\"top\"><td><a href=\"".$urlfunc->makePretty("?p=order&action=view&id=$txid")."\">$txid</td><td>$link_pembayaran</td><td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total,0,',','.')."</td><td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td></tr>\r\n";
		}
		$content .= "</table>\r\n";
	}
	
} */

if ($action == 'history') {
	$title = _ORDERHISTORY;
	if ($_SESSION['member_uid']!='') {
		
		// $sql = "SELECT tx.transaction_id, tx.tanggal_buka_transaksi, tx.status, tx.ongkir,tx.no_pengiriman, status_pembayaran, tx.nominal, tx.tanggal_konfirmasi FROM sc_transaction tx, sc_customer c WHERE c.web_user_id='".$_SESSION['member_uid']."' AND c.customer_id=tx.customer_id ORDER BY autoid DESC";
		
		$sql = "SELECT tx.transaction_id, tx.tanggal_buka_transaksi, tx.status, tx.ongkir,tx.no_pengiriman, status_pembayaran, tx.nominal, tx.tanggal_konfirmasi, kodevoucher, tipe_voucher, nominal_voucher, kurir FROM sc_transaction tx, sc_customer c WHERE c.web_user_id='".$_SESSION['member_uid']."' AND c.customer_id=tx.customer_id ORDER BY autoid DESC";
		
		$result = $mysqli->query($sql);
		$content .= "<table class=\"table systemtable table-striped\" border=\"0\" width=\"100%\">\r\n";
		$content .= "<thead><tr><th>"._CURRENTORDERTITLE."</th><th>"._ORDERDATE."</th><th>"._TOTAL."</th><th>"._POSTAGE."</th><th>"._ORDERSTATUS."</th></tr></thead><tbody>\r\n";
		while (list($txid, $txdate, $status, $ongkir,$no_pengiriman, $status_pembayaran, $nominal, $tanggal_konfirmasi, $kodevoucher, $tipe_voucher, $nominal_voucher, $kurir) = $result->fetch_row()) {
			$sql1 = "SELECT jumlah, harga FROM sc_transaction_detail WHERE transaction_id='$txid'";
			$result1 = $mysqli->query($sql1);
			$total = 0;
			while (list($quantity, $price) = $result1->fetch_row()) {
				// $total = $total + $qty * $harga;
				
				$line_cost = $quantity * $price;  //work out the line cost
				$total = $total + $line_cost;   //add to the total cost
			}
			switch ($status) {
				case 0:
					$temp = _STATUS0;
					break;
				case 1:
					$temp = _STATUS1;
					break;
				case 2:
					$temp = _STATUS2;
					break;
				case 3:
					$temp = _STATUS3;
					break;
				case 4:
					$temp = _STATUS4;
					break;
				case 5:
					$temp = _STATUS5." $no_pengiriman";
					break;
			}
			
			if ($nominal_voucher > 0) {
		
				if ($tipe_voucher == 1) {
					$voucher = number_format($nominal_voucher, 0, ',', '.');
					$disc = $total - $nominal_voucher;
					$total = $disc;
				} else if ($tipe_voucher == 2) {
					$disc = 100 - $nominal_voucher;
					$temp_total = (($disc/100) * $total);
					$voucher = (int)$nominal_voucher . '%';
					$total = $temp_total;
				}
			} else {
				$voucher = 0;
			}
			// $content .= "<tr>";
			// $content .= "<td colspan=\"4\" align=\"right\">"._DISCOUNT."</td>";
			// $content .= "<td align=\"right\">".$voucher."</td>";
			// $content .= "</tr>";
			
			if ($insurancevalue > 0) $total += $insurancevalue;
			// $content .= "<tr>";
			// $content .= "<td colspan=\"4\" align=\"right\">"._USEINSURANCE."</td>";
			// $content .= "<td align=\"right\">".number_format($insurancevalue, 0, ',', '.')."</td>";
			// $content .= "</tr>";
			
			if ($packingvalue > 0) $total += $packingvalue;
			// $content .= "<tr>";
			// $content .= "<td colspan=\"4\" align=\"right\">"._USEPACKING."</td>";
			// $content .= "<td align=\"right\">".number_format($packingvalue, 0, ',', '.')."</td>";
			// $content .= "</tr>";
				
			//START MENENTUKAN PENGGUNAAN ONGKOS KIRIM
			if ($pakaiongkir) {
				// $content .= "<tr>";
				// $content .= "<td colspan=\"4\" align=\"right\">"._POSTAGE." ($kurir_tipe)</td>";
				// $content .= "<td align=\"right\">".number_format($shipping, 0, ',', '.')."</td>";
				// $content .= "</tr>";
				$total = $total + $shipping;
			}
			//END MENENTUKAN PENGGUNAAN ONGKOS KIRIM
			
			// if ($status_pembayaran == 0) {
				// $link_pembayaran = "<a href=\"".$urlfunc->makePretty("?p=pembayaran_gadget&action=add&id=$txid")."\">".$array_status_pembayaran[$status_pembayaran]."</a>";
			// } else {
				// $link_pembayaran = ($status_pembayaran < 3) ? "<a href=\"".$urlfunc->makePretty("?p=pembayaran_gadget&action=edit&id=$txid")."\">".$array_status_pembayaran[$status_pembayaran]."</a>" : $array_status_pembayaran[$status_pembayaran];
			// }
			
			$temp_url = base64_encode($txid);
			if ($status == 2 && $nominal == 0 && $tanggal_konfirmasi == '0000-00-00') {
				$temp ="<a href=\"".$urlfunc->makePretty("?p=order&action=confirm_payment&id=$temp_url")."\"><button type='button'>"._CONFIRMPAYMENT."</button></a>";
			}
			
			$content .= "<tr valign=\"top\"><td><a href=\"".$urlfunc->makePretty("?p=order&action=view&id=$temp_url")."\">$txid</td><td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total,0,',','.')."</td><td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td></tr>\r\n";
		}
		$content .= "</tbody></table>\r\n";
	}
	
}

if ($action == 'history_quotation') {
	$title = _QUOTATIONHISTORY;
	if ($_SESSION['member_uid']!='') {
		
		// $sql = "SELECT tx.transaction_id, tx.tanggal_buka_transaksi, tx.status, tx.ongkir,tx.no_pengiriman, status_pembayaran, tx.nominal, tx.tanggal_konfirmasi FROM sc_transaction tx, sc_customer c WHERE c.web_user_id='".$_SESSION['member_uid']."' AND c.customer_id=tx.customer_id ORDER BY autoid DESC";
		
		$url = $urlfunc->makePretty("?p=order&action=history_quotation&status_quo=");
		$content .= '<h3>'._STATUSQUOTATION.'</h3><select onchange="location = this.options[this.selectedIndex].value" style="margin-bottom: 20px;vertical-align: top;margin-right: 10px;width: 150px;">';
		
		foreach($quotation_status_arr_front as $idx => $sort) {
			if ($idx == $status_quo && $status_quo != '') {
				$content .= '<option value="'.$url.$idx.'" selected>'.$sort.'</option>';
			} else {
				$content .= '<option value="'.$url.$idx.'">'.$sort.'</option>';
			}
		}
		$content .= '</select>';
	
		$where_condition = ($status_quo != '') ? " AND status_quotation='$status_quo'" : '';
		$sql = "SELECT tx.quotation_id, tx.tanggal_buka_transaksi, tx.status, tx.ongkir,tx.no_pengiriman, status_pembayaran, tx.nominal, tx.tanggal_konfirmasi, kodevoucher, tipe_voucher, nominal_voucher, kurir, discount, siap_kirim, level1, level2, is_from_template FROM sc_quotation tx, sc_customer c WHERE c.web_user_id='".$_SESSION['member_uid']."' AND c.customer_id=tx.customer_id $where_condition ORDER BY autoid DESC";
		$result = $mysqli->query($sql);
		$content .= "<table class=\"table systemtable table-striped\" border=\"0\" width=\"100%\">\r\n";
		$content .= "<thead><tr><th>"._CURRENTORDERTITLE."</th><th>"._ORDERDATE."</th><th class=\"text-right\">"._TOTAL."</th><!--<th>"._POSTAGE."</th><th>"._ORDERSTATUS."</th>--></tr></thead><tbody>\r\n";
		
		if ($result->num_rows > 0) {
			
			$urldownload = $urlfunc->makePretty("?p=order&action=downloadquotation&status_quo=$status_quo");
			$content .= "<a class=\"btn btn-primary\" href=\"$urldownload\">"._DOWNLOADEXCEL."</a>";
			
			while (list($txid, $txdate, $status, $ongkir,$no_pengiriman, $status_pembayaran, $nominal, $tanggal_konfirmasi, $kodevoucher, $tipe_voucher, $nominal_voucher, $kurir, $order_discount, $siap_kirim, $level1, $level2, $is_from_template) = $result->fetch_row()) {
				// $sql1 = "SELECT jumlah, harga FROM sc_quotation_detail WHERE quotation_id='$txid'";
				
				// if ($is_from_template == 1) {
					// $sql = "SELECT jumlah,harga,product_id,product_name, filename, diskon, disc_per_item FROM sc_quotation_detail qd INNER JOIN sc_quotation q ON qd.quotation_id=q.quotation_id WHERE qd.quotation_id='$txid' AND is_from_template=1 ORDER BY quotation_detail_id";
					// $result = $mysqli->query($sql);
					
					// while (list($quantity, $price, $product_id, $productname,$filename, $discount, $price_dics, $disc_per_item) = $result->fetch_row()) {
						// $line_cost = $price * $quantity;
						// $total = $total + $line_cost;   //add to the total cost
						// $product_id = ($product_id == 0) ? '' : $product_id;
						// // $content .= '
						// // <tr>
							// // <td style="text-align:center">'.$product_id.'</td>
							// // <td>
								// // <p class="product-name"><strong>'.$product_code.'</strong></p>
								// // <span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
							// // </td>
							// // <td style="text-align:right">'.number_format($price, 0, ',', '.').'</td>
							// // <td style="text-align:center">'.$discount.'</td>
							// // <td style="text-align:center">'.$quantity.'</td>
							// // <td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
						// // </tr>
						// // ';
						
						
						// $content .= "<tr valign=\"top\">";
						// $content .= "<td><a href=\"".$urlfunc->makePretty("?p=order&action=view_quotation&id=$temp_url")."\">$txid</td>";
						// $content .= "<td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total_price,0,',','.')."</td><!--<td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td>--></tr>\r\n";
					// }
				// } else {
					
					// $sql1 = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
					
					if ($is_from_template == 1) {
						$sql1 = "SELECT quotation_detail_id, jumlah,td.harga,product_name,td.filename, '' as merek, td.diskon,  '' as keterangan, '' as sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, parent, is_request_disc  FROM sc_quotation_detail td WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
					} else {
						$sql1 = "SELECT quotation_detail_id, jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
					}
					$result1 = $mysqli->query($sql1);
					$total = 0;
					while (list($quotation_detail_id, $quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item) = $result1->fetch_row()) {
						// $total = $total + $qty * $harga;
						
						// $line_cost = $quantity * $price;  //work out the line cost
						// $total = $total + $line_cost;   //add to the total cost
						
						// // 05/03/2020 14:57
						// $line_cost = $quantity * $price;  //work out the line cost
						// $disc = $price * (int)$disc_per_item / 100;
						// $harga_setelah_disc = $disc * $quantity;
						// $price_dics = $line_cost - $harga_setelah_disc;
						// $total += $price_dics;
						
						if ($is_from_template == 1) {
							if ($discount != '') {
								$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
								// $line_cost = $quantity * $price;  //work out the line cost
								// $disc = $price * (int)$discount / 100;
								// $harga_setelah_disc = $disc * $quantity;
								
								$line_cost = ($quantity * $price) - (($quantity * $price) * (int)$discount / 100);
								$total += $line_cost;
							} else {
								$line_cost = $quantity * $price;  //work out the line cost
								$total += $line_cost;
							}
						} else {
							$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
							$line_cost = $quantity * $price;  //work out the line cost
							$disc = $price * (int)$discount / 100;
							$harga_setelah_disc = $disc * $quantity;
							
							// $price_dics = $line_cost - $harga_setelah_disc;
							if ($disc_per_item != '') {
								$price_dics = $line_cost - $harga_setelah_disc;
								$line_cost = $price_dics;
							} /* else {
								// $price_dics = 0;
							} */
							$total += $price_dics;
						}
						
						
						
						// if ($discount != '' && $disc_per_item == '') {
							// $line_cost = $pricenormal * $quantity;
							// $discount = $disc_per_item;
							
							// $nominal = str_replace('%', '', $disc_per_item);
							// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							// $disc = 100 - $nominal;
							// $temp = (($disc/100) * $line_cost);
							// $line_cost = $temp;
							
							// /* if (substr_count($disc_per_item, '%') == 1) {
								// $discount = $disc_per_item;
								// $nominal = str_replace('%', '', $disc_per_item);
								// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
								
								// $disc = 100 - $nominal;
								// $temp = (($disc/100) * $line_cost);
								// $line_cost = $temp;
							// } else {
								// $discount = $disc_per_item;
								// // $discount = number_format($discount, 0, ',', '.');
								// $price_dics = $pricenormal - $disc_per_item;
								
								// $line_cost = $price_dics * $quantity;
							// } */
						// } else if ($discount != '' && $disc_per_item != '') {
							// $line_cost = $pricenormal * $quantity;
							// $discount = $disc_per_item;
							
							// $nominal = str_replace('%', '', $disc_per_item);
							// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							// $disc = 100 - $nominal;
							// $temp = (($disc/100) * $line_cost);
							// $line_cost = $temp;
								
							// /* if (substr_count($disc_per_item, '%') == 1) {
								// $discount = $disc_per_item;
								// $nominal = str_replace('%', '', $disc_per_item);
								// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
								
								// $disc = 100 - $nominal;
								// $temp = (($disc/100) * $line_cost);
								// $line_cost = $temp;
							// } else {
								// $discount = $disc_per_item;
								// // $discount = number_format($discount, 0, ',', '.');
								// $price_dics = $pricenormal - $disc_per_item;
								
								// $line_cost = $price_dics * $quantity;
							// } */
						// } else if ($discount == '' && $disc_per_item != '') {
							// $line_cost = $pricenormal * $quantity;
							// $discount = $disc_per_item;
							
							// $nominal = str_replace('%', '', $disc_per_item);
							// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							// $disc = 100 - $nominal;
							// $temp = (($disc/100) * $line_cost);
							// $line_cost = $temp;
								
							// /* if (substr_count($disc_per_item, '%') == 1) {
								// $discount = $disc_per_item;
								// $nominal = str_replace('%', '', $disc_per_item);
								// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
								
								// $disc = 100 - $nominal;
								// $temp = (($disc/100) * $line_cost);
								// $line_cost = $temp;
							// } else {
								// // $discount = number_format($discount, 0, ',', '.');
								// $price_dics = $pricenormal - $disc_per_item;
								
								// $line_cost = $price_dics * $quantity;
							// } */
						// } else if ($discount == '') {
							// $line_cost = $pricenormal * $quantity;
							// $discount = '';
						// } else {
							// $line_cost = $price_dics * $quantity;  //work out the line cost
						// }
						// // $html .= "$price_dics = $pricenormal - $disc_per_item<br>";
						// // $html .= "$line_cost = $price_dics * $quantity<br>";
						// $total = $total + $line_cost;   //add to the total cost
						
					}
					switch ($status) {
						case 0:
							$temp = _STATUS0;
							break;
						case 1:
							$temp = _STATUS1;
							break;
						case 2:
							$temp = _STATUS2;
							break;
						case 3:
							$temp = _STATUS3;
							break;
						case 4:
							$temp = _STATUS4." $no_pengiriman";
							break;
					}
					
					if ($nominal_voucher > 0) {
				
						if ($tipe_voucher == 1) {
							$voucher = number_format($nominal_voucher, 0, ',', '.');
							$disc = $total - $nominal_voucher;
							$total = $disc;
						} else if ($tipe_voucher == 2) {
							$disc = 100 - $nominal_voucher;
							$temp_total = (($disc/100) * $total);
							$voucher = (int)$nominal_voucher . '%';
							$total = $temp_total;
						}
					} else {
						$voucher = 0;
					}
					
					// VAT
					$discount = ($total*10)/100;
					$total_price = ($discount > 0) ? $total+floatval($discount) : $total;
					$total_price = floatval($total_price);
					
					if ($order_discount != '') {
						// $voucher = number_format($nominal_voucher, 0, ',', '.');
						// $total_price = $total_price - $nominal_voucher;
						
						if (substr_count($order_discount, '%') == 1) {
							$voucher = $order_discount;
							$disc = $total*$voucher/100;
							$total = $total - $disc;
							$subtotal = $total;
						} else {
							$voucher = number_format($order_discount, 0, ',', '.');
							$total = $total - $order_discount;
							$subtotal = $total;
						}
						
						$discount = ($subtotal*10)/100;
						$total_price = ($discount > 0) ? $subtotal+floatval($discount) : $subtotal;
						$total_price = floatval($total_price);
					}
					
					$temp_url = base64_encode($txid);
					
					if ($level2 == 1 || $level1 == 1) {
						$txid = $quotation_number_pending;
					}
					
					
					// if ($is_from_template == 1) {
						// $sql = "SELECT jumlah,harga,product_id,product_name, filename, diskon, disc_per_item FROM sc_quotation_detail qd INNER JOIN sc_quotation q ON qd.quotation_id=q.quotation_id WHERE qd.quotation_id='$txid' AND is_from_template=1 ORDER BY quotation_detail_id";
						// $result = $mysqli->query($sql);
						// $total = 0;
						// while (list($quantity, $price, $product_id, $productname,$filename, $discount, $price_dics, $disc_per_item) = $result->fetch_row()) {
							// $line_cost = $price * $quantity;
							// $total = $total + $line_cost;   //add to the total cost
							// // $product_id = ($product_id == 0) ? '' : $product_id;
							// // $content .= '
							// // <tr>
								// // <td style="text-align:center">'.$product_id.'</td>
								// // <td>
									// // <p class="product-name"><strong>'.$product_code.'</strong></p>
									// // <span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
								// // </td>
								// // <td style="text-align:right">'.number_format($price, 0, ',', '.').'</td>
								// // <td style="text-align:center">'.$discount.'</td>
								// // <td style="text-align:center">'.$quantity.'</td>
								// // <td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
							// // </tr>
							// // ';
							
							
							// $content .= "<tr valign=\"top\">";
							// $content .= "<td><a href=\"".$urlfunc->makePretty("?p=order&action=view_quotation&id=$temp_url")."\">$txid</td>";
							// $content .= "<td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total_price,0,',','.')."</td><!--<td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td>--></tr>\r\n";
						// }
						
						// $total_price = $total;
					// }
					
					$content .= "<tr valign=\"top\">";
					$content .= "<td><a href=\"".$urlfunc->makePretty("?p=order&action=view_quotation&id=$temp_url")."\">$txid</td>";
					$content .= "<td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total_price,0,',','.')."</td><!--<td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td>--></tr>\r\n";
			}
			} else {
				$content .= '<tr class="text-center"><td colspan="3" >'._QUOTATIONNOTFOUND.'</td></tr>';
			}
		$content .= "</tbody></table>\r\n";
	} else {
		$content .= _NORIGHT;
	}
	
}

if ($action == 'history_price_information') {
	$title = _PRICEINFORMATIONHISTORY;
	if ($_SESSION['member_uid']!='') {
		
		$sql = "SELECT tx.price_information_id, tx.tanggal_buka_transaksi, tx.status, tx.ongkir,tx.no_pengiriman, status_pembayaran, tx.nominal, tx.tanggal_konfirmasi, kodevoucher, tipe_voucher, nominal_voucher, kurir, discount, siap_kirim, level1, level2, is_from_template FROM sc_price_information tx, sc_customer c WHERE c.web_user_id='".$_SESSION['member_uid']."' AND c.customer_id=tx.customer_id $where_condition ORDER BY autoid DESC";
		$result = $mysqli->query($sql);
		$content .= "<table class=\"table systemtable table-striped\" border=\"0\" width=\"100%\">\r\n";
		$content .= "<thead><tr><th>"._CURRENTORDERTITLE."</th><th>"._ORDERDATE."</th><th class=\"text-right\">"._TOTAL."</th><!--<th>"._POSTAGE."</th><th>"._ORDERSTATUS."</th>--></tr></thead><tbody>\r\n";
		
		if ($result->num_rows > 0) {
			
			while (list($txid, $txdate, $status, $ongkir,$no_pengiriman, $status_pembayaran, $nominal, $tanggal_konfirmasi, $kodevoucher, $tipe_voucher, $nominal_voucher, $kurir, $order_discount, $siap_kirim, $level1, $level2, $is_from_template) = $result->fetch_row()) {
				
					$sql1 = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, price_information FROM sc_price_information_detail td LEFT JOIN catalogdata c ON c.id=td.product_id WHERE price_information_id='$txid' ORDER BY price_information_detail_id";
					$result1 = $mysqli->query($sql1);
					$total = 0;
					while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $price_information) = $result1->fetch_row()) {
						
						$line_cost = $price_information * $quantity;  //work out the line cost
						// $html .= "$price_dics = $pricenormal - $disc_per_item<br>";
						// $html .= "$line_cost = $price_dics * $quantity<br>";
						$total = $total + $line_cost;   //add to the total cost
						
					}
					switch ($status) {
						case 0:
							$temp = _STATUS0;
							break;
						case 1:
							$temp = _STATUS1;
							break;
						case 2:
							$temp = _STATUS2;
							break;
						case 3:
							$temp = _STATUS3;
							break;
						case 4:
							$temp = _STATUS4." $no_pengiriman";
							break;
					}
					
					if ($nominal_voucher > 0) {
				
						if ($tipe_voucher == 1) {
							$voucher = number_format($nominal_voucher, 0, ',', '.');
							$disc = $total - $nominal_voucher;
							$total = $disc;
						} else if ($tipe_voucher == 2) {
							$disc = 100 - $nominal_voucher;
							$temp_total = (($disc/100) * $total);
							$voucher = (int)$nominal_voucher . '%';
							$total = $temp_total;
						}
					} else {
						$voucher = 0;
					}
					
					// VAT
					// $discount = ($total*10)/100;
					$total_price = ($discount > 0) ? $total+round($discount) : $total;
					$total_price = round($total_price);
					
					/* if ($order_discount != '') {
						// $voucher = number_format($nominal_voucher, 0, ',', '.');
						// $total_price = $total_price - $nominal_voucher;
						
						if (substr_count($order_discount, '%') == 1) {
							$voucher = $order_discount;
							$disc = $total*$voucher/100;
							$total = $total - $disc;
							$subtotal = $total;
						} else {
							$voucher = number_format($order_discount, 0, ',', '.');
							$total = $total - $order_discount;
							$subtotal = $total;
						}
						
						$discount = ($subtotal*10)/100;
						$total_price = ($discount > 0) ? $subtotal+round($discount) : $subtotal;
						$total_price = round($total_price);
					} */
					
					$temp_url = base64_encode($txid);
					
					if ($level2 == 1 || $level1 == 1) {
						$txid = $quotation_number_pending;
					}
					
					
					$content .= "<tr valign=\"top\">";
					$content .= "<td><a href=\"".$urlfunc->makePretty("?p=order&action=view_price_information&id=$temp_url")."\">$txid</td>";
					$content .= "<td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total_price,0,',','.')."</td><!--<td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td>--></tr>\r\n";
			}
			} else {
				$content .= '<tr class="text-center"><td colspan="3" >'._QUOTATIONNOTFOUND.'</td></tr>';
			}
		$content .= "</tbody></table>\r\n";
	} else {
		$content .= _NORIGHT;
	}
	
}

if ($action == 'downloadpdf') {
	#region quotation pdf
	ob_start();
	$txid = base64_decode($_GET['pid']);
	$type = fiestolaundry($_GET['type'], 3);
	
	$str_filename = str_replace('/','_',$txid);
	$filename = "$str_filename.pdf";
	$file = "$cfg_pdf_path/$filename";
	
	if (file_exists($file)) {
		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename="' . $filename . '"');
		header('Content-Transfer-Encoding: binary');
		header('Accept-Ranges: bytes');
		echo file_get_contents($file);
	} else {
		
		// Include the main TCPDF library (search for installation path).
		require_once('aplikasi/tcpdf/config/tcpdf_include.php');

		// Extend the TCPDF class to create custom Header and Footer
		class MYPDF extends TCPDF {

			//Page header
			public function Header() {
				// Logo
				global $berca_id, $txid, $mysqli;
				
				$berca_letterhead = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? 'header-kop.png' : 'header-kop-bnm.png';
				$image_file = "images/$berca_letterhead"; // *** Very IMP: make sure this image is available on given path on your server
									
				$this->Image($image_file,16,8,178);
				// Set font
				$this->SetFont('dejavusans', '', 10);
				
				if ($this->page == 1) {
					// $berca_letterhead = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? 'header-kop.png' : 'header-kop-bnm.png';
					// $image_file = "images/$berca_letterhead"; // *** Very IMP: make sure this image is available on given path on your server
										
					// $this->Image($image_file,16,8,178);
					// // Set font
					// $this->SetFont('helvetica', 'C', 12);
					
				} else {
					
					$sql = "SELECT tanggal_buka_transaksi FROM sc_quotation WHERE quotation_id='$txid'";
					$result = $mysqli->query($sql);
					list($tanggal_buka_transaksi) = $result->fetch_row();
					
					// $txt = '
					

					
					
					// '._QUOTATIONDATE.': '.tglformat($tanggal_buka_transaksi).'
					// '._QUOTATIONNUMBER.': '.$txid;
					
					$txt = '
					<table>
						<tr>
						<td width="418px">&nbsp;</td>
						<td>
							<table cellpadding="5" border="1" style="text-align:left;width:290px;">
								<tr><td style="width:70px;">'._QUOTATIONDATE.'</td><td style="">: '.tglformat($tanggal_buka_transaksi).'</td></tr>
								<tr><td style="width:70px;">'._QUOTATIONNUMBER.'</td><td style="">: '.$txid.'</td></tr>
							</table>
						</td>
						</tr>
					</table>
					';

					// print a block of text using Write()
					// $this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
					// $this->writeHTMLCell(0, 0, '', '', $txt, 0, 0, false, "L", true);
					$this->SetTopMargin(55);
					$this->SetY(35);
					$this->writeHTML($txt, true, false, true, false, 'R');
				}
			}
			
			public function Footer() {
				global $txt_footer_pdf, $txt_footer_bnm_pdf, $berca_id, $type, $cfg_app_url;
				
				// Position at 15 mm from bottom
				$this->SetY(-15);
				// Set font
				$this->SetFont('dejavusans', 'I', 8);
				// Page number
				
				// $berca_footer_pdf = ($type == $berca_id) ? $txt_footer_pdf : $txt_footer_bnm_pdf;
				// $this->writeHTMLCell(0, 0, '', '', $berca_footer_pdf, 0, 0, false, "L", true);
				
				if (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) {
					$this->writeHTMLCell(0, 0, '', '', $txt_footer_pdf, 0, 0, false, "L", true);
					$this->Cell(7, 17, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
				} else {
					// $this->writeHTMLCell(0, '', $this->SetX(5), $this->SetY(-28), $txt_footer_bnm_pdf, 0, 0, false, true, 'L', false);
					
					$this->SetY(-25);
					
					$logoX = 15; // 186mm. The logo will be displayed on the right side close to the border of the page
					$logoFileName = "$cfg_app_url/images/footerbnm.jpg";
					$logoWidth = 15; // 15mm

					$logo = $this->Image($logoFileName, $logoX, $this->GetY()+2, 178, 19, '','','',true,300,'C');
					$this->Cell(365, 38, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
					$this->SetX($this->w - $this->documentRightMargin - $logoWidth); // documentRightMargin = 18
					$this->writeHTMLCell(0, 0, '', '', $logo, 0, 0, false, "L", true);
				}
									
			}

		}

		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
				
		// set document information
		// $pdf->SetCreator('Fiesto');
		// $pdf->SetAuthor('Fiesto');
		$pdf->SetTitle(_EQUOTATION . ' #' . $txid);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		
		$pdf->SetAutoPageBreak(true, 30);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP +17, PDF_MARGIN_RIGHT);

		// set image scale factor
		// $pdf->SetAutoPageBreak(TRUE, 30)
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('dejavusans', '', 10, '', true);

		// add a page
		$pdf->AddPage();
		$sql = "SELECT tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher,kurir, discount, termofpayment, deliverytime, quotationvalidity, termandcondition, is_from_template, tc_warranty, tc_delivery_franco FROM sc_quotation WHERE quotation_id='$txid'";
		$result = $mysqli->query($sql);
		if ($result->num_rows == 0) {
			$url = $urlfunc->makePretty("?p=order&action=history_quotation");
			header("Location: $url");
			exit();
		} else {
			list($tanggal_buka_transaksi, $tanggal_tutup_transaksi, $customer_id, $receiver_id, $metode_pembayaran, $shipping, $status, $catatan, $no_pengiriman, $latestupdate, $kodevoucher, $tipe_voucher, $nominal_voucher,$kurir, $order_discount, $customTermOfPayment, $customDeliveryTime, $customQuotationValidity, $customQuotationTC, $is_from_template, $tc_warranty, $tc_delivery_franco) = $result->fetch_row();
			
			$couriers 		= json_decode($kurir, true);
			$kecamatan 		= $couriers['kota'];
			$kurir 			= $couriers['tipe'] . ' ' . $couriers['kota'];
			$kurir_tipe		= $couriers['tipe'];
			$ongkos_kirim 	= number_format($shipping, 0, ',', '.');
			$estimasi 		= $couriers['estimasi'];
			
			$content .= "<h2>"._EQUOTATION."</h2>\r\n";
			
			$sql = "SELECT nama_lengkap, email, company, alamat, alamat2, kota, provinsi, zip, telepon, cellphone1, cellphone2, nama_pengirim, telp_pengirim, email_pengirim, pic FROM sc_receiver WHERE receiver_id='$receiver_id'";
			$result = $mysqli->query($sql);
			list($nama_lengkap, $email, $company, $alamat, $alamat2, $kota, $provinsi, $zip, $telepon, $cellphone1, $cellphone2, $sendername, $senderphone, $senderemail, $pic) = $result->fetch_row();
			$temp_address = ($alamat2 != '') ? "$alamat<br>$alamat2" : $alamat;
			
			if ($pic != '') {
				$temp_pic = '
				<tr>
					<td valign="top">'._QUOTATIONPREPAREBY.'</td>
					<td valign="top">:</td>
					<td>'.$pic.'</td>
				</tr>';
			} else {
				$temp_pic = '';
			}
			// if(
				// ($jumlah_product>=3 && $jumlah_product<=8) or
				// ($jumlah_product>=16 && $jumlah_product<=20) or 
				// ($jumlah_product>=33 && $jumlah_product<=46) 
			// ){
				// $pdf->addPage();
				// $html = '<p style="line-height:3">&nbsp;</p>';
				// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			// }
			// if ($this->page == 1) {
				
			// } else {
				// $html = '<p style="line-height:3">&nbsp;</p>';
			// }
			$html .= '
			<h1>'._EQUOTATION.'</h1>
			<div class="table_block table-responsive table-small-gap">
				<table>
					<tr>
						<td width="48%">
							<table>
								<tr valign="top">
									<td width="10%">'._QUOTATIONTO.'</td>
									<td width="8%" align="left">:</td>
									<td width="82%">
										'.$company.'<br>
										'.$temp_address.'<br>
										'.$kota.'&nbsp;'.$zip.'<br>
										Phone: '.$cellphone1.'
										<p style="line-height:0.8;">&nbsp;</p>
									</td>
								</tr>
								<tr valign="top">
									<td>'._QUOTATIONATTN.'</td>
									<td>:</td>
									<td>
										'.$nama_lengkap.'<br>
										'.$email.'
									</td>
								</tr>
							</table>
						</td>
						<td width="4%">&nbsp;</td>
						<td width="48%">
							<table>
								<tr>
									<td width="27%">'._QUOTATIONDATE.'</td>
									<td width="3%">:</td>
									<td width="70%">'.tglformat($tanggal_buka_transaksi).'</td>
								</tr>
								<tr>
									<td>'._QUOTATIONNUMBER.'</td>
									<td>:</td>
									<td>'.$txid.'</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								'.$temp_pic.'
							</table>
						</td>
					</tr>
				</table>
				<p style="line-height:0.6;">&nbsp;</p>';
				
				$html .= '
				<table border="0" class="table table-bordered" width="100%" cellpadding="7">
					<tr style="background-color:#f5f5f5;border-bottom: 1px solid #aaa;font-weight:bold">
						<th style="text-align:center" width="8%" style="border-bottom: 1px solid #aaa;">'._ITEM.'</th>
						<th style="text-align:center" width="35%" style="border-bottom: 1px solid #aaa;">'._PARTNUMBER.'</th>
						<th style="text-align:center" width="20%" style="border-bottom: 1px solid #aaa;">'._UNITPRICE.'</th>
						<th style="text-align:center" align="center" width="10%" style="text-align:center;border-bottom: 1px solid #aaa;">'._DISCOUNT.'</th>
						<!--<th style="text-align:center" width="15%" align="center" style="border-bottom: 1px solid #aaa;">'._SUBTOTAL.'</th>-->
						<!--<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._QTY.'</th>-->
						<th style="text-align:center" width="7%" align="center" style="border-bottom: 1px solid #aaa;">Qty</th>
						<th style="text-align:right" width="20%" style="border-bottom: 1px solid #aaa;">'._TOTAIDR.'</th>
					</tr>
				';
				
				$colSpan = 5;
				$item = 0;
				$total = 0;
				$temp_files = array();

				if ($is_from_template == 1) {
					$sql = "SELECT jumlah,harga,product_id,product_name, filename, diskon, disc_per_item FROM sc_quotation_detail qd INNER JOIN sc_quotation q ON qd.quotation_id=q.quotation_id WHERE qd.quotation_id='$txid' AND is_from_template=1 ORDER BY quotation_detail_id";
					$result = $mysqli->query($sql);
					while (list($quantity, $price, $product_id, $productname,$filename, $discount, $price_dics, $disc_per_item) = $result->fetch_row()) {
						$line_cost = $price * $quantity;
						$total = $total + $line_cost;   //add to the total cost
						$product_id = ($product_id == 0) ? '' : $product_id;
						$html .= '
						<tr>
							<td style="text-align:center">'.$product_id.'</td>
							<td>
								<p class="product-name"><strong>'.$product_code.'</strong></p>
								<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
							</td>
							<td style="text-align:right">'.number_format($price, 0, ',', '.').'</td>
							<td style="text-align:center">'.$discount.'</td>
							<td style="text-align:center">'.$quantity.'</td>
							<td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
						</tr>
						';
					}
				} else {
					// $sql = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, c.pdf FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id <> $cat_loose_item ORDER BY quotation_detail_id";
					$sql = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, td.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, c.pdf FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id <> $cat_loose_item ORDER BY quotation_detail_id";
					
					$result = $mysqli->query($sql);
					$jumlah_product = mysqli_num_rows($result);
					while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $productPdf) = $result->fetch_row()) {
						$brand_name = get_brand_name($idmerek);
						// $line_cost = $quantity * $price_dics;  //work out the line cost
						
						if ($productPdf != '' && file_exists("$cfg_img_path/catalog/pdf/$productPdf")) {
							$temp_files[] = "$cfg_img_path/catalog/pdf/$productPdf";
						}
						
						if ($discount != '' && $disc_per_item == '') {
							$line_cost = $pricenormal * $quantity;
							$discount = $disc_per_item;
							
							$nominal = str_replace('%', '', $disc_per_item);
							$price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							$disc = 100 - $nominal;
							$temp = (($disc/100) * $line_cost);
							$line_cost = $temp;
							
						} else if ($discount != '' && $disc_per_item != '') {
							$line_cost = $pricenormal * $quantity;
							$discount = $disc_per_item;
							
							$nominal = str_replace('%', '', $disc_per_item);
							$price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							$disc = 100 - $nominal;
							$temp = (($disc/100) * $line_cost);
							$line_cost = $temp;
											
						} else if ($discount == '' && $disc_per_item != '') {
							$line_cost = $pricenormal * $quantity;
							$discount = $disc_per_item;
							
							$nominal = str_replace('%', '', $disc_per_item);
							$price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							$disc = 100 - $nominal;
							$temp = (($disc/100) * $line_cost);
							$line_cost = $temp;
							
						} else if ($discount == '') {
							$line_cost = $pricenormal * $quantity;
							$discount = '';
						} else {
							$line_cost = $price_dics * $quantity;  //work out the line cost
						}
						
						if ($discount != '') {
							$discount = round($discount).'%';
						}
						
						// $html .= "$price_dics = $pricenormal - $disc_per_item<br>";
						// $html .= "$line_cost = $price_dics * $quantity<br>";
						$total = $total + $line_cost;   //add to the total cost
						
						
						$item++;
						$html .= '
						<tr>
							<td style="text-align:center">'.$item.'</td>
							<td>
								<p class="product-name">
								<strong>'.$brand_name.' '.$product_code.'</strong><br/>
								<medium class="description-product"><i>'.htmlentities($productname, ENT_QUOTES).'</i></medium>
								</p>
							</td>
							<td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
							<td style="text-align:center">'.$discount.'</td>
							<!--<td style="text-align:right">'.number_format($price_dics, 0, ',', '.').'</td>-->
							<td style="text-align:center">'.$quantity.'</td>
							<td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
						</tr>
						';
					}
					
					// Loose item
					// $sql = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, c.pdf FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id = $cat_loose_item ORDER BY quotation_detail_id";
					$sql = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, c.harganormal, disc_per_item, c.pdf FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id = $cat_loose_item ORDER BY quotation_detail_id";
					$result = $mysqli->query($sql);
					if ($result->num_rows > 0) {
						$html .= "<tr style=\"background-color: #f5f5f5;font-weight: bold;\"><td colspan=\"6\" style=\"border-bottom:1px solid #aaa\">"._LOOSEITEM."</td></tr>";
						while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $productPdf) = $result->fetch_row()) {
							$brand_name = get_brand_name($idmerek);
							// $line_cost = $quantity * $price_dics;  //work out the line cost
							
							if ($productPdf != '' && file_exists("$cfg_img_path/catalog/pdf/$productPdf")) {
								$temp_files[] = "$cfg_img_path/catalog/pdf/$productPdf";
							}
							
							if ($discount != '' && $disc_per_item == '') {
								$line_cost = $pricenormal * $quantity;
								$discount = $disc_per_item;
								
								$nominal = str_replace('%', '', $disc_per_item);
								$price_dics = $pricenormal - ($pricenormal*$nominal/100);
								
								$disc = 100 - $nominal;
								$temp = (($disc/100) * $line_cost);
								$line_cost = $temp;
								
							} else if ($discount != '' && $disc_per_item != '') {
								$line_cost = $pricenormal * $quantity;
								$discount = $disc_per_item;
								
								$nominal = str_replace('%', '', $disc_per_item);
								$price_dics = $pricenormal - ($pricenormal*$nominal/100);
								
								$disc = 100 - $nominal;
								$temp = (($disc/100) * $line_cost);
								$line_cost = $temp;
												
							} else if ($discount == '' && $disc_per_item != '') {
								$line_cost = $pricenormal * $quantity;
								$discount = $disc_per_item;
								
								$nominal = str_replace('%', '', $disc_per_item);
								$price_dics = $pricenormal - ($pricenormal*$nominal/100);
								
								$disc = 100 - $nominal;
								$temp = (($disc/100) * $line_cost);
								$line_cost = $temp;
								
							} else if ($discount == '') {
								$line_cost = $pricenormal * $quantity;
								$discount = '';
							} else {
								$line_cost = $price_dics * $quantity;  //work out the line cost
							}
							
							if ($discount != '') {
								$discount = intval($discount).'%';
							}
							
							// $html .= "$price_dics = $pricenormal - $disc_per_item<br>";
							// $html .= "$line_cost = $price_dics * $quantity<br>";
							$total = $total + $line_cost;   //add to the total cost
							
							
							$item++;
							
							if ($item >= 3 && $item <= 12) {
								for($i = 0; $i < $item; $i++) {
								$html .= '<tr><td colspan="6">&nbsp;aaa</td></tr>';
								}
							}
							$html .= '
							<tr>
								<td style="text-align:center">'.$item.'</td>
								<td>
									<p class="product-name">
									<strong>'.$product_code.'</strong><br/>
									<medium class="description-product"><i>'.htmlentities($productname, ENT_QUOTES).'</i></medium>
									</p>
								</td>
								<td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
								<td style="text-align:center">'.$discount.'</td>
								<!--<td style="text-align:right">'.number_format($price_dics, 0, ',', '.').'</td>-->
								<td style="text-align:center">'.$quantity.'</td>
								<td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
							</tr>
							';
						}
					}
				}
					
				$html .= '
					<tr>
						<td style="border-top: 1px solid #aaa;font-weight: bold;" colspan="'.$colSpan.'" align="right">'._TOTALLISTPRICE.'</td>
						<td align="right" style="border-top: 1px solid #aaa;">'.number_format($total, 0, ',', '.').'</td>
					</tr>';
				
				$discount = ($total*10)/100;
				$total_price = ($discount > 0) ? $total+round($discount) : $total;
				$total_price = round($total_price);
				
				$termofpayment = ($customTermOfPayment != '') ? $customTermOfPayment : '';
				$deliverytime = ($customDeliveryTime != '') ? $customDeliveryTime : '';
				$deliveryfranco = ($deliveryfranco != '') ? $deliveryfranco : '';
				$warranty = ($warranty != '') ? $warranty : '';
				$quotationvalidity = ($quotationvalidity != '') ? $quotationvalidity : '';
				// $quotationvalidity = ($customQuotationValidity != '') ? $customQuotationValidity : '';
				$termandcondition = ($customQuotationTC != '') ? $customQuotationTC : '';
				
				if ($order_discount != '') {
					// $voucher = number_format($nominal_voucher, 0, ',', '.');
					// $total_price = $total_price - $nominal_voucher;
					
					if (substr_count($order_discount, '%') == 1) {
						$voucher = $order_discount;
						$disc = $total*$voucher/100;
						$total = $total - $disc;
						$subtotal = $total;
						$discount_type = _DISCOUNTPERCENT;
					} else {
						$voucher = number_format($order_discount, 0, ',', '.');
						$total = $total - $order_discount;
						$subtotal = $total;
						$discount_type = _DISCOUNTPERCENT;
					}
					
					$discount = ($subtotal*10)/100;
					$total_price = ($discount > 0) ? $subtotal+round($discount) : $subtotal;
					$total_price = round($total_price);
					
					$html .= '
					<tr>
						<td colspan="'.$colSpan.'" align="right" style="font-weight: bold;">'.$discount_type.'</td>
						<td align="right">('.((substr_count($order_discount, '%') == 1) ? $order_discount : number_format($order_discount, 0, ',', '.')).')</td>
					</tr>
					';
					
					$html .= '
					<tr>
						<td colspan="'.$colSpan.'" align="right" style="font-weight: bold;">'._SUBTOTAL.'</td>
						<td align="right">'.number_format($subtotal, 0, ',', '.').'</td>
					</tr>
					';
				}
				$terbilang = convertNumber($total_price);	//bilangan($total_price);
				
				$html .= '
					<tr>
						<td colspan="'.$colSpan.'" align="right" style="font-weight: bold;">'._VATIDR.'</td>
						<td align="right">'.number_format($discount, 0, ',', '.').'</td>
					</tr>
					<tr>
						<td colspan="'.$colSpan.'" align="right" style="font-weight: bold;">'._TOTAIDR.'</td>
						<td align="right">'.number_format($total_price, 0, ',', '.').'</td>
					</tr>
					<tr style="background-color:#f5f5f5;">
						<td colspan="7" style="border-top:1px solid #aaa; font-weight:bold;solid #aaa">'._TERBILANG.': '.$terbilang.'</td>
					</tr>
				</tbody>
				</table>';
				
				
				$berca_estore = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? '<img src="images/berca-estore.png" height="23px" >' : '<img src="images/berca-estore-bnm.png" height="32px" >';
				
				// $html .= "Item: $item - {$pdf->getAliasNbPages()}" ;
				
				// if ($catatan != '') {
					// $html .= '
					// <p>
					// <u><b>'._NOTE.'</b></u><br/>
					// '.$catatan.'<br/>'.$additional_note.'</p>';
				// } else {
					// $html .= '<p>'.$additional_note.'<br/></p>';
				// }
				
				$html .= '<p><u><b>'._NOTE.'</b></u><br/>';
				if ($catatan != '') {
					
					$html .= $catatan.'<br/>'.$additional_note;
				} else {
					$html .= $additional_note.'<br/>';
				}
				$html .= '</p>';
				
				$html .= '
			</div>
			';
				
			$html .= ob_get_clean();
			// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			
			
			if(
				($jumlah_product>=5 && $jumlah_product<=8) or
				($jumlah_product>=16 && $jumlah_product<=27) or 
				($jumlah_product>=33 && $jumlah_product<=46) 
			){
				// $pdf->addPage();
				// $html = '<p style="line-height:3">&nbsp;</p>';
				$html .= '<br pagebreak="true"/>';
				// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			}

			if ($is_from_template == 1) {
				$html .= '
				<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
					<table>
						<tr>
							<td style="width: 150px;">Delivery Time</td>
							<td style="width: 550px;">: '.$customDeliveryTime.'</td>
						</tr>
						<tr>
							<td>Term of Payment</td>
							<td>: '.$customTermOfPayment.'</td>
						</tr>
						<tr>
							<td>Quotation Validity</td>
							<td>: '.$customQuotationValidity.'</td>
						</tr>
						<tr>
							<td>Delivery Franco</td>
							<td>: '.$tc_delivery_franco.'</td>
						</tr>
						<tr>
							<td>Warranty</td>
							<td>: '.$tc_warranty.'</td>
						</tr>
					</table>';
			} else {
				$html .= '
				<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
					'.$termandcondition;
			}
			$html .= '
				<!--<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
				'.$customQuotationTC.'-->
				<p style="font-size:0px;line-height:0px;">&nbsp;</p>
				<p>'._SINCERELYYOURS.',<br/><br/>'.$berca_estore.'</p>
				<div style="background-color:#eee;margin-bottom:30px;margin-top:30px;text-align:center;line-height:35px">'.$quotationvalidity.'</div>';
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			
			// $str_filename = str_replace('/','_',$txid);
			// $filename = "$str_filename.pdf";
			
			$str_filename = str_replace('/','_',$txid);
			$filename = "$str_filename.pdf";
			$file = "$cfg_pdf_path/$filename";
			
			$pdf->Output($file, 'F');
			if (count($temp_files) > 0) {
				$temp_files[] = "$file";
				
				/*
				* Start PDF Merge
				*/
				// include 'aplikasi/vendor/autoload.php';
				// $pdfMerge = new \Jurosh\PDFMerge\PDFMerger;
				
				require_once 'aplikasi/PDFMerger.php';
				$pdfMerge = new PDFMerger;
				
				// print_r($temp_files);
				if (count($temp_files) > 0) {
					foreach($temp_files as $file) {
						$pdfMerge->addPDF($file, "all", 'vertical');
					}
				}
				
				$pdfMerge->merge("file", "$cfg_pdf_path/$filename");
				$filename = "$cfg_pdf_path/$filename";
			} else {
				
				$pdf->Output($file, 'I');
			}
			
			/*
			* Start PDF Merge
			*/
			// include 'aplikasi/vendor/autoload.php';
			// $pdfMerge = new \Jurosh\PDFMerge\PDFMerger;
			
			// // print_r($temp_files);
			// if (count($temp_files) > 0) {
				// foreach($temp_files as $file) {
					// $pdfMerge->addPDF($file, "all", 'vertical');
				// }
			// }
			// $pdfMerge->merge("file", "$cfg_pdf_path/$filename");
			// $filename = "$cfg_pdf_path/$filename";
			
			// $pdfMerge->merge("browser", "$cfg_pdf_path/$filename");
			// $pdf->Output($filename, 'I');
			// $attachment = "$cfg_pdf_path/$filename";
		}
	}	
	
	#end region quotation pdf
}

if ($action == 'downloadpdfadmin') {
	#region quotation pdf
	ob_start();
	$txid = base64_decode($_GET['pid']);
	$type = fiestolaundry($_GET['type'], 3);
	
	$quo_id = str_replace('/', '_', $txid);
	
	
	
	if (file_exists("$cfg_pdf_path/$quo_id.pdf")) {
		
		$file = "$quo_id.pdf"; 
		$filename = "$cfg_pdf_path/$quo_id.pdf"; 
		  
		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename="' . $file . '"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($filename));
		header('Accept-Ranges: bytes');
		readfile($filename);
	} else {
	
		// Include the main TCPDF library (search for installation path).
		require_once('aplikasi/tcpdf/config/tcpdf_include.php');
		
		// print_r($_SESSION);die();
		// Extend the TCPDF class to create custom Header and Footer
		class MYPDF extends TCPDF {

			//Page header
			public function Header() {
				// Logo
				global $berca_id, $txid, $mysqli;
				
				// $berca_letterhead = ((isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) || $_SESSION['web_mode'] == $berca_id) ? 'header-kop.png' : 'header-kop-bnm.png';
				$berca_letterhead = (get_member_company($txid) == $berca_id) ? 'header-kop.png' : 'header-kop-bnm.png';
				$image_file = "images/$berca_letterhead"; // *** Very IMP: make sure this image is available on given path on your server
									
				$this->Image($image_file,16,8,178);
				// Set font
				$this->SetFont('dejavusans', '', 10);
				
				if ($this->page == 1) {
					// $berca_letterhead = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? 'header-kop.png' : 'header-kop-bnm.png';
					// $image_file = "images/$berca_letterhead"; // *** Very IMP: make sure this image is available on given path on your server
										
					// $this->Image($image_file,16,8,178);
					// // Set font
					// $this->SetFont('helvetica', 'C', 12);
					
				} else {
					
					$sql = "SELECT tanggal_buka_transaksi FROM sc_quotation WHERE quotation_id='$txid'";
					$result = $mysqli->query($sql);
					list($tanggal_buka_transaksi) = $result->fetch_row();
					
					// $txt = '
					

					
					
					// '._QUOTATIONDATE.': '.tglformat($tanggal_buka_transaksi).'
					// '._QUOTATIONNUMBER.': '.$txid;
					
					$txt = '
					<table>
						<tr>
						<td width="418px">&nbsp;</td>
						<td>
							<table cellpadding="5" border="1" style="text-align:left;width:290px;">
								<tr><td style="width:70px;">'._QUOTATIONDATE.'</td><td style="">: '.tglformat($tanggal_buka_transaksi).'</td></tr>
								<tr><td style="width:70px;">'._QUOTATIONNUMBER.'</td><td style="">: '.$txid.'</td></tr>
							</table>
						</td>
						</tr>
					</table>
					';

					// print a block of text using Write()
					// $this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
					// $this->writeHTMLCell(0, 0, '', '', $txt, 0, 0, false, "L", true);
					$this->SetTopMargin(55);
					$this->SetY(35);
					$this->writeHTML($txt, true, false, true, false, 'R');
				}
			}
			
			public function Footer() {
				global $txt_footer_pdf, $txt_footer_bnm_pdf, $berca_id, $type, $cfg_app_url;
				
				// Position at 15 mm from bottom
				$this->SetY(-15);
				// Set font
				$this->SetFont('dejavusans', 'I', 8);
				// Page number
				
				// $berca_footer_pdf = ($type == $berca_id) ? $txt_footer_pdf : $txt_footer_bnm_pdf;
				// $this->writeHTMLCell(0, 0, '', '', $berca_footer_pdf, 0, 0, false, "L", true);
				$txid = base64_decode($_GET['pid']);
				// if ((isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) || $_SESSION['web_mode'] == $berca_id) {
				if (get_member_company($txid) == $berca_id) {
					$this->writeHTMLCell(0, 0, '', '', $txt_footer_pdf, 0, 0, false, "L", true);
					$this->Cell(7, 17, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
				} else {
					// $this->writeHTMLCell(0, '', $this->SetX(5), $this->SetY(-28), $txt_footer_bnm_pdf, 0, 0, false, true, 'L', false);
					
					$this->SetY(-25);
					
					$logoX = 15; // 186mm. The logo will be displayed on the right side close to the border of the page
					$logoFileName = "./images/footerbnm.jpg";
					$logoWidth = 15; // 15mm

					$logo = $this->Image($logoFileName, $logoX, $this->GetY()+2, 178, 19, '','','',true,300,'C');
					$this->Cell(365, 38, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
					$this->SetX($this->w - $this->documentRightMargin - $logoWidth); // documentRightMargin = 18
					$this->writeHTMLCell(0, 0, '', '', $logo, 0, 0, false, "L", true);
				}
									
			}
		}

		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		// $pdf->SetCreator('Fiesto');
		// $pdf->SetAuthor('Fiesto');
		$pdf->SetTitle(_EQUOTATION . ' #' . $txid);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP +17, PDF_MARGIN_RIGHT);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('dejavusans', '', 10, '', true);

		// add a page
		$pdf->AddPage();

		$sql = "SELECT tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher,kurir, discount, termofpayment, deliverytime, quotationvalidity, termandcondition, is_from_template, tc_warranty, tc_delivery_franco FROM sc_quotation WHERE quotation_id='$txid'";
		$result = $mysqli->query($sql);
		if ($result->num_rows == 0) {
			$url = $urlfunc->makePretty("?p=order&action=history_quotation");
			header("Location: $url");
			exit();
		} else {
			list($tanggal_buka_transaksi, $tanggal_tutup_transaksi, $customer_id, $receiver_id, $metode_pembayaran, $shipping, $status, $catatan, $no_pengiriman, $latestupdate, $kodevoucher, $tipe_voucher, $nominal_voucher,$kurir, $order_discount, $customTermOfPayment, $customDeliveryTime, $customQuotationValidity, $customQuotationTC, $is_from_template, $tc_warranty, $tc_delivery_franco) = $result->fetch_row();
			
			$couriers 		= json_decode($kurir, true);
			$kecamatan 		= $couriers['kota'];
			$kurir 			= $couriers['tipe'] . ' ' . $couriers['kota'];
			$kurir_tipe		= $couriers['tipe'];
			$ongkos_kirim 	= number_format($shipping, 0, ',', '.');
			$estimasi 		= $couriers['estimasi'];
			
			$content .= "<h2>"._EQUOTATION."</h2>\r\n";
			
			$sql = "SELECT nama_lengkap, email, company, alamat, alamat2, kota, provinsi, zip, telepon, cellphone1, cellphone2, nama_pengirim, telp_pengirim, email_pengirim, pic FROM sc_receiver WHERE receiver_id='$receiver_id'";
			$result = $mysqli->query($sql);
			list($nama_lengkap, $email, $company, $alamat, $alamat2, $kota, $provinsi, $zip, $telepon, $cellphone1, $cellphone2, $sendername, $senderphone, $senderemail, $pic) = $result->fetch_row();
			$temp_address = ($alamat2 != '') ? "$alamat<br>$alamat2" : $alamat;
			
			if ($pic != '') {
				$temp_pic = '
				<tr>
					<td valign="top">'._QUOTATIONPREPAREBY.'</td>
					<td valign="top">:</td>
					<td>'.$pic.'</td>
				</tr>';
			} else {
				$temp_pic = '';
			}
			$html = '
			<h1>'._EQUOTATION.'</h1>
			<div class="table_block table-responsive table-small-gap">
				<table>
					<tr>
						<td width="48%">
							<table>
								<tr valign="top">
									<td width="10%">'._QUOTATIONTO.'</td>
									<td width="8%" align="left">:</td>
									<td width="82%">
										'.$company.'<br>
										'.$temp_address.'<br>
										'.$kota.'<br>
										Phone: '.$cellphone1.'
										<p style="line-height:0.8;">&nbsp;</p>
									</td>
								</tr>
								<tr valign="top">
									<td>'._QUOTATIONATTN.'</td>
									<td>:</td>
									<td>
										'.$nama_lengkap.'<br>
										'.$email.'
									</td>
								</tr>
							</table>
						</td>
						<td width="4%">&nbsp;</td>
						<td width="48%">
							<table>
								<tr>
									<td width="27%">'._QUOTATIONDATE.'</td>
									<td width="3%">:</td>
									<td width="70%">'.tglformat($tanggal_buka_transaksi).'</td>
								</tr>
								<tr>
									<td>'._QUOTATIONNUMBER.'</td>
									<td>:</td>
									<td>'.$txid.'</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								'.$temp_pic.'
							</table>
						</td>
					</tr>
				</table>
				<p style="line-height:0.6;">&nbsp;</p>';
				
				if ($is_from_template == 1) {
					$sql = "SELECT jumlah,td.harga,product_name,td.filename, '' as merek, td.diskon,  '' as keterangan, '' as sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, parent FROM sc_quotation_detail td WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
				} else {
					$sql = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, parent FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
				}
				
				$result = $mysqli->query($sql);
				$jumlah_product = mysqli_num_rows($result);
				$html .= '
				<p style="line-height:0.6;">&nbsp;</p>
				<table border="0" class="table table-bordered" width="100%" cellpadding="7">
					<tr style="background-color:#f5f5f5;border-bottom: 1px solid #aaa;font-weight:bold">
						<th style="text-align:center" width="8%" style="border-bottom: 1px solid #aaa;">'._ITEM.'</th>
						<th style="text-align:center" width="35%" style="border-bottom: 1px solid #aaa;">'._PARTNUMBER.'</th>
						<th style="text-align:center" width="20%" style="border-bottom: 1px solid #aaa;">'._UNITPRICE.'</th>
						<th style="text-align:center" align="center" width="10%" style="text-align:center;border-bottom: 1px solid #aaa;">'._DISCOUNT.'</th>
						<!--<th style="text-align:center" width="15%" align="center" style="border-bottom: 1px solid #aaa;">'._SUBTOTAL.'</th>-->
						<!--<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._QTY.'</th>-->
						<th style="text-align:center" width="7%" align="center" style="border-bottom: 1px solid #aaa;">Qty</th>
						<th style="text-align:right" width="20%" style="border-bottom: 1px solid #aaa;">'._TOTAIDR.'</th>
					</tr>
				';
				
				$colSpan = 5;
				$item = 0;
				$total = 0;
				$no_parent = 1;
				while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $parent) = $result->fetch_row()) {
					$brand_name = get_brand_name($idmerek);
					$parent = $parent == 0 ? $no_parent++ : "";
					// $line_cost = $quantity * $price_dics;  //work out the line cost
					
					// 05/03/2020 14:57
					if ($is_from_template == 1) {
						if ($discount != '') {
							// $discount = ($disc_per_item != '') ? $disc_per_item : $discount;
							// $line_cost = $quantity * $price;  //work out the line cost
							// $disc = $price * (int)$discount / 100;
							// $harga_setelah_disc = $disc * $quantity;
							
							$line_cost = ($quantity * $price) - (($quantity * $price) * (int)$discount / 100);
							$total += $line_cost;
						} else {
							$line_cost = $quantity * $price;  //work out the line cost
							$total += $line_cost;
						}
					} else {
						$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
						$line_cost = $quantity * $price;  //work out the line cost
						$disc = $price * (int)$discount / 100;
						$harga_setelah_disc = $disc * $quantity;
						
						// $price_dics = $line_cost - $harga_setelah_disc;
						if ($disc_per_item != '') {
							$price_dics = $line_cost - $harga_setelah_disc;
							$line_cost = $price_dics;
						} /* else {
							$price_dics = 0;
						} */
						$total += $price_dics;
					}
					
					// if ($discount != '' && $disc_per_item == '') {
						// $line_cost = $pricenormal * $quantity;
						// $discount = $disc_per_item;
						
						// $nominal = str_replace('%', '', $disc_per_item);
						// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
						
						// $disc = 100 - $nominal;
						// $temp = (($disc/100) * $line_cost);
						// $line_cost = $temp;
						
						// /* if (substr_count($disc_per_item, '%') == 1) {
							// $discount = $disc_per_item;
							// $nominal = str_replace('%', '', $disc_per_item);
							// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							// $disc = 100 - $nominal;
							// $temp = (($disc/100) * $line_cost);
							// $line_cost = $temp;
						// } else {
							// $discount = $disc_per_item;
							// $discount = number_format($discount, 0, ',', '.');
							// $price_dics = $pricenormal - $disc_per_item;
							
							// $line_cost = $price_dics * $quantity;
						// } */
					// } else if ($discount != '' && $disc_per_item != '') {
						// $line_cost = $pricenormal * $quantity;
						// $discount = $disc_per_item;
						
						// $nominal = str_replace('%', '', $disc_per_item);
						// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
						
						// $disc = 100 - $nominal;
						// $temp = (($disc/100) * $line_cost);
						// $line_cost = $temp;
						
						// /* if (substr_count($disc_per_item, '%') == 1) {
							// $discount = $disc_per_item;
							// $nominal = str_replace('%', '', $disc_per_item);
							// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							// $disc = 100 - $nominal;
							// $temp = (($disc/100) * $line_cost);
							// $line_cost = $temp;
						// } else {
							// $discount = $disc_per_item;
							// $discount = number_format($discount, 0, ',', '.');
							// $price_dics = $pricenormal - $disc_per_item;
							
							// $line_cost = $price_dics * $quantity;
						// } */
					// } else if ($discount == '' && $disc_per_item != '') {
						// $line_cost = $pricenormal * $quantity;
						// $discount = $disc_per_item;
						
						// $nominal = str_replace('%', '', $disc_per_item);
						// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
						
						// $disc = 100 - $nominal;
						// $temp = (($disc/100) * $line_cost);
						// $line_cost = $temp;
						
						// /* if (substr_count($disc_per_item, '%') == 1) {
							// $discount = $disc_per_item;
							// $nominal = str_replace('%', '', $disc_per_item);
							// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
							
							// $disc = 100 - $nominal;
							// $temp = (($disc/100) * $line_cost);
							// $line_cost = $temp;
						// } else {
							// $discount = number_format($discount, 0, ',', '.');
							// $price_dics = $pricenormal - $disc_per_item;
							
							// $line_cost = $price_dics * $quantity;
						// } */
					// } else if ($discount == '') {
						// $line_cost = $pricenormal * $quantity;
						// $discount = '';
					// } else {
						// $line_cost = $price_dics * $quantity;  //work out the line cost
					// }
					
					// $html .= "$price_dics = $pricenormal - $disc_per_item<br>";
					// $html .= "$line_cost = $price_dics * $quantity<br>";
					
					if ($discount != '') $discount = round($discount)."%";
					// $total = $total + $line_cost;   //add to the total cost
					
					// if ($disc_per_item != '') {
						// // jika ada disc, calculate total - disc per item
						// if (substr_count($disc_per_item, '%') == 1) {
							// $nominal = str_replace('%', '', $disc_per_item);
							// $disc = 100 - $nominal;
							// $temp = (($disc/100) * $line_cost);
							// $line_cost = $temp;
						// } else {
							// $line_cost = $line_cost - $disc_per_item;
						// }
					// }
					
					$item++;
					
					if ($is_from_template == 1) {
						$no = $parent;
					} else {
						$no = $item;
					}
					$price = ($price > 0) ? number_format($price, 0, ',', '.') : '';
					$line_cost = ($line_cost > 0) ? number_format($line_cost, 0, ',', '.') : '';
					$html .= '
					<tr>
						<td style="text-align:center">'.$no.'</td>
						<td>
							<p class="product-name">
							<strong>'.$product_code.'</strong><br/>
							<medium class="description-product"><i>'.htmlentities($productname, ENT_QUOTES).'</i></medium>
							</p>
						</td>
						<td style="text-align:right">'.$price.'</td>
						<td style="text-align:center">'.$discount.'</td>
						<!--<td style="text-align:right">'.number_format($price_dics, 0, ',', '.').'</td>-->
						<td style="text-align:center">'.$quantity.'</td>
						<td style="text-align:right">'.$line_cost.'</td>
					</tr>
					';
				}
				// die();
				$html .= '
					<tr>
						<td style="border-top: 1px solid #aaa;font-weight: bold;" colspan="'.$colSpan.'" align="right">'._TOTALLISTPRICE.'</td>
						<td align="right" style="border-top: 1px solid #aaa;">'.number_format($total, 0, ',', '.').'</td>
					</tr>';
				
				$discount = ($total*10)/100;
				$total_price = ($discount > 0) ? $total+round($discount) : $total;
				$total_price = round($total_price);
				
				$termofpayment = ($customTermOfPayment != '') ? $customTermOfPayment : '';
				$deliverytime = ($customDeliveryTime != '') ? $customDeliveryTime : '';
				$deliveryfranco = ($deliveryfranco != '') ? $deliveryfranco : '';
				$warranty = ($warranty != '') ? $warranty : '';
				$quotationvalidity = ($quotationvalidity != '') ? $quotationvalidity : '';
				// $quotationvalidity = ($customQuotationValidity != '') ? $customQuotationValidity : '';
				$termandcondition = ($customQuotationTC != '') ? $customQuotationTC : '';
				
				if ($order_discount != '') {
					// $voucher = number_format($nominal_voucher, 0, ',', '.');
					// $total_price = $total_price - $nominal_voucher;
					
					if (substr_count($order_discount, '%') == 1) {
						$voucher = $order_discount;
						$disc = $total*$voucher/100;
						$total = $total - $disc;
						$subtotal = $total;
						$discount_type = _DISCOUNTPERCENT;
					} else {
						$voucher = number_format($order_discount, 0, ',', '.');
						$total = $total - $order_discount;
						$subtotal = $total;
						$discount_type = _DISCOUNTPERCENT;
					}
					
					$discount = ($subtotal*10)/100;
					$total_price = ($discount > 0) ? $subtotal+round($discount) : $subtotal;
					$total_price = round($total_price);
					
					$html .= '
					<tr>
						<td colspan="'.$colSpan.'" align="right" style="font-weight: bold;">'.$discount_type.'</td>
						<td align="right">('.((substr_count($order_discount, '%') == 1) ? $order_discount : number_format($order_discount, 0, ',', '.')).')</td>
					</tr>
					';
					
					$html .= '
					<tr>
						<td colspan="'.$colSpan.'" align="right" style="font-weight: bold;">'._SUBTOTAL.'</td>
						<td align="right">'.number_format($subtotal, 0, ',', '.').'</td>
					</tr>
					';
				}
				$terbilang = convertNumber($total_price);	//bilangan($total_price);
				
				$html .= '
					<tr>
						<td colspan="'.$colSpan.'" align="right" style="font-weight: bold;">'._VATIDR.'</td>
						<td align="right">'.number_format($discount, 0, ',', '.').'</td>
					</tr>
					<tr>
						<td colspan="'.$colSpan.'" align="right" style="font-weight: bold;">'._TOTAIDR.'</td>
						<td align="right">'.number_format($total_price, 0, ',', '.').'</td>
					</tr>
					<tr style="background-color:#f5f5f5;">
						<td colspan="7" style="border-top:1px solid #aaa; font-weight:bold;solid #aaa">'._TERBILANG.': '.$terbilang.'</td>
					</tr>
				</tbody>
				</table>';
				// $berca_estore = (/* $type == $berca_id ||  */$_SESSION['web_mode'] == $berca_id) ? '<img src="images/berca-estore.jpeg" height="23px" >' : '<img src="images/berca-estore-bnm.png" height="32px" >';
				$berca_estore = (get_member_company($txid) == $berca_id) ? '<img src="images/berca-estore.jpeg" height="23px" >' : '<img src="images/berca-estore-bnm.png" height="32px" >';
				
				// $html .= "Item: $item - {$pdf->getAliasNbPages()}" ;
				
				// if ($catatan != '') {
					// $html .= '
					// <p>
					// <u><b>'._NOTE.'</b></u><br/>
					// '.$catatan.'<br/>'.$additional_note.'</p>';
				// } else {
					// $html .= '<p>'.$additional_note.'<br/></p>';
				// }
				
				$html .= '<p><u><b>'._NOTE.'</b></u><br/>';
				if ($catatan != '') {
					
					$html .= $catatan.'<br/>'.$additional_note;
				} else {
					$html .= $additional_note.'<br/>';
				}
				$html .= '</p>';
				
				$html .= '
			</div>';
			$html .= ob_get_clean();
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			if(
				($jumlah_product>=5 && $jumlah_product<=8) or
				($jumlah_product>=16 && $jumlah_product<=27) or 
				($jumlah_product>=33 && $jumlah_product<=46) 
			){
				$html .= '<br pagebreak="true"/>';
			}
			if ($is_from_template == 1) {
				$content .= '
				<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
					<table>
						<tr>
							<td style="width: 150px;">Delivery Time</td>
							<td style="width: 550px;">: '.$customDeliveryTime.'</td>
						</tr>
						<tr>
							<td>Term of Payment</td>
							<td>: '.$customTermOfPayment.'</td>
						</tr>
						<tr>
							<td>Quotation Validity</td>
							<td>: '.$customQuotationValidity.'</td>
						</tr>
						<tr>
							<td>Warranty</td>
							<td>: '.$tc_warranty.'</td>
						</tr>
						<tr>
							<td>Delivery Franco</td>
							<td>: '.$tc_delivery_franco.'</td>
						</tr>
					</table>';
			} else {
				$html .= '<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
				'.$termandcondition.'<p style="font-size:0px;line-height:0px;">&nbsp;</p>';
			}
			$html = '
				
				<p>'._SINCERELYYOURS.',<br/><br/>'.$berca_estore.'</p>
				<div style="background-color:#eee;margin-bottom:30px;margin-top:30px;text-align:center;line-height:35px">'.$quotationvalidity.'</div>
				';
			
			$html .= ob_get_clean();
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			
			$str_filename = str_replace('/','_',$txid);
			$filename = "$str_filename.pdf";

			$pdf->Output($filename, 'I');
			// $attachment = "$cfg_pdf_path/$filename";
		}
		#end region quotation pdf
	}
}

if ($action == 'trackorder'){
	
		$orderid = fiestolaundry($_REQUEST['order_orderid'],20);
		$email = fiestolaundry($_REQUEST['order_email'],50);
		if(strlen($orderid)>0)
		{
		$sql = "
		SELECT tx.transaction_id, tx.tanggal_buka_transaksi, tx.status, tx.ongkir,tx.no_pengiriman 
		FROM sc_transaction tx, sc_customer c,sc_receiver rc 
		WHERE 
		tx.transaction_id='".$orderid."' AND 
		c.customer_id=tx.customer_id
		AND rc.receiver_id=tx.receiver_id
		";
		$result = $mysqli->query($sql);
		if($result and mysql_num_rows($result)>0)
		{
		$content .= "<table class=\"systemtable\" border=\"1\">\r\n";
		$content .= "<tr><th>"._CURRENTORDERTITLE."</th><th>"._ORDERDATE."</th><th>"._TOTAL."</th><th>"._POSTAGE."</th><th>"._ORDERSTATUS."</th></tr>\r\n";
		while (list($txid, $txdate, $status, $ongkir,$no_pengiriman) = $result->fetch_row()) {
			$sql1 = "SELECT jumlah, harga FROM sc_transaction_detail WHERE transaction_id='$txid'";
			$result1 = $mysqli->query($sql1);
			$total = 0;
			while (list($qty, $harga) = $result1->fetch_row()) {
				$total = $total + $qty * $harga;
			}
			switch ($status) {
				case 0:
					$temp = _STATUS0;
					break;
				case 1:
					$temp = _STATUS1;
					break;
				case 2:
					$temp = _STATUS2;
					break;
				case 3:
					$temp = _STATUS3;
					break;
				case 4:
					$temp = _STATUS4." $no_pengiriman";
					break;
					
			}
			$content .= "<tr valign=\"top\"><td><a href=\"".$urlfunc->makePretty("?p=order&action=view&id=$txid")."\">$txid</td><td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total,0,',','.')."</td><td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td></tr>\r\n";
		}
		$content .= "</table>\r\n";
		}
	 }
	 else
	 {
		 $content=_ORDERNOTFOUND;
	 }
}

if ($action == 'confirm_payment') {
	$title = _CONFIRMPAYMENT;
	// $sql = "SELECT tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher, packingvalue, insurancevalue, kurir, tanggal_konfirmasi, nominal FROM sc_transaction WHERE transaction_id='$txid' AND (status<>0 OR is_del<>1)";
	$sql = "SELECT tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher, kurir, tanggal_konfirmasi, nominal FROM sc_transaction WHERE transaction_id='$txid' AND (status<>0)";
	
	$result = $mysqli->query($sql);
	$num_rows = $result->num_rows;
	if ($num_rows > 0) {
		list($tanggal_buka_transaksi, $tanggal_tutup_transaksi, $customer_id, $receiver_id, $metode_pembayaran, $shipping, $status, $catatan, $no_pengiriman, $latestupdate, $kodevoucher, $tipe_voucher, $nominal_voucher, $kurir, $tanggal_konfirmasi, $nominal) = $result->fetch_row();
		
		$couriers 		= json_decode($kurir, true);
		$kecamatan 		= $couriers['kota'];
		$kurir 			= $couriers['tipe'] . ' ' . $couriers['kota'];
		$kurir_tipe		= $couriers['tipe'];
		$ongkos_kirim 	= number_format($shipping, 0, ',', '.');
		$estimasi 		= $couriers['estimasi'];
		
		$content .= "<div id=\"checkout-form\">";
		$content .= "<h2>"._ORDERSTATUS."</h2>\r\n";
		$content .= "<p>";
		switch ($status) {
			case 1:
				$content .= _STATUS1;
				break;
			case 2:
				$content .= _STATUS2;
				break;
			case 3:
				$content .= _STATUS3." $no_pengiriman";
				break;
		}
		// $content .= " ("._LATESTUPDATE.": ".tglformat($latestupdate,true).")</p>\r\n";
		
		$content .= "<h2>"._ORDERDETAILS."</h2>\r\n";
		$content .= "<table border=\"0\">\r\n";
		$content .= "<tr><td>"._CURRENTORDERTITLE."</td><td>:</td><td>$txid</td></tr>\r\n";
		$content .= "<tr><td>"._ORDERDATE."</td><td>:</td><td>".tglformat($tanggal_buka_transaksi,true)."</td></tr>\r\n";
		$content .= "</table><br />\r\n";				
		$content .= "<div class=\"table_block table-responsive table-small-gap\">\r\n";
		$content .= "<table border=\"0\" class=\"table table-bordered\" cellpadding=\"3\">"; //format the cart using a HTML table
		//iterate through the cart, the $product_id is the key and $quantity is the value
		$content .= "<thead><tr style=\"background: #eee;font-weight: bold;\">";
		$content .= "<th>"._THUMBNAIL."</th>";
		$content .= "<th>"._PRODUCTNAME."</th>";
		$content .= "<th>"._PRICE."</th>";
		$content .= "<th>"._DISCOUNT."</th>";
		$content .= "<th>"._QUANTITY."</th>";
		$content .= "<th>"._LINECOST."</th>";
		$content .= "</tr></thead>";
		$totalweight = 0;
		
		
		// $sql = "SELECT jumlah, harga, product_name,filename FROM sc_transaction_detail WHERE transaction_id='$txid'";
		$sql = "SELECT jumlah,td.harga,product_name,td.filename, c.idmerek, td.harganormal, td.diskon, c.sku FROM sc_transaction_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE transaction_id='$txid' ";
		
		$result = $mysqli->query($sql);
		while (list($quantity, $price, $productname,$filename,$idmerek,$harganormal,$diskon, $product_code) = $result->fetch_row()) {
			$brand_name = get_brand_name($idmerek);

			$line_cost = $quantity * $price;  //work out the line cost
			$total = $total + $line_cost;   //add to the total cost
			$content .= "<tr>";
			$content .= "<td>";
			if(file_exists("$cfg_thumb_path/$filename")) {
				$content.="<img src=\"$cfg_thumb_url/$filename\" width=\"50px\" class=\"thumb_small\" >";
			} else {
				$content.="<img src=\"$cfg_app_url/images/none.gif\" width=\"50px\" class=\"thumb_small\" >";
			}
			$content .="</td>";
			// $content .= "<td>$productname</td>";
			$content .= "<td><p class=\"product-name\">$brand_name $product_code</p><small class=\"description-product\">$productname</small></td>";
			$content .= "<td align=\"right\">".number_format($harganormal, 0, ',', '.')."</td>";
			$content .= '<td class="cart_unit" align="right">';
			if ($diskon != '') {
				if (substr_count($diskon, "%") == 1) {
					$label_disc = $diskon;
				} else {
					$label_disc = number_format($diskon, 0, ',', '.');
				}
				$content .= '<span class="price-nowx">'. $label_disc .'</span>';
			}
			// $content .= '</span>';
			$content .= '</td>';
			/* $content .= "<td align=\"right\">";
			if ($diskon != '') {
				$content .= '<div class="price-old" id="tHarga">'.$matauang.' '.number_format($harganormal, 0, ',', '.').'</div>';
				$content .= '<div class="price-now" id="tHargaDiskon">'.$matauang.' '.number_format($price, 0, ',', '.').'</div>';

				if (substr_count($diskon, "%") == 1) {
					$label_disc = $diskon;
				} else {
					$label_disc = $matauang.' '.number_format($diskon, 0, ',', '.');
				}
				$content .= '<span class="reduction"><span>-'.$label_disc.'</span></span>';
			} else {
				$content .= '<span class="price-now" id="tHarga">'.$matauang.' '.number_format($harganormal, 0, ',', '.').'</span>';
			}
			$content .= "</td>"; */
			$content .= "<td align=\"center\">".number_format($quantity, 0, ',', '.')."</td>";
			$content .= "<td align=\"right\">".number_format($line_cost, 0, ',', '.')."</td>";
			$content .= "</tr>";
		}
		
		if ($pakaiadditional || $pakaiongkir || $pakaivoucher) {
			$content .= "<tr>";
			$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;border-top:1px solid #ddd\">"._SUBTOTAL."</td>";
			$content .= "<td align=\"right\" style=\"border-top:1px solid #ddd\">".number_format($total, 0, ',', '.')."</td>";
			$content .= "</tr>";
		}

		if ($pakaivoucher) {
			if ($nominal_voucher > 0) {
				
				if ($tipe_voucher == 1) {
					$voucher = number_format($nominal_voucher, 0, ',', '.');
					$disc = $total - $nominal_voucher;
					$total = $disc;
				} else if ($tipe_voucher == 2) {
					$disc = 100 - $nominal_voucher;
					$temp = (($disc/100) * $total);
					$voucher = (int)$nominal_voucher . '%';
					$total = $temp;
				}
			} else {
				$voucher = 0;
			}
			$content .= "<tr>";
			$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._DISCOUNT."</td>";
			$content .= "<td align=\"right\">".$voucher."</td>";
			$content .= "</tr>";
		}
		
		if ($pakaiadditional) {
			if ($insurancevalue > 0) $total += $insurancevalue;
			$content .= "<tr>";
			$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._USEINSURANCE."</td>";
			$content .= "<td align=\"right\">".number_format($insurancevalue, 0, ',', '.')."</td>";
			$content .= "</tr>";
			
			if ($packingvalue > 0) $total += $packingvalue;
			$content .= "<tr>";
			$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._USEPACKING."</td>";
			$content .= "<td align=\"right\">".number_format($packingvalue, 0, ',', '.')."</td>";
			$content .= "</tr>";
		}
		//START MENENTUKAN PENGGUNAAN ONGKOS KIRIM
		if ($pakaiongkir) {
			$content .= "<tr>";
			$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">$kurir_tipe</td>";
			$content .= "<td align=\"right\">".number_format($shipping, 0, ',', '.')."</td>";
			$content .= "</tr>";
			$total = $total + $shipping;
		}
		//END MENENTUKAN PENGGUNAAN ONGKOS KIRIM
		$content .= "<tr>";
		$content .= "<td colspan=\"5\" align=\"right\" style=\"font-weight: bold;\">"._TOTAL."</td>";
		$content .= "<td align=\"right\">".number_format($total, 0, ',', '.')."</td>";
		$content .= "</tr>";
		$content .= "</table>";
		$content .= "</div>";
		
		if ($tanggal_konfirmasi == '0000-00-00' && $nominal == 0 && $status <= 2) {
			$content .= "<div id=\"confirmPayment\" class=\"row\"><div class=\"col-sm-12\">".payment_confirmation()."</div></div>";
		}
		$content .= "</div>";
	} else {
		$url = $urlfunc->makePretty("?p=order&action=view&pid=$txid");
		$data = _NOINVOICE . '<a href="'.$url.'">'.$txid.'</a> '. _NOTVALID;
		$content .= $data;
	}
}

if ($action == 'do_confirm') {
	$action = fiestolaundry($_REQUEST['action'],20);
	$tanggal_konfirmasi = fiestolaundry($_POST['tanggal_konfirmasi']);
	$bank_tujuan = fiestolaundry($_POST['bank_tujuan'], 50);
	$bank_asal = fiestolaundry($_POST['bank_asal'], 50);
	$atas_nama = fiestolaundry($_POST['atas_nama'], 100);
	$no_rekening = fiestolaundry($_POST['no_rekening'], 30);
	$nominal = fiestolaundry($_POST['nominal'], 13);
	$trans_id = $_POST['trans_id'];
	$is_valid = true;
	$errors = array();
	
	if ($tanggal_konfirmasi == '') {
		$is_valid = false;
		$errors[] = 'Tanggal konfirmasi harus diisi';
	}
	if ($bank_tujuan == '') {
		$is_valid = false;
		$errors[] = 'Bank tujuan harus diisi';
	}
	if ($bank_asal == '') {
		$is_valid = false;
		$errors[] = 'Bank asal harus diisi';
	}
	if ($nominal == '') {
		$is_valid = false;
		$errors[] = 'Nominal harus diisi';
	}
	if (!is_numeric($nominal)) {
		$is_valid = false;
		$errors[] = 'Nominal harus berupa angka';		
	}
	
	if ($_FILES['filename']['name'] != '') {
		
		$attachment = array();
		if ($_FILES['filename']['name'] == UPLOAD_ERR_OK) {
					
			$pathinfo = pathinfo($_FILES['filename']['name']);
			if ($_FILES['filename']['size'] > $maxupload) {
				$is_valid = false;
				$errors[] = "Oops! Your files size is to large";
			}
			
			if (!in_array($pathinfo['extension'], $allowedtypes)) {
				$errors[] = "The uploaded file is not supported file type \r\n Only the following file types are supported: ".implode(', ',$allowedtypes);
				$is_valid = false;
			}
			
			$tmpFilePath = $_FILES['filename']['tmp_name'];
				
			if ($tmpFilePath != ""){
				$newFilePath = $cfg_thumb_path .'/'. $_FILES['filename']['name'];

				if(move_uploaded_file($tmpFilePath, $newFilePath)) {
					$attachment[] = "$newFilePath";
				}
			}
		}
	}
	
	if (sizeof($errors) > 0) {
		$content .= '<ul>';
		foreach($errors as $error) {
			$content .= '<li>'.$error.'</li>';
		}
		$content .= '</ul>';
		$content .= '<a class="back" href="javascript:history.back(-1)">'._BACK.'</a>';
	}
	
	if ($is_valid) {
		// $temp = explode('-',$tanggal_konfirmasi);
		// $tanggal_konfirmasi = "$temp[2]-$temp[1]-$temp[0]";
		$sql = "UPDATE sc_transaction SET status=3, tanggal_konfirmasi='$tanggal_konfirmasi', bank_tujuan='$bank_tujuan', bank_asal='$bank_asal', atas_nama='$atas_nama', no_rekening='$no_rekening', nominal='$nominal' WHERE transaction_id='$trans_id' AND (status<>0)";
		// echo $sql;
		// die();
		$url = $urlfunc->makePretty("?p=order&action=history");
		$result = $mysqli->query($sql);
		$num_rows = $result->affected_rows;
		if ($num_rows == 1) {
			$content .= send_email_to_admin($trans_id);
			if (!isset($_SESSION['member_uid'])) {
				header("Location: $cfg_app_url");
				exit();
			} else {
				header("Location: $url");
				exit();
			}
		} else {
			$url = $urlfunc->makePretty("?p=order&action=view&pid=$trans_id");
			$data = _NOINVOICE . '<a href="'.$url.'">'.$trans_id.'</a> '. _NOTVALID;
			$content .= $data;
		}
	}
}

if ($action == 'quotation_will_expired') {
	// $sql_config = "SELECT name, value FROM config WHERE name='range_expired'";
	// $result_config = $mysqli->query($sql_config);
	// list($name, $value) = $result_config->fetch_row();
	// $$name = $value;
	
	$title = 'Quotation will expired';
	
	$now = date('Y-m-d');
	// $sql = "SELECT *, DATEDIFF(tgl_expired, '$now') AS selisih FROM sc_quotation WHERE DATEDIFF(tgl_expired, '$now') <=$range_expired  AND status<=1";
	$user_id = $_SESSION['member_uid'];
	$sql = "SELECT q.*, DATEDIFF(tgl_expired, '$now') AS selisih  FROM sc_quotation q 
			INNER JOIN sc_customer c ON c.customer_id=q.customer_id
			INNER JOIN webmember m ON m.user_id = c.web_user_id
			WHERE DATEDIFF(tgl_expired, '$now') <=$range_expired AND status_quotation=0 AND m.user_id='$user_id'";
	if ($result = $mysqli->query($sql)) {
		if ($result->num_rows > 0) {
			$content .= "
			<div class=\"row-fluid\">
				<div class=\"span12\">
					<div class=\"content-widgets\">
						<div>
							<div class=\"widget-header-block\">
								<h2 class=\"widget-header\"><i class=\"icon-shopping-cart \"></i> ". _SALESQUOTATIONEXPIRED . "</h2>
							</div>
							<div class=\"content-box\">
			";
			$content .= "<table class=\"table systemtable table-striped\" border=\"0\" width=\"100%\">\r\n";
			$content .= "<tr><th>"._CURRENTORDERTITLE."</th><th>"._EXPIREDDESC."</th><th>"._EXPIREDDATE."</th><th></th></tr>\r\n";
			while($row = $result->fetch_assoc()) {
				$quotation_id = base64_encode($row['quotation_id']);
				$sales = get_sales_quotation($row['customer_id']);
				// $content .= '<li class="list=group-item"><a href="?p=order&action=view_quotation&id='.$quotation_id.'">'.$row['quotation_id']. ' - ' . $sales . '</a></li>';
				$url = $urlfunc->makePretty("?p=order&action=view_quotation&pid=$quotation_id");
				$content .= '<tr>';
				$content .= '<td><a href="'.$url.'">'.$row['quotation_id'].'</a></td>';
				$content .= '<td>'.$row['selisih'].' '._EXPIREDNDESC.'</td>';
				$content .= '<td>'.tglformat($row['tgl_expired']).'</td>';
				$content .= '<td></td>';
				$content .= '</tr>';
			}
			$content .= '</table>';
			$content .= "
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			";
		}
	}
}

// if ($_SESSION['member_level'] == 1 || $_SESSION['member_level'] == 2) {
	
if ($action == 'quotation_need_approval') {
	
	if (validAccess(2)) {
		$title = _NEEDAPPROVALALERT;
		$parent = $_SESSION['member_uid'];
		if (isset($parent)) {
			$sql = "SELECT q.quotation_id, m.fullname, q.tanggal_buka_transaksi, is_from_template FROM sc_quotation q
					INNER JOIN sc_customer c ON c.customer_id=q.customer_id
					INNER JOIN webmember m ON m.user_id=c.web_user_id
					WHERE q.level2='1' AND m.atasan_langsung='$parent' ORDER BY autoid DESC";
			if ($result = $mysqli->query($sql)) {
				if ($result->num_rows > 0) {
					
					$content .= '<h2>Quotation minta persetujuan approval</h2>';
					$content .= '<table class="table table-striped">';
					$content .= '<tr><th>'._CURRENTORDERTITLE.'</th><th>'._ORDERDATE.'</th><th class="text-right">'._TOTAL.'</th></tr>';
						
					while($row = $result->fetch_assoc()) {
						$txid = $row['quotation_id'];
						$txdate = $row['tanggal_buka_transaksi'];
						
						// $sql1 = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
						
						if ($row['is_from_template'] == 1) {
							$sql1 = "SELECT jumlah,td.harga,product_name,td.filename, '' as merek, td.diskon,  '' as keterangan, '' as sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, parent FROM sc_quotation_detail td WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
						} else {
							$sql1 = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
						}
						$result1 = $mysqli->query($sql1);
						$total = 0;
						while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item) = $result1->fetch_row()) {
							
							// 05/03/2020 14:57
							if ($row['is_from_template'] == 1) {
								if ($discount != '') {
									// $discount = ($disc_per_item != '') ? $disc_per_item : $discount;
									// $line_cost = $quantity * $price;  //work out the line cost
									// $disc = $price * (int)$discount / 100;
									// $harga_setelah_disc = $disc * $quantity;
									
									$line_cost = ($quantity * $price) - (($quantity * $price) * (int)$discount / 100);
									$total += $line_cost;
								} else {
									$line_cost = $quantity * $price;  //work out the line cost
									$total += $line_cost;
								}
							} else {
								$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
								$line_cost = $quantity * $price;  //work out the line cost
								$disc = $price * (int)$discount / 100;
								$harga_setelah_disc = $disc * $quantity;
								
								// $price_dics = $line_cost - $harga_setelah_disc;
								if ($disc_per_item != '') {
									$price_dics = $line_cost - $harga_setelah_disc;
									$line_cost = $price_dics;
								} /* else {
									// $price_dics = 0;
								} */
								$total += $price_dics;
							}
							// if ($discount != '' && $disc_per_item == '') {
								// $line_cost = $pricenormal * $quantity;
								// $discount = $disc_per_item;
								// if (substr_count($disc_per_item, '%') == 1) {
									// $discount = $disc_per_item;
									// $nominal = str_replace('%', '', $disc_per_item);
									// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
									
									// $disc = 100 - $nominal;
									// $temp = (($disc/100) * $line_cost);
									// $line_cost = $temp;
								// } else {
									// $discount = $disc_per_item;
									// // $discount = number_format($discount, 0, ',', '.');
									// $price_dics = $pricenormal - $disc_per_item;
									
									// $line_cost = $price_dics * $quantity;
								// }
							// } else if ($discount != '' && $disc_per_item != '') {
								// $line_cost = $pricenormal * $quantity;
								// $discount = $disc_per_item;
								// if (substr_count($disc_per_item, '%') == 1) {
									// $discount = $disc_per_item;
									// $nominal = str_replace('%', '', $disc_per_item);
									// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
									
									// $disc = 100 - $nominal;
									// $temp = (($disc/100) * $line_cost);
									// $line_cost = $temp;
								// } else {
									// $discount = $disc_per_item;
									// // $discount = number_format($discount, 0, ',', '.');
									// $price_dics = $pricenormal - $disc_per_item;
									
									// $line_cost = $price_dics * $quantity;
								// }
							// } else if ($discount == '' && $disc_per_item != '') {
								// $line_cost = $pricenormal * $quantity;
								// $discount = $disc_per_item;
								// if (substr_count($disc_per_item, '%') == 1) {
									// $discount = $disc_per_item;
									// $nominal = str_replace('%', '', $disc_per_item);
									// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
									
									// $disc = 100 - $nominal;
									// $temp = (($disc/100) * $line_cost);
									// $line_cost = $temp;
								// } else {
									// // $discount = number_format($discount, 0, ',', '.');
									// $price_dics = $pricenormal - $disc_per_item;
									
									// $line_cost = $price_dics * $quantity;
								// }
							// } else if ($discount == '') {
								// $line_cost = $pricenormal * $quantity;
								// $discount = '';
							// } else {
								// $line_cost = $price_dics * $quantity;  //work out the line cost
							// }
							// // $html .= "$price_dics = $pricenormal - $disc_per_item<br>";
							// // $html .= "$line_cost = $price_dics * $quantity<br>";
							// $total = $total + $line_cost;   //add to the total cost
							
						}
						switch ($status) {
							case 0:
								$temp = _STATUS0;
								break;
							case 1:
								$temp = _STATUS1;
								break;
							case 2:
								$temp = _STATUS2;
								break;
							case 3:
								$temp = _STATUS3;
								break;
							case 4:
								$temp = _STATUS4." $no_pengiriman";
								break;
						}
						
						if ($nominal_voucher > 0) {
					
							if ($tipe_voucher == 1) {
								$voucher = number_format($nominal_voucher, 0, ',', '.');
								$disc = $total - $nominal_voucher;
								$total = $disc;
							} else if ($tipe_voucher == 2) {
								$disc = 100 - $nominal_voucher;
								$temp_total = (($disc/100) * $total);
								$voucher = (int)$nominal_voucher . '%';
								$total = $temp_total;
							}
						} else {
							$voucher = 0;
						}
						
						// VAT
						$discount = ($total*10)/100;
						$total_price = ($discount > 0) ? $total+round($discount) : $total;
						$total_price = round($total_price);
						
						if ($order_discount != '') {
							// $voucher = number_format($nominal_voucher, 0, ',', '.');
							// $total_price = $total_price - $nominal_voucher;
							
							if (substr_count($order_discount, '%') == 1) {
								$voucher = $order_discount;
								$disc = $total*$voucher/100;
								$total = $total - $disc;
								$subtotal = $total;
							} else {
								$voucher = number_format($order_discount, 0, ',', '.');
								$total = $total - $order_discount;
								$subtotal = $total;
							}
							
							$discount = ($subtotal*10)/100;
							$total_price = ($discount > 0) ? $subtotal+round($discount) : $subtotal;
							$total_price = round($total_price);
						}
						
						$temp_url = base64_encode($txid);
						$content .= "<tr valign=\"top\"><td><a href=\"".$urlfunc->makePretty("?p=order&action=view_quotation_approval&id=$temp_url")."\">$txid</td><td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total_price,0,',','.')."</td><!--<td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td>--></tr>\r\n";
					}
					$content .= '</table>';
				} else {
					$content .= '<div class="alert alert-info">';
					$content .= '<p class="lead text-center">'._NODATAQUOTATIONAPPROVAL.'</p>';
					$content .= '</div>';
				}
			}
		}
	} else {
		$content .= _NORIGHT;
	}
}

if ($action == 'quotation_need_approval_lvl1') {
	
	if (validAccess(1)) {
		$title = _NEEDAPPROVALALERT;
		$parent = $_SESSION['member_uid'];
		if (isset($parent)) {
			$sql = "SELECT * FROM (SELECT q.autoid, q.quotation_id, m.fullname, q.tanggal_buka_transaksi, atasan_langsung, (SELECT atasan_langsung FROM webmember WHERE user_id=m.atasan_langsung) AS parent, is_from_template
					FROM sc_quotation q
					INNER JOIN sc_customer c ON c.customer_id=q.customer_id
					INNER JOIN webmember m ON m.user_id=c.web_user_id
					WHERE q.level1='1') x WHERE x.parent='$parent' OR x.atasan_langsung='$parent' ORDER BY x.autoid DESC";
					// echo "$sql<br>";
			if ($result = $mysqli->query($sql)) {
				if ($result->num_rows > 0) {
					
					$content .= '<h2>Quotation minta persetujuan approval</h2>';
					$content .= '<table class="table table-striped">';
					$content .= '<tr><th>'._CURRENTORDERTITLE.'</th><th>'._ORDERDATE.'</th><th class="text-right">'._TOTAL.'</th></tr>';
					while($row = $result->fetch_assoc()) {
						
						$txid = $row['quotation_id'];
						$txdate = $row['tanggal_buka_transaksi'];
						
						// $sql1 = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
						
						if ($row['is_from_template'] == 1) {
							$sql1 = "SELECT jumlah,td.harga,product_name,td.filename, '' as merek, td.diskon,  '' as keterangan, '' as sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, parent FROM sc_quotation_detail td WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
						} else {
							$sql1 = "SELECT jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
						}
						$result1 = $mysqli->query($sql1);
						$total = 0;
						while (list($quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item) = $result1->fetch_row()) {
							
							// 05/03/2020 14:57
							if ($row['is_from_template'] == 1) {
								if ($discount != '') {
									// $discount = ($disc_per_item != '') ? $disc_per_item : $discount;
									// $line_cost = $quantity * $price;  //work out the line cost
									// $disc = $price * (int)$discount / 100;
									// $harga_setelah_disc = $disc * $quantity;
									
									$line_cost = ($quantity * $price) - (($quantity * $price) * (int)$discount / 100);
									$total += $line_cost;
								} else {
									$line_cost = $quantity * $price;  //work out the line cost
									$total += $line_cost;
								}
							} else {
								$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
								$line_cost = $quantity * $price;  //work out the line cost
								$disc = $price * (int)$discount / 100;
								$harga_setelah_disc = $disc * $quantity;
								
								// $price_dics = $line_cost - $harga_setelah_disc;
								if ($disc_per_item != '') {
									$price_dics = $line_cost - $harga_setelah_disc;
									$line_cost = $price_dics;
								} /* else {
									// $price_dics = 0;
								} */
								$total += $price_dics;
							}
							
							// if ($discount != '' && $disc_per_item == '') {
								// $line_cost = $pricenormal * $quantity;
								// $discount = $disc_per_item;
								// if (substr_count($disc_per_item, '%') == 1) {
									// $discount = $disc_per_item;
									// $nominal = str_replace('%', '', $disc_per_item);
									// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
									
									// $disc = 100 - $nominal;
									// $temp = (($disc/100) * $line_cost);
									// $line_cost = $temp;
								// } else {
									// $discount = $disc_per_item;
									// // $discount = number_format($discount, 0, ',', '.');
									// $price_dics = $pricenormal - $disc_per_item;
									
									// $line_cost = $price_dics * $quantity;
								// }
							// } else if ($discount != '' && $disc_per_item != '') {
								// $line_cost = $pricenormal * $quantity;
								// $discount = $disc_per_item;
								// if (substr_count($disc_per_item, '%') == 1) {
									// $discount = $disc_per_item;
									// $nominal = str_replace('%', '', $disc_per_item);
									// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
									
									// $disc = 100 - $nominal;
									// $temp = (($disc/100) * $line_cost);
									// $line_cost = $temp;
								// } else {
									// $discount = $disc_per_item;
									// // $discount = number_format($discount, 0, ',', '.');
									// $price_dics = $pricenormal - $disc_per_item;
									
									// $line_cost = $price_dics * $quantity;
								// }
							// } else if ($discount == '' && $disc_per_item != '') {
								// $line_cost = $pricenormal * $quantity;
								// $discount = $disc_per_item;
								// if (substr_count($disc_per_item, '%') == 1) {
									// $discount = $disc_per_item;
									// $nominal = str_replace('%', '', $disc_per_item);
									// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
									
									// $disc = 100 - $nominal;
									// $temp = (($disc/100) * $line_cost);
									// $line_cost = $temp;
								// } else {
									// // $discount = number_format($discount, 0, ',', '.');
									// $price_dics = $pricenormal - $disc_per_item;
									
									// $line_cost = $price_dics * $quantity;
								// }
							// } else if ($discount == '') {
								// $line_cost = $pricenormal * $quantity;
								// $discount = '';
							// } else {
								// $line_cost = $price_dics * $quantity;  //work out the line cost
							// }
							// // $html .= "$price_dics = $pricenormal - $disc_per_item<br>";
							// // $html .= "$line_cost = $price_dics * $quantity<br>";
							// $total = $total + $line_cost;   //add to the total cost
							
						}
						switch ($status) {
							case 0:
								$temp = _STATUS0;
								break;
							case 1:
								$temp = _STATUS1;
								break;
							case 2:
								$temp = _STATUS2;
								break;
							case 3:
								$temp = _STATUS3;
								break;
							case 4:
								$temp = _STATUS4." $no_pengiriman";
								break;
						}
						
						if ($nominal_voucher > 0) {
					
							if ($tipe_voucher == 1) {
								$voucher = number_format($nominal_voucher, 0, ',', '.');
								$disc = $total - $nominal_voucher;
								$total = $disc;
							} else if ($tipe_voucher == 2) {
								$disc = 100 - $nominal_voucher;
								$temp_total = (($disc/100) * $total);
								$voucher = (int)$nominal_voucher . '%';
								$total = $temp_total;
							}
						} else {
							$voucher = 0;
						}
						
						// VAT
						$discount = ($total*10)/100;
						$total_price = ($discount > 0) ? $total+round($discount) : $total;
						$total_price = round($total_price);
						
						if ($order_discount != '') {
							// $voucher = number_format($nominal_voucher, 0, ',', '.');
							// $total_price = $total_price - $nominal_voucher;
							
							if (substr_count($order_discount, '%') == 1) {
								$voucher = $order_discount;
								$disc = $total*$voucher/100;
								$total = $total - $disc;
								$subtotal = $total;
							} else {
								$voucher = number_format($order_discount, 0, ',', '.');
								$total = $total - $order_discount;
								$subtotal = $total;
							}
							
							$discount = ($subtotal*10)/100;
							$total_price = ($discount > 0) ? $subtotal+round($discount) : $subtotal;
							$total_price = round($total_price);
						}
						
						$temp_url = base64_encode($txid);
						$content .= "<tr valign=\"top\"><td><a href=\"".$urlfunc->makePretty("?p=order&action=view_quotation_approval_lvl1&id=$temp_url")."\">$txid</td><td>".tglformat($txdate,true)."</td><td align=\"right\">".number_format($total_price,0,',','.')."</td><!--<td align=\"right\">".number_format($ongkir,0,',','.')."</td><td>$temp</td>--></tr>\r\n";
					}
					$content .= '</table>';
				} else {
					$content .= '<div class="alert alert-info">';
					$content .= '<p class="lead text-center">'._NODATAQUOTATIONAPPROVAL.'</p>';
					$content .= '</div>';
				}
			}
		}
	} else {
		$content .= _NORIGHT;
	}
}

if ($action=='view_quotation_approval') {

	if (validAccess(2)) {
		$member_max_discount = $_SESSION['member_max_discount'];
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$quotation_detail_id = fiestolaundry($_POST['quotation_detail_id'],15);
			$quotation_id = fiestolaundry($_POST['quotation_id'],15);
			$disc_per_item = fiestolaundry($_POST['disc_per_item'], 13);
			$max_plafon = fiestolaundry($_POST['max_plafon'], 11);	
			
			if ($_SESSION['member_company'] == 'bi') {
				$url = 'https://biestore.berca-indonesia.com/webmember/login';
			}
			if ($_SESSION['member_company'] == 'bnm') {
				$url = 'https://bnmestore.bercaniaga.co.id/webmember/login';
			}
			
			if (isset($_POST['setuju'])) {
				// $cek_diskon = ((int)$disc_per_item < $max_plafon) ? 0 : 2;
				$cek_diskon = 2;
				$sql = "UPDATE sc_quotation_detail SET disc_per_item='$disc_per_item', is_request_disc='$cek_diskon'
						WHERE quotation_detail_id='$quotation_detail_id' AND quotation_id='$quotation_id'";
				$mysqli->query($sql);
				// fiestophpmailer($parent_mail, "Form Request Plafon: $transId", $html, $smtpuser, $fullname, $email, $html, $attachment);
			}
			
			if (isset($_POST['ajukan'])) {
				// $cek_diskon = ((int)$disc_per_item < $max_plafon) ? 0 : 2;
				
				$cek_diskon = 3;
				$sql = "UPDATE sc_quotation_detail SET disc_per_item='$disc_per_item', is_request_disc='$cek_diskon'
						WHERE quotation_detail_id='$quotation_detail_id' AND quotation_id='$quotation_id'";
				$mysqli->query($sql);
				
			}
			
			if (isset($_POST['request'])) {
				$sql = "UPDATE sc_quotation SET level2=2, level1=1 WHERE quotation_id='$quotation_id'";
				$mysqli->query($sql);
				
				// fiestophpmailer($parent_mail, "Form Request Plafon: $transId", $html, $smtpuser, $fullname, $email, $html, $attachment);
				
				// Send email ke level1 dan level3
				$sql = "SELECT m.fullname, m.user_email, atasan_langsung,
						(SELECT atasan_langsung FROM webmember WHERE user_id=m.atasan_langsung) AS parent
						FROM sc_quotation q 
						INNER JOIN sc_customer c ON c.customer_id=q.customer_id 
						INNER JOIN webmember m ON m.user_id = c.web_user_id 
						WHERE quotation_id='$quotation_id'";
				$result = $mysqli->query($sql);
				list($fullname, $user_email, $atasan_langsung, $parent) = $result->fetch_row();
				$user_atasan = get_member_email($atasan_langsung);
				$user_parent = get_member_email($parent);
				
				if ($_SESSION['web_mode'] == 'bnm') {
					$smtpsecure = '';	//kosongkan jika pakai protokol normal (Non SSL)
					$smtphost 	= 'mail.bercaniaga.com';
					$smtpport 	= '587';
					$smtpuser 	= 'bnmestore@bercaniaga.com';
					$smtppass 	= 'BNMeStore123';
					$sendername = 'bnmestore@bercaniaga.com';
				}
				
				// Level 3
				$html = array(
					'/#quotation/' => $quotation_id,
					'/#link/' => '<a href="'.$url.'">'._LOGINEMAIL.'</a>'
				);

				$htmlmsg = preg_replace(array_keys($html), array_values($html), $confirm_email_level1);
				fiestophpmailer($user_parent, "Request diskon $quotation_id", $htmlmsg, $smtpuser, $smtpuser, $user_atasan, $htmlmsg);
				
				$url = $urlfunc->makePretty("?p=order&action=quotation_need_approval");
				header("Location: $url");
				exit;
			}
			
			if (isset($_POST['selesai'])) {
				$sql = "UPDATE sc_quotation SET level2='2', siap_kirim=1 WHERE quotation_id='$quotation_id'";
				$mysqli->query($sql);
				
				$sql = "SELECT m.fullname, m.user_email, atasan_langsung
						FROM sc_quotation q 
						INNER JOIN sc_customer c ON c.customer_id=q.customer_id
						INNER JOIN webmember m ON m.user_id = c.web_user_id
						WHERE quotation_id='$quotation_id'";
				$result = $mysqli->query($sql);
				list($user_name, $user_email) = $result->fetch_row();
				$user_atasan = get_member_email($atasan_langsung);
			
				if ($_SESSION['web_mode'] == 'bnm') {
					$smtpsecure = '';	//kosongkan jika pakai protokol normal (Non SSL)
					$smtphost 	= 'mail.bercaniaga.com';
					$smtpport 	= '587';
					$smtpuser 	= 'bnmestore@bercaniaga.com';
					$smtppass 	= 'BNMeStore123';
					$sendername = 'bnmestore@bercaniaga.com';
				}
				
				$html = array(
					'/#quotation/' => $quotation_id
				);
				$htmlmsg = preg_replace(array_keys($html), array_values($html), $confirm_email_level3);
				fiestophpmailer($user_email, "Request diskon $quotation_id telah disetujui", $htmlmsg, $smtpuser, $smtpuser, $user_atasan, $htmlmsg);
				
				sendEmailQuotationOrder($quotation_id, $user_email);
				
				$url = $urlfunc->makePretty("?p=order&action=quotation_need_approval");
				header("Location: $url");
				exit;
			}
		}

		$sql = "SELECT quotation_id, tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher,kurir, discount, termofpayment, deliverytime, quotationvalidity, termandcondition, is_from_template, tc_warranty, tc_delivery_franco FROM sc_quotation WHERE quotation_id='$txid'";
		$result = $mysqli->query($sql);
		list($quotation_id, $tanggal_buka_transaksi, $tanggal_tutup_transaksi, $customer_id, $receiver_id, $metode_pembayaran, $shipping, $status, $catatan, $no_pengiriman, $latestupdate, $kodevoucher, $tipe_voucher, $nominal_voucher,$kurir, $order_discount, $customTermOfPayment, $customDeliveryTime, $customQuotationValidity, $customQuotationTC, $is_from_template, $tc_warranty, $tc_delivery_franco) = $result->fetch_row();
		
		$couriers 		= json_decode($kurir, true);
		$kecamatan 		= $couriers['kota'];
		$kurir 			= $couriers['tipe'] . ' ' . $couriers['kota'];
		$kurir_tipe		= $couriers['tipe'];
		$ongkos_kirim 	= number_format($shipping, 0, ',', '.');
		$estimasi 		= $couriers['estimasi'];
		
		$content .= "<h2>"._EQUOTATION."</h2>\r\n";
		
		$sql = "SELECT nama_lengkap, email, company, alamat, alamat2, kota, provinsi, zip, telepon, cellphone1, cellphone2, nama_pengirim, telp_pengirim, email_pengirim, pic FROM sc_receiver WHERE receiver_id='$receiver_id'";
		$result = $mysqli->query($sql);
		list($nama_lengkap, $email, $company, $alamat, $alamat2, $kota, $provinsi, $zip, $telepon, $cellphone1, $cellphone2, $sendername, $senderphone, $senderemail, $pic) = $result->fetch_row();
		$temp_address = ($alamat2 != '') ? "$alamat<br>$alamat2" : $alamat;
		
		if ($pic != '') {
			$temp_pic = '
			<tr>
				<td valign="top">'._QUOTATIONPREPAREBY.'</td>
				<td valign="top">:</td>
				<td>'.$pic.'</td>
			</tr>';
		} else {
			$temp_pic = '';
		}
		
		$content .= '
		<div class="table_block table-responsive table-small-gap">
		<table class="header-table-quotation">
			<tr>
				<td width="48%">
					<table>
						<tr valign="top">
							<td width="10%">'._QUOTATIONTO.'</td>
							<td width="8%" align="left">:</td>
							<td width="82%">
								'.$company.'<br>
								'.$temp_address.'<br>
								'.$kota.'<br>
								'.$zip.'<br>
								Phone: '.$cellphone1.'
								<p style="line-height:0.8;">&nbsp;</p>
							</td>
						</tr>
						<tr valign="top">
							<td>'._QUOTATIONATTN.'</td>
							<td>:</td>
							<td>
								'.$nama_lengkap.'<br>
								'.$email.'
							</td>
						</tr>
					</table>
				</td>
				<td width="4%">&nbsp;</td>
				<td width="50%" valign="top">
					<table>
						<tr>
							<td width="27%">'._QUOTATIONDATE.'</td>
							<td width="3%">:</td>
							<td width="70%">'.tglformat($tanggal_buka_transaksi).'</td>
						</tr>
						<tr>
							<td>'._QUOTATIONNUMBER.'</td>
							<td>:</td>
							<td>'.$txid.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						'.$temp_pic.'
						<!--<tr>
							<td>'._EMAIL.'</td>
							<td>:</td>
							<td>'.$email.'</td>
						</tr>-->
					</table>
				</td>
			</tr>
		</table>
		</div>
		';
		
		$content .= '
		<p style="line-height:0.6;">&nbsp;</p>
		<div class="table_block table-responsive table-small-gap">
		<table border="0" class="table table-bordered" width="100%" cellpadding="7">
			<tr style="background-color:#f5f5f5;border-bottom: 1px solid #aaa;font-weight:bold">
				<th style="text-align:center" width="8%" style="border-bottom: 1px solid #aaa;">'._ITEM.'</th>
				<th style="text-align:center" width="35%" style="border-bottom: 1px solid #aaa;">'._PARTNUMBER.'</th>
				<th style="text-align:center" width="20%" style="border-bottom: 1px solid #aaa;">'._UNITPRICE.'</th>
				<th style="text-align:center" width="10%" style="text-align:center;border-bottom: 1px solid #aaa;">'._DISCOUNT.'</th>
				<!--<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._QTY.'</th>-->
				<th style="text-align:center" width="7%" style="border-bottom: 1px solid #aaa;">Qty</th>
				<th style="text-align:right" width="20%" style="border-bottom: 1px solid #aaa;">'._TOTAIDR.'</th>
			</tr>
		';
		$item = 0;
		$total = 0;
		
		// $sql = "SELECT quotation_detail_id,jumlah,td.harga,product_id,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, is_request_disc FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id <> $cat_loose_item ORDER BY quotation_detail_id";
		
		if ($is_from_template == 1) {
			$sql = "SELECT quotation_detail_id, jumlah,td.harga,product_name,td.filename, '' as merek, td.diskon,  '' as keterangan, '' as sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, parent, is_request_disc  FROM sc_quotation_detail td WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
		} else {
			$sql = "SELECT quotation_detail_id, jumlah,td.harga,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, parent, is_request_disc  FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id <> $cat_loose_item ORDER BY quotation_detail_id";
		}
		// echo $sql;
		$no_parent = 1;
		$result = $mysqli->query($sql);
		while (list($quotation_detail_id, $quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $parent, $is_request_disc) = $result->fetch_row()) {
			
			$brand_name = get_brand_name($idmerek);
			$parent = $parent == 0 ? $no_parent++ : "";
			// $line_cost = $quantity * $price;  //work out the line cost
			// $total = $total + $line_cost;   //add to the total cost
			
			// 05/03/2020 14:57
			if ($is_from_template == 1) {
				if ($discount != '') {
					$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
					$line_cost = ($quantity * $price) - (($quantity * $price) * (int)$discount / 100);
					$total += $line_cost;
				} else {
					$line_cost = $quantity * $price;  //work out the line cost
					$total += $line_cost;
				}
			} else {
				$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
				$line_cost = $quantity * $price;  //work out the line cost
				$disc = $price * (int)$discount / 100;
				$harga_setelah_disc = $disc * $quantity;
				
				// $price_dics = $line_cost - $harga_setelah_disc;
				if ($disc_per_item != '') {
					$price_dics = $line_cost - $harga_setelah_disc;
					$line_cost = $price_dics;
				} /* else {
					// $price_dics = 0;
				} */
				$total += $price_dics;
			}
			
			$suffix_disc = (substr_count($discount, '%') == 1) ? "$discount" : "$discount%";
			if ($discount != '') $discount = $suffix_disc;
		
			$sql_plafon = "SELECT keterangan, product_id FROM sc_quotation_plafon WHERE quotation_id='$quotation_id' AND product_id='$product_id'";
			$result_plafon = $mysqli->query($sql_plafon);
			list($plafon_keterangan, $plafon_product_id) = $result_plafon->fetch_row();
			
			$keterangan = ($plafon_keterangan != '') ? "<br><small>Ket: $plafon_keterangan</small>" : "";
			if ($is_request_disc == 1) {
				$datatambahan = <<<HTML
				  <div class="form-group">
					<div class="input-group">
					  <input type="text" class="text-center discount_input disc_per_item" value="$discount" name="disc_per_item" id="disc_per_item$quotation_detail_id" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
					  <div class="input-group-addon">%</div>
					</div>
				  </div>
HTML;
				$form_required_approval = <<<HTML
					<div id="discount-per-item">
					<form method="POST" action="#" class="form-inline">
						<input type="hidden" name="quotation_detail_id" value="$quotation_detail_id">
						<input type="hidden" name="quotation_id" value="$quotation_id">
						<input type="hidden" name="max_plafon" value="$member_max_discount">
						<!--<input type="text" class="text-center discount_input disc_per_item" value="$discount" name="disc_per_item" id="disc_per_item$quotation_detail_id">-->
						$datatambahan
						<button type="submit" name="setuju" id="setuju$quotation_detail_id" class="btn btn-default more button_discount_input">Setuju</button>
						<button type="submit" name="ajukan" id="ajukan$quotation_detail_id" style="display:none" class="btn btn-default more button_discount_input">Ajukan</button>
					</form>
					</div>
HTML;
			} else {
				$signdisc = ($discount > 0) ? '' : '';
				$form_required_approval = <<<HTML
					$discount$signdisc
HTML;
			}
			
			$item++;
			// $price = ($price > 0) ? $price : '';
			// (negative_check($line_cost)) ? number_format($line_cost, 0, ',', '.') : ""
			$product_price = ($price > 0) ? number_format($price, 0, ',', '.') : (negative_check($price)) ? number_format($price, 0, ',', '.') : "";
			// $line_cost = ($line_cost > 0) ? $line_cost : "";
			$line_cost = ($line_cost > 0) ? $line_cost : (negative_check($line_cost)) ? number_format($line_cost, 0, ',', '.') : "";
			$is_product_code = ($product_code != '') ? '<p class="product-name"><strong>'.$product_code.'</strong></p>' : '';
			
			if ($is_from_template == 1) {
				$no = $parent;
			} else {
				$no = $item;
			}
			$red_class = negative_check($price) ? ';color:red' : '';

			$content .= '
			<tr>
				<td style="text-align:center">'.$no.'</td>
				<td>
					'.$is_product_code.'
					<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
				</td>
				<td style="text-align:right'.$red_class.'">'.$product_price.'</td>
				<!--<td style="text-align:center">'.$discount.'</td>-->
				<td style="text-align:center">'.$form_required_approval.'</td>
				<td style="text-align:center">'.$quantity.'</td>
				<td style="text-align:right'.$red_class.'">'.$line_cost.'</td>
			</tr>
			';
			
			// $brand_name = get_brand_name($idmerek);
			// // $line_cost = $quantity * $price;  //work out the line cost
			// // $total = $total + $line_cost;   //add to the total cost
			
			// if ($discount != '' && $disc_per_item == '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $discount = $disc_per_item;
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;
				
			// } else if ($discount != '' && $disc_per_item != '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $discount = $disc_per_item;
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;

			// } else if ($discount == '' && $disc_per_item != '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $discount = $disc_per_item;
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;

			// } else if ($discount == '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = '';
			// } else {
				// $line_cost = $price * $quantity;  //work out the line cost
			// }
			// $total = $total + $line_cost;   //add to the total cost
			// // $total = (int)$total;
		
			// $sql_plafon = "SELECT keterangan, product_id FROM sc_quotation_plafon WHERE quotation_id='$quotation_id' AND product_id='$product_id'";
			// $result_plafon = $mysqli->query($sql_plafon);
			// list($plafon_keterangan, $plafon_product_id) = $result_plafon->fetch_row();
			
			// $keterangan = ($plafon_keterangan != '') ? "<br><small>Ket: $plafon_keterangan</small>" : "";
			// if ($is_request_disc == 1) {
				// $datatambahan = <<<HTML
				  // <div class="form-group">
					// <div class="input-group">
					  // <input type="text" class="text-center discount_input disc_per_item" value="$discount" name="disc_per_item" id="disc_per_item$quotation_detail_id" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
					  // <div class="input-group-addon">%</div>
					// </div>
				  // </div>
// HTML;
				// $form_required_approval = <<<HTML
					// <div id="discount-per-item">
					// <form method="POST" action="#" class="form-inline">
						// <input type="hidden" name="quotation_detail_id" value="$quotation_detail_id">
						// <input type="hidden" name="quotation_id" value="$quotation_id">
						// <input type="hidden" name="max_plafon" value="$member_max_discount">
						// <!--<input type="text" class="text-center discount_input disc_per_item" value="$discount" name="disc_per_item" id="disc_per_item$quotation_detail_id">-->
						// $datatambahan
						// <button type="submit" name="setuju" id="setuju$quotation_detail_id" class="btn btn-default more button_discount_input">Setuju</button>
						// <button type="submit" name="ajukan" id="ajukan$quotation_detail_id" style="display:none" class="btn btn-default more button_discount_input">Ajukan</button>
					// </form>
					// </div>
// HTML;
			// } else {
				// $signdisc = ($discount > 0) ? '%' : '';
				// $form_required_approval = <<<HTML
					// $discount$signdisc
// HTML;
			// }
			// $item++;
			// $content .= '
			// <tr>
				// <td style="text-align:center">'.$item.'</td>
				// <td>
					// <p class="product-name"><strong>'.$product_code.'</strong></p>
					// <span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
					// '.$keterangan.'
				// </td>
				// <td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
				// <!--<td style="text-align:center">'.$discount.'</td>-->
				// <td style="text-align:center">'.$form_required_approval.'</td>
				// <td style="text-align:center">'.$quantity.'</td>
				// <td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
			// </tr>
			// ';
		}
		
		$sql = "SELECT quotation_detail_id,jumlah,td.harga,product_id,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, is_request_disc FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id = $cat_loose_item ORDER BY quotation_detail_id";
		$result = $mysqli->query($sql);
		if ($result->num_rows > 0) {
			$content .= "<tr style=\"background: #eee;font-weight: bold;\"><td colspan=\"6\">"._LOOSEITEM."</td></tr>";
			while (list($quotation_detail_id, $quantity, $price, $product_id, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $is_request_disc) = $result->fetch_row()) {
				$brand_name = get_brand_name($idmerek);
				// $line_cost = $quantity * $price;  //work out the line cost
				// $total = $total + $line_cost;   //add to the total cost
				
				if ($discount != '' && $disc_per_item == '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$discount = $disc_per_item;
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount != '' && $disc_per_item != '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$discount = $disc_per_item;
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;

				} else if ($discount == '' && $disc_per_item != '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$discount = $disc_per_item;
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;

				} else if ($discount == '') {
					$line_cost = $pricenormal * $quantity;
					$discount = '';
				} else {
					$line_cost = $price * $quantity;  //work out the line cost
				}
				$total = $total + $line_cost;   //add to the total cost
				// $total = (int)$total;
			
				$sql_plafon = "SELECT keterangan, product_id FROM sc_quotation_plafon WHERE quotation_id='$quotation_id' AND product_id='$product_id'";
				$result_plafon = $mysqli->query($sql_plafon);
				list($plafon_keterangan, $plafon_product_id) = $result_plafon->fetch_row();
				
				$keterangan = ($plafon_keterangan != '') ? "<br><small>Ket: $plafon_keterangan</small>" : "";
				if ($is_request_disc == 1) {
					$datatambahan = <<<HTML
					  <div class="form-group">
						<div class="input-group">
						  <input type="text" class="text-center discount_input disc_per_item" value="$discount" name="disc_per_item" id="disc_per_item$quotation_detail_id" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
						  <div class="input-group-addon">%</div>
						</div>
					  </div>
HTML;
					$form_required_approval = <<<HTML
						<form method="POST" action="#" class="form-inline">
							<input type="hidden" name="quotation_detail_id" value="$quotation_detail_id">
							<input type="hidden" name="quotation_id" value="$quotation_id">
							<input type="hidden" name="max_plafon" value="$member_max_discount">
							<!--<input type="text" class="text-center discount_input disc_per_item" value="$discount" name="disc_per_item" id="disc_per_item$quotation_detail_id">-->
							$datatambahan
							<button type="submit" name="setuju" id="setuju$quotation_detail_id" class="btn btn-default more button_discount_input">Setuju</button>
							<button type="submit" name="ajukan" id="ajukan$quotation_detail_id" style="display:none" class="btn btn-default more button_discount_input">Ajukan</button>
						</form>
HTML;
				} else {
					$signdisc = ($discount > 0) ? '%' : '';
					$form_required_approval = <<<HTML
						$discount$signdisc
HTML;
				}
				$item++;
				$content .= '
				<tr>
					<td style="text-align:center">'.$item.'</td>
					<td>
						<p class="product-name"><strong>'.$product_code.'</strong></p>
						<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
						'.$keterangan.'
					</td>
					<td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
					<!--<td style="text-align:center">'.$discount.'</td>-->
					<td style="text-align:center">'.$form_required_approval.'</td>
					<td style="text-align:center">'.$quantity.'</td>
					<td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
				</tr>
				';
			}
		}
		
		$discount = ($total*10)/100;
		$total_price = ($discount > 0) ? $total+round($discount) : $total;
		$total_price = round($total_price);
		
		$termofpayment = ($customTermOfPayment != '') ? $customTermOfPayment : '';
		$deliverytime = ($customDeliveryTime != '') ? $customDeliveryTime : '';
		$deliveryfranco = ($deliveryfranco != '') ? $deliveryfranco : '';
		$warranty = ($warranty != '') ? $warranty : '';
		$quotationvalidity = ($quotationvalidity != '') ? $quotationvalidity : '';
		// $quotationvalidity = ($customQuotationValidity != '') ? $customQuotationValidity : '';
		$termandcondition = ($customQuotationTC != '') ? $customQuotationTC : '';
		
		$content .= '
		<tr>
			<td style="border-top: 1px solid #aaa;font-weight: bold;" colspan="5" align="right">'._TOTALLISTPRICE.'</td>
			<td align="right" style="border-top: 1px solid #aaa;">'.number_format($total, 0, ',', '.').'</td>
		</tr>
		';
		
		if ($order_discount != '') {
			
			if (substr_count($order_discount, '%') == 1) {
				$voucher = $order_discount;
				$disc = $total*$voucher/100;
				$total = $total - $disc;
				$subtotal = $total;
				$discount_type = _DISCOUNTPERCENT;
			} else {
				$voucher = number_format($order_discount, 0, ',', '.');
				$total = $total - $order_discount;
				$subtotal = $total;
				$discount_type = _DISCOUNTIDR;
			}
			
			$discount = ($subtotal*10)/100;
			$total_price = ($discount > 0) ? $subtotal+$discount : $subtotal;
			$total_price = round($total_price);
			
			$content .= '
			<tr>
				<td colspan="5" align="right" style="font-weight: bold;">'.$discount_type.'</td>
				<td align="right">('.((substr_count($order_discount, '%') == 1) ? $order_discount : number_format($order_discount, 0, ',', '.')).')</td>
			</tr>
			';
			
			$content .= '
			<tr>
				<td colspan="5" align="right" style="font-weight: bold;">'._SUBTOTAL.'</td>
				<td align="right">'.number_format($subtotal, 0, ',', '.').'</td>
			</tr>
			';
		}
		$terbilang = convertNumber($total_price);	//bilangan($total_price);
		
		$content .= '
		<tr>
			<td colspan="5" align="right" style="font-weight: bold;">'._VATIDR.'</td>
			<td align="right">'.number_format($discount, 0, ',', '.').'</td>
		</tr>
		<tr>
			<td colspan="5" align="right" style="font-weight: bold;">'._TOTAIDR.'</td>
			<td align="right">'.number_format($total_price, 0, ',', '.').'</td>
		</tr>
		<tr style="background-color:#f5f5f5;">
			<td colspan="6" style="border-top:1px solid #aaa; font-weight:bold;solid #aaa">'._TERBILANG.': '.$terbilang.'</td>
		</tr>
		</table>
		</div>
		';
		
		// $sql = "SELECT * FROM sc_quotation_detail WHERE quotation_id='$quotation_id'";
		$sql = "SELECT d.disc_per_item, d.is_request_disc
				FROM sc_quotation q 
				INNER JOIN sc_quotation_detail d ON d.quotation_id=q.quotation_id
				WHERE q.quotation_id='$quotation_id' AND q.level2<2";
				// echo $sql;
		$result = $mysqli->query($sql);
		// $col_request = 0;
		$process_is_done = 0;
		$process_to_level1 = 0;
		while($row = $result->fetch_assoc()) {
			// echo "{$row['disc_per_item']} > $member_max_discount<br>";
			// if ((int)$row['disc_per_item'] > $member_max_discount && $row['is_request_disc'] == 2) {
				// $col_request++;
				// $quotation_is_process = false;
			// }
			
			if ((float)$row['disc_per_item'] > $member_max_discount && $row['is_request_disc'] == 3) {
				$process_to_level1++;
			} else if (/* $row['is_request_disc'] == 0 ||  */$row['is_request_disc'] == 2) {
				$process_is_done++;
			}
		}
		
		$countQuotationDetail = countQuotationDetail($quotation_id);
		$countQuotationDetailLevel1 = countQuotationDetail($quotation_id, 1);
		
		// echo '<br>';
		// echo "process_to_level1: $process_to_level1 | process_is_done: $process_is_done | countQuotationDetail: $countQuotationDetail | countQuotationDetailLevel1: $countQuotationDetailLevel1 ";
		$content .= '<div class="text-center">';
		if ($process_to_level1 > 0 && $countQuotationDetailLevel1 == $countQuotationDetail) {
			$content .= '<div class="alert alert-danger">';
			$content .= '<p>'._REQUESTTOLEVEL1.'</p>';
			$content .= '</div>';
			$content .= '<form method="POST" action="#">';
			$content .= '<input type="hidden" name="quotation_id" value="'.$quotation_id.'">';
			$content .= '<input type="submit" class="btn btn-primary more" name="request" value="Request">';
			$content .= '</form>';
		}
		// if ($process_is_done > 0) {
		if ($process_is_done == $countQuotationDetail) {
			$content .= '<form method="POST" action="#">';
			$content .= '<input type="hidden" name="quotation_id" value="'.$quotation_id.'">';
			$content .= '<input type="submit" class="btn btn-primary more" name="selesai" value="Selesai">';
			$content .= '</form>';
		}
		$content .= '</div>';
		
		// $content .= '<div class="text-center">';
		// if ($col_request > 0) {
			// $content .= '<div class="alert alert-danger">';
			// $content .= '<p>Terdapat data diatas plafon Anda. Silakan ajukan request ke atasan Anda.</p>';
			// $content .= '</div>';
			// $content .= '<form method="POST" action="#">';
			// $content .= '<input type="hidden" name="quotation_id" value="'.$quotation_id.'">';
			// $content .= '<input type="submit" class="btn btn-primary" name="request" value="Request">';
			// $content .= '</form>';
		// } else {
			// $content .= '<form method="POST" action="#">';
			// $content .= '<input type="hidden" name="quotation_id" value="'.$quotation_id.'">';
			// $content .= '<input type="submit" class="btn btn-primary" name="selesai" value="Selesai">';
			// $content .= '</form>';
		// }
		// $content .= '</div>';
		
		// if ($catatan != '') {
			// $content .= '
			// <p>
				// <u><b>'._NOTE.'</b></u><br/>
				// '.$catatan.'
			// </p>
			// ';
		// } else {
			// $content .= '<p>'.$additional_note.'<br/></p>';
		// }
		
		$content .= '<p><u><b>'._NOTE.'</b></u><br/>';
		if ($catatan != '') {
			
			$content .= $catatan.'<br/>'.$additional_note;
		} else {
			$content .= $additional_note.'<br/>';
		}
		$content .= '</p>';
		
		$berca_estore = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? '<img src="'.$cfg_app_url.'/images/berca-estore.jpeg" style="height:23px" >' : '<img src="'.$cfg_app_url.'/images/berca-estore-bnm.png" style="height:32px" >';
		
		if ($is_from_template == 1) {
			$content .= '
			<p>
				<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
				<table>
					<tr>
						<td style="width: 150px;">Delivery Time</td>
						<td style="width: 550px;">: '.$deliverytime.'</td>
					</tr>
					<tr>
						<td>Term of Payment</td>
						<td>: '.$termofpayment.'</td>
					</tr>
					<tr>
						<td>Quotation Validity</td>
						<td>: '.$quotationvalidity.'</td>
					</tr>
					<tr>
						<td>Delivery Franco</td>
						<td>: '.$tc_delivery_franco.'</td>
					</tr>
					<tr>
						<td>Warranty</td>
						<td>: '.$tc_warranty.'</td>
					</tr>
				</table>
			</p>';
		} else {
			$content .= '
			<p>
				<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
				'.$termandcondition.'
			</p>';
		}
		$content .= '
		<!--<p>
			<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
			'._TERMOFPAYMENT.': '.$termofpayment.'<br/>
			'._DELIVERYTIME.': '.$deliverytime.'<br/>
			'._DELIVERYFRANCO.': '.$deliveryfranco.'<br/>
			'.(($warranty != '') ? _WARRANTY.': '.$warranty.'<br/>' : '').'
			'._QUOTATIONVALIDITY.': '.$quotationvalidity.'
		</p>-->
		<!--<p>
			<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
			'.$termandcondition.'
		</p>-->';
		$content .= '
		<p>'._SINCERELYYOURS.',</p>
		<p>'.$berca_estore.'</p>
		<div class="automatically-generate" style="background-color:#eee;margin-bottom:30px;margin-top:30px;text-align:center;line-height:35px">'.$quotationvalidity.'</div>
		';
		
		$_pid = base64_encode($txid);
		// Hanya jika level 3
		// $content .= '<div class="downloadpdf"><a class="btn btn-default more downloadpdf-button" href="'.$urlfunc->makePretty("?p=order&action=downloadpdf&pid=$_pid").'" target="_blank"><img src="'.$cfg_app_url.'/images/pdf-icon.svg">'._DOWNLOADPDF.'</a></div>';
		
	} else {
		$content .= _NORIGHT;
	}
}

if ($action=='view_quotation_approval_lvl1') {
	
	if (validAccess(1)) {
		$member_max_discount = $_SESSION['member_max_discount'];
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$quotation_detail_id = fiestolaundry($_POST['quotation_detail_id'],15);
			$quotation_id = fiestolaundry($_POST['quotation_id'],15);
			$disc_per_item = fiestolaundry($_POST['disc_per_item'], 13);
			$max_plafon = fiestolaundry($_POST['max_plafon'], 11);	
			
			if (isset($_POST['setuju'])) {
				// $cek_diskon = ((int)$disc_per_item < $max_plafon) ? 0 : 2;
				
				$cek_diskon = 4;
				$sql = "UPDATE sc_quotation_detail SET disc_per_item='$disc_per_item', is_request_disc='$cek_diskon'
						WHERE quotation_detail_id='$quotation_detail_id' AND quotation_id='$quotation_id'";
				$mysqli->query($sql);
				
				// $sql = "UPDATE sc_quotation SET level1='2', siap_kirim='1' WHERE quotation_id='$quotation_id'";
				// $mysqli->query($sql);
				
				// // Send email ke level1 dan level3
				// $sql = "SELECT m.fullname, m.user_email, m.atasan_langsung
						// FROM sc_quotation q 
						// INNER JOIN sc_customer c ON c.customer_id=q.customer_id 
						// INNER JOIN webmember m ON m.user_id = c.web_user_id 
						// WHERE quotation_id='$quotation_id'";
				// $result = $mysqli->query($sql);
				// list($fullname, $user_email, $atasan_langsung) = $result->fetch_row();
				// $user_parent = get_member_email($atasan_langsung);
				
				// // Level 2
				// $isAllowEmpty = true;
				// $htmlmsg = null;
				// fiestophpmailer($user_parent, "Request diskon $quotation_id telah disetujui", $htmlmsg, $smtpuser, $smtpuser);
				
				// // Level 3
				// $html = array(
					// '/#quotation/' => $quotation_id
				// );
				// $htmlmsg = preg_replace(array_keys($html), array_values($html), $confirm_email_level3);
				// fiestophpmailer($user_email, "Request diskon $quotation_id telah disetujui", $htmlmsg, $smtpuser, $smtpuser);
				
				// $url = $urlfunc->makePretty("?p=order&action=quotation_need_approval_lvl1");
				// header("Location: $url");
				// exit;
				
			}
			
			/* if (isset($_POST['ajukan'])) {
				// $cek_diskon = ((int)$disc_per_item < $max_plafon) ? 0 : 2;
				
				$cek_diskon = 3;
				$sql = "UPDATE sc_quotation_detail SET disc_per_item='$disc_per_item', is_request_disc='$cek_diskon'
						WHERE quotation_detail_id='$quotation_detail_id' AND quotation_id='$quotation_id'";
				$mysqli->query($sql);
				
				$sql = "UPDATE sc_quotation SET level1='1', level2='2' WHERE quotation_id='$quotation_id'";
				$mysqli->query($sql);
				
				// Send email ke level1 dan level3
				$sql = "SELECT m.fullname, m.user_email, 
						(SELECT atasan_langsung FROM webmember WHERE user_id=m.atasan_langsung) AS parent
						FROM sc_quotation q 
						INNER JOIN sc_customer c ON c.customer_id=q.customer_id 
						INNER JOIN webmember m ON m.user_id = c.web_user_id 
						WHERE quotation_id='$quotation_id'";
				$result = $mysqli->query($sql);
				list($fullname, $user_email, $parent) = $result->fetch_row();
				$user_parent = get_member_email($parent);
				
				// Level 1
				$isAllowEmpty = true;
				$htmlmsg = null;
				fiestophpmailer($user_parent, "Form Request Plafon: $quotation_id Level 1", $htmlmsg, $smtpuser, $smtpuser);
				
				// Level 3
				$html = array(
					'/#quotation/' => $quotation_id
				);
				$htmlmsg = preg_replace(array_keys($html), array_values($html), $confirm_email_level3);
				fiestophpmailer($user_email, "Form Request Plafon: $quotation_id di ajukan ke Direktur", $htmlmsg, $smtpuser, $smtpuser);
				
				$url = $urlfunc->makePretty("?p=order&action=quotation_need_approval");
				header("Location: $url");
				exit;
			}
			
			if (isset($_POST['request'])) {
				$sql = "UPDATE sc_quotation SET level2=2, level1=1 WHERE quotation_id='$quotation_id'";
				$mysqli->query($sql);
				// fiestophpmailer($parent_mail, "Form Request Plafon: $transId", $html, $smtpuser, $fullname, $email, $html, $attachment);
			}
			if (isset($_POST['selesai'])) {
				$sql = "UPDATE sc_quotation SET level2='2', siap_kirim=1 WHERE quotation_id='$quotation_id'";
				$mysqli->query($sql);
				
				$sql = "SELECT m.fullname, m.user_email
						FROM sc_quotation q 
						INNER JOIN sc_customer c ON c.customer_id=q.customer_id
						INNER JOIN webmember m ON m.user_id = c.web_user_id
						WHERE quotation_id='$quotation_id'";
				$result = $mysqli->query($sql);
				list($user_name, $user_email) = $result->fetch_row();
			
				$html = array(
					'/#quotation/' => $quotation_id
				);
				$htmlmsg = preg_replace(array_keys($html), array_values($html), $confirm_email_level3);
				fiestophpmailer($user_email, "Form Request Plafon: $quotation_id telah selesai", $htmlmsg, $smtpuser, $smtpuser);
				
				$url = $urlfunc->makePretty("?p=order&action=quotation_need_approval");
				header("Location: $url");
				exit;
			} */
			
			
			if (isset($_POST['selesai'])) {
				$sql = "UPDATE sc_quotation SET level1='2', siap_kirim=1 WHERE quotation_id='$quotation_id'";
				$mysqli->query($sql);
				
				// Send email ke level1 dan level3
				$sql = "SELECT m.fullname, m.user_email, m.atasan_langsung, (SELECT atasan_langsung FROM webmember WHERE user_id=m.atasan_langsung) AS parent
						FROM sc_quotation q 
						INNER JOIN sc_customer c ON c.customer_id=q.customer_id 
						INNER JOIN webmember m ON m.user_id = c.web_user_id 
						WHERE quotation_id='$quotation_id'";
				$result = $mysqli->query($sql);
				list($fullname, $user_email, $atasan_langsung, $user_parent) = $result->fetch_row();
				$user_atasan = get_member_email($atasan_langsung);
				$user_parent = get_member_email($atasan_langsung);
				
				/* // Level 2
				$isAllowEmpty = true;
				$htmlmsg = null;
				fiestophpmailer($user_parent, "Request diskon $quotation_id telah disetujui", $htmlmsg, $smtpuser, $smtpuser);
				
				// Level 3
				$html = array(
					'/#quotation/' => $quotation_id
				);
				$htmlmsg = preg_replace(array_keys($html), array_values($html), $confirm_email_level3);
				fiestophpmailer($user_email, "Request diskon $quotation_id telah disetujui", $htmlmsg, $smtpuser, $smtpuser);
				
				$url = $urlfunc->makePretty("?p=order&action=quotation_need_approval_lvl1");
				header("Location: $url");
				exit; */
				
				if ($_SESSION['member_company'] == 'bi') {
					$url = 'https://biestore.berca-indonesia.com/webmember/login';
				}
				if ($_SESSION['member_company'] == 'bnm') {
					$url = 'https://bnmestore.bercaniaga.co.id/webmember/login';
				}
				
				if ($_SESSION['web_mode'] == 'bnm') {
					$smtpsecure = '';	//kosongkan jika pakai protokol normal (Non SSL)
					$smtphost 	= 'mail.bercaniaga.com';
					$smtpport 	= '587';
					$smtpuser 	= 'bnmestore@bercaniaga.com';
					$smtppass 	= 'BNMeStore123';
					$sendername = 'bnmestore@bercaniaga.com';
				}			
				
				// Level 2
				$html = array(
					'/#quotation/' => $quotation_id,
					'/#link/' => '<a href="'.$url.'">'._LOGINEMAIL.'</a>'
					
				);
				$htmlmsg = preg_replace(array_keys($html), array_values($html), $confirm_email_level2);
				// fiestophpmailer($user_atasan, "Request diskon $quotation_id telah disetujui", $htmlmsg, $smtpuser, $smtpuser, $user_parent, $htmlmsg);
				fiestophpmailer($user_atasan, "Request diskon $quotation_id telah disetujui", $htmlmsg, $smtpuser, $smtpuser, $smtpuser, $htmlmsg);
				
				// Level 3
				$html = array(
					'/#quotation/' => $quotation_id,
					'/#link/' => '<a href="'.$url.'">'._LOGINEMAIL.'</a>'
				);
				$htmlmsg = preg_replace(array_keys($html), array_values($html), $confirm_email_level3);
				fiestophpmailer($user_email, "Request diskon $quotation_id telah disetujui", $htmlmsg, $smtpuser, $smtpuser, $user_atasan, $htmlmsg);
				
				sendEmailQuotationOrder($quotation_id, $user_email);
				
				$url = $urlfunc->makePretty("?p=order&action=quotation_need_approval_lvl1");
				header("Location: $url");
				exit;
			}
		}

		$sql = "SELECT quotation_id, tanggal_buka_transaksi, tanggal_tutup_transaksi, customer_id, receiver_id, metode_pembayaran, ongkir, status, catatan, no_pengiriman, latestupdate, kodevoucher, tipe_voucher, nominal_voucher,kurir, discount, termofpayment, deliverytime, quotationvalidity, termandcondition, is_from_template, tc_warranty, tc_delivery_franco FROM sc_quotation WHERE quotation_id='$txid'";
		
		$result = $mysqli->query($sql);
		list($quotation_id, $tanggal_buka_transaksi, $tanggal_tutup_transaksi, $customer_id, $receiver_id, $metode_pembayaran, $shipping, $status, $catatan, $no_pengiriman, $latestupdate, $kodevoucher, $tipe_voucher, $nominal_voucher,$kurir, $order_discount, $customTermOfPayment, $customDeliveryTime, $customQuotationValidity, $customQuotationTC, $is_from_template, $tc_warranty, $tc_delivery_franco) = $result->fetch_row();
		
		$couriers 		= json_decode($kurir, true);
		$kecamatan 		= $couriers['kota'];
		$kurir 			= $couriers['tipe'] . ' ' . $couriers['kota'];
		$kurir_tipe		= $couriers['tipe'];
		$ongkos_kirim 	= number_format($shipping, 0, ',', '.');
		$estimasi 		= $couriers['estimasi'];
		
		$content .= "<h2>"._EQUOTATION."</h2>\r\n";
		
		$sql = "SELECT nama_lengkap, email, company, alamat, alamat2, kota, provinsi, zip, telepon, cellphone1, cellphone2, nama_pengirim, telp_pengirim, email_pengirim, pic  FROM sc_receiver WHERE receiver_id='$receiver_id'";
		$result = $mysqli->query($sql);
		list($nama_lengkap, $email, $company, $alamat, $alamat2, $kota, $provinsi, $zip, $telepon, $cellphone1, $cellphone2, $sendername, $senderphone, $senderemail, $pic) = $result->fetch_row();
		$temp_address = ($alamat2 != '') ? "$alamat<br>$alamat2" : $alamat;
		
		if ($pic != '') {
			$temp_pic = '
			<tr>
				<td valign="top">'._QUOTATIONPREPAREBY.'</td>
				<td valign="top">:</td>
				<td>'.$pic.'</td>
			</tr>';
		} else {
			$temp_pic = '';
		}
		
		$content .= '
		<div class="table_block table-responsive table-small-gap">
		<table class="header-table-quotation">
			<tr>
				<td width="48%">
					<table>
						<tr valign="top">
							<td width="10%">'._QUOTATIONTO.'</td>
							<td width="8%" align="left">:</td>
							<td width="82%">
								'.$company.'<br>
								'.$temp_address.'<br>
								'.$kota.'<br>
								'.$zip.'<br>
								Phone: '.$cellphone1.'
								<p style="line-height:0.8;">&nbsp;</p>
							</td>
						</tr>
						<tr valign="top">
							<td>'._QUOTATIONATTN.'</td>
							<td>:</td>
							<td>
								'.$nama_lengkap.'<br>
								'.$email.'
							</td>
						</tr>
					</table>
				</td>
				<td width="4%">&nbsp;</td>
				<td width="50%" valign="top">
					<table>
						<tr>
							<td width="27%">'._QUOTATIONDATE.'</td>
							<td width="3%">:</td>
							<td width="70%">'.tglformat($tanggal_buka_transaksi).'</td>
						</tr>
						<tr>
							<td>'._QUOTATIONNUMBER.'</td>
							<td>:</td>
							<td>'.$txid.'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						'.$temp_pic.'
						<!--<tr>
							<td>'._EMAIL.'</td>
							<td>:</td>
							<td>'.$email.'</td>
						</tr>-->
					</table>
				</td>
			</tr>
		</table>
		</div>
		';
		
		$content .= '
		<p style="line-height:0.6;">&nbsp;</p>
		<div class="table_block table-responsive table-small-gap">
		<table border="0" class="table table-bordered" width="100%" cellpadding="7">
			<tr style="background-color:#f5f5f5;border-bottom: 1px solid #aaa;font-weight:bold">
				<th style="text-align:center" width="8%" style="border-bottom: 1px solid #aaa;">'._ITEM.'</th>
				<th style="text-align:center" width="35%" style="border-bottom: 1px solid #aaa;">'._PARTNUMBER.'</th>
				<th style="text-align:center" width="20%" style="border-bottom: 1px solid #aaa;">'._UNITPRICE.'</th>
				<th style="text-align:center" width="10%" style="text-align:center;border-bottom: 1px solid #aaa;">'._DISCOUNT.'</th>
				<!--<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._QTY.'</th>-->
				<th style="text-align:center" width="7%" style="border-bottom: 1px solid #aaa;">Qty</th>
				<th style="text-align:right" width="20%" style="border-bottom: 1px solid #aaa;">'._TOTAIDR.'</th>
			</tr>
		';
		$item = 0;
		$total = 0;
		
		// $sql = "SELECT quotation_detail_id,jumlah,td.harga,product_id,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, is_request_disc FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id <> $cat_loose_item ORDER BY quotation_detail_id";
		
		if ($is_from_template == 1) {
			$sql = "SELECT quotation_detail_id, jumlah,td.harga,product_name,td.filename, '' as merek, td.diskon,  '' as keterangan, '' as sku, td.harganormal-(if(LOCATE('%', td.diskon)>0, (TRIM(REPLACE(td.diskon, '%', ''))/100*td.harganormal),td.diskon)) as hargadiskon, td.harganormal, disc_per_item, is_request_disc, parent FROM sc_quotation_detail td WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
		} else {
			$sql = "SELECT quotation_detail_id, jumlah,td.harga,td.product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, is_request_disc, parent FROM sc_quotation_detail td LEFT JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' ORDER BY quotation_detail_id";
		}
		$no_parent = 1;
		$result = $mysqli->query($sql);
		while (list($quotation_detail_id, $quantity, $price, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $is_request_disc, $parent) = $result->fetch_row()) {
			
			$brand_name = get_brand_name($idmerek);
			$parent = $parent == 0 ? $no_parent++ : "";
			// $line_cost = $quantity * $price;  //work out the line cost
			// $total = $total + $line_cost;   //add to the total cost
			
			// 05/03/2020 14:57
			if ($is_from_template == 1) {
				if ($discount != '') {
					$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
					$line_cost = ($quantity * $price) - (($quantity * $price) * (int)$discount / 100);
					$total += $line_cost;
				} else {
					$line_cost = $quantity * $price;  //work out the line cost
					$total += $line_cost;
				}
			} else {
				$discount = ($disc_per_item != '') ? $disc_per_item : $discount;
				$line_cost = $quantity * $price;  //work out the line cost
				$disc = $price * (int)$discount / 100;
				$harga_setelah_disc = $disc * $quantity;
				
				// $price_dics = $line_cost - $harga_setelah_disc;
				if ($disc_per_item != '') {
					$price_dics = $line_cost - $harga_setelah_disc;
					$line_cost = $price_dics;
				} /* else {
					// $price_dics = 0;
				} */
				$total += $price_dics;
			}
			
			$suffix_disc = (substr_count($discount, '%') == 1) ? "$discount" : "$discount%";
			if ($discount != '') $discount = $suffix_disc;
		
			$sql_plafon = "SELECT keterangan, product_id FROM sc_quotation_plafon WHERE quotation_id='$quotation_id' AND product_id='$product_id'";
			$result_plafon = $mysqli->query($sql_plafon);
			list($plafon_keterangan, $plafon_product_id) = $result_plafon->fetch_row();
			
			$keterangan = ($plafon_keterangan != '') ? "<br><small>Ket: $plafon_keterangan</small>" : "";
			
			// kondisi lama
			// if ($is_request_disc == 2 || $is_request_disc == 3) {
			if ($is_request_disc == 3) {
				$datatambahan = <<<HTML
				  <div class="form-group">
					<div class="input-group">
					  <input type="text" class="text-center discount_input" value="$discount" name="disc_per_item" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
					  <div class="input-group-addon">%</div>
					</div>
				  </div>
HTML;
				$form_required_approval = <<<HTML
				<div id="discount-per-item">
					<form method="POST" action="#" class="form-inline">
						<input type="hidden" name="quotation_detail_id" value="$quotation_detail_id">
						<input type="hidden" name="quotation_id" value="$quotation_id">
						<input type="hidden" name="max_plafon" value="$member_max_discount">
						<!--<input type="text" class="text-center discount_input" value="$discount" name="disc_per_item">-->
						$datatambahan
						<button type="submit" name="setuju" class="btn btn-default more button_discount_input">Setuju</button>
						<!--<button type="submit" name="ajukan" id="ajukan" style="display:none" class="btn btn-default more button_discount_input">Ajukan</button>-->
					</form>
				</div>
HTML;
			} else {
				$signdisc = ($discount > 0) ? '' : '';
				$form_required_approval = <<<HTML
					$discount$signdisc
HTML;
			}
			
			$item++;
			// $price = ($price > 0) ? $price : '';
			// (negative_check($line_cost)) ? number_format($line_cost, 0, ',', '.') : ""
			$product_price = ($price > 0) ? number_format($price, 0, ',', '.') : (negative_check($price)) ? number_format($price, 0, ',', '.') : "";
			// $line_cost = ($line_cost > 0) ? $line_cost : "";
			$line_cost = ($line_cost > 0) ? $line_cost : (negative_check($line_cost)) ? number_format($line_cost, 0, ',', '.') : "";
			$is_product_code = ($product_code != '') ? '<p class="product-name"><strong>'.$product_code.'</strong></p>' : '';
			
			if ($is_from_template == 1) {
				$no = $parent;
			} else {
				$no = $item;
			}
			$red_class = negative_check($price) ? ';color:red' : '';

			$content .= '
			<tr>
				<td style="text-align:center">'.$no.'</td>
				<td>
					'.$is_product_code.'
					<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
				</td>
				<td style="text-align:right'.$red_class.'">'.$product_price.'</td>
				<!--<td style="text-align:center">'.$discount.'</td>-->
				<td style="text-align:center">'.$form_required_approval.'</td>
				<td style="text-align:center">'.$quantity.'</td>
				<td style="text-align:right'.$red_class.'">'.$line_cost.'</td>
			</tr>
			';
			
			//=========================================
			
			// $brand_name = get_brand_name($idmerek);
			// // $line_cost = $quantity * $price;  //work out the line cost
			// // $total = $total + $line_cost;   //add to the total cost
			
			// if ($discount != '' && $disc_per_item == '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $discount = $disc_per_item;
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;
				
			// } else if ($discount != '' && $disc_per_item != '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $discount = $disc_per_item;
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;
				
			// } else if ($discount == '' && $disc_per_item != '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = $disc_per_item;
				
				// $discount = $disc_per_item;
				// $nominal = str_replace('%', '', $disc_per_item);
				// $price_dics = $pricenormal - ($pricenormal*$nominal/100);
				
				// $disc = 100 - $nominal;
				// $temp = (($disc/100) * $line_cost);
				// $line_cost = $temp;
				
			// } else if ($discount == '') {
				// $line_cost = $pricenormal * $quantity;
				// $discount = '';
			// } else {
				// $line_cost = $price * $quantity;  //work out the line cost
			// }
			// $total = $total + $line_cost;   //add to the total cost
			// // $total = (int)$total;
		
			// $sql_plafon = "SELECT keterangan, product_id FROM sc_quotation_plafon WHERE quotation_id='$quotation_id' AND product_id='$product_id'";
			// $result_plafon = $mysqli->query($sql_plafon);
			// list($plafon_keterangan, $plafon_product_id) = $result_plafon->fetch_row();
			
			// $keterangan = ($plafon_keterangan != '') ? "<br><small>Ket: $plafon_keterangan</small>" : "";
			// // if ($is_request_disc == 2) {
			// if ($is_request_disc == 2 || $is_request_disc == 3) {
				// $datatambahan = <<<HTML
				  // <div class="form-group">
					// <div class="input-group">
					  // <input type="text" class="text-center discount_input" value="$discount" name="disc_per_item" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
					  // <div class="input-group-addon">%</div>
					// </div>
				  // </div>
// HTML;

				// $form_required_approval = <<<HTML
				// <div id="discount-per-item">
					// <form method="POST" action="#" class="form-inline">
						// <input type="hidden" name="quotation_detail_id" value="$quotation_detail_id">
						// <input type="hidden" name="quotation_id" value="$quotation_id">
						// <input type="hidden" name="max_plafon" value="$member_max_discount">
						// <!--<input type="text" class="text-center discount_input" value="$discount" name="disc_per_item">-->
						// $datatambahan
						// <button type="submit" name="setuju" class="btn btn-default more button_discount_input">Setuju</button>
						// <!--<button type="submit" name="ajukan" id="ajukan" style="display:none" class="btn btn-default more button_discount_input">Ajukan</button>-->
					// </form>
					// </div>
// HTML;
			// } else {
				// $signdisc = ($discount > 0) ? '%' : '';
				// $form_required_approval = <<<HTML
					// $discount$signdisc
// HTML;
			// }
			// $item++;
			// $content .= '
			// <tr>
				// <td style="text-align:center">'.$item.'</td>
				// <td>
					// <p class="product-name"><strong>'.$product_code.'</strong></p>
					// <span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
					// '.$keterangan.'
				// </td>
				// <td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
				// <!--<td style="text-align:center">'.$discount.'</td>-->
				// <td style="text-align:center">'.$form_required_approval.'</td>
				// <td style="text-align:center">'.$quantity.'</td>
				// <td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
			// </tr>
			// ';
		}
		
		$sql = "SELECT quotation_detail_id,jumlah,td.harga,product_id,product_name,td.filename, c.idmerek, td.diskon, c.keterangan, c.sku, c.harganormal-(if(LOCATE('%', c.diskon)>0, (TRIM(REPLACE(c.diskon, '%', ''))/100*c.harganormal),c.diskon)) as hargadiskon, c.harganormal, disc_per_item, is_request_disc FROM sc_quotation_detail td INNER JOIN catalogdata c ON c.id=td.product_id  WHERE quotation_id='$txid' AND c.cat_id = $cat_loose_item ORDER BY quotation_detail_id";
		$result = $mysqli->query($sql);
		if ($result->num_rows > 0) {
			while (list($quotation_detail_id, $quantity, $price, $product_id, $productname,$filename, $idmerek, $discount, $keterangan, $product_code, $price_dics, $pricenormal, $disc_per_item, $is_request_disc) = $result->fetch_row()) {
				$brand_name = get_brand_name($idmerek);
				// $line_cost = $quantity * $price;  //work out the line cost
				// $total = $total + $line_cost;   //add to the total cost
				
				if ($discount != '' && $disc_per_item == '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$discount = $disc_per_item;
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount != '' && $disc_per_item != '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$discount = $disc_per_item;
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount == '' && $disc_per_item != '') {
					$line_cost = $pricenormal * $quantity;
					$discount = $disc_per_item;
					
					$discount = $disc_per_item;
					$nominal = str_replace('%', '', $disc_per_item);
					$price_dics = $pricenormal - ($pricenormal*$nominal/100);
					
					$disc = 100 - $nominal;
					$temp = (($disc/100) * $line_cost);
					$line_cost = $temp;
					
				} else if ($discount == '') {
					$line_cost = $pricenormal * $quantity;
					$discount = '';
				} else {
					$line_cost = $price * $quantity;  //work out the line cost
				}
				$total = $total + $line_cost;   //add to the total cost
				// $total = (int)$total;
			
				$sql_plafon = "SELECT keterangan, product_id FROM sc_quotation_plafon WHERE quotation_id='$quotation_id' AND product_id='$product_id'";
				$result_plafon = $mysqli->query($sql_plafon);
				list($plafon_keterangan, $plafon_product_id) = $result_plafon->fetch_row();
				
				$keterangan = ($plafon_keterangan != '') ? "<br><small>Ket: $plafon_keterangan</small>" : "";
				if ($is_request_disc == 2) {
					$datatambahan = <<<HTML
					  <div class="form-group">
						<div class="input-group">
						  <input type="number" max="100" class="text-center discount_input" value="$discount" name="disc_per_item" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
						  <div class="input-group-addon">%</div>
						</div>
					  </div>
HTML;

					$form_required_approval = <<<HTML
						<form method="POST" action="#" class="form-inline">
							<input type="hidden" name="quotation_detail_id" value="$quotation_detail_id">
							<input type="hidden" name="quotation_id" value="$quotation_id">
							<input type="hidden" name="max_plafon" value="$member_max_discount">
							<!--<input type="text" class="text-center discount_input" value="$discount" name="disc_per_item">-->
							$datatambahan
							<button type="submit" name="setuju" class="btn btn-default more button_discount_input">Setuju</button>
							<!--<button type="submit" name="ajukan" id="ajukan" style="display:none" class="btn btn-default more button_discount_input">Ajukan</button>-->
						</form>
HTML;
				} else {
					$signdisc = ($discount > 0) ? '%' : '';
					$form_required_approval = <<<HTML
						$discount$signdisc
HTML;
				}
				$item++;
				$content .= '
				<tr>
					<td style="text-align:center">'.$item.'</td>
					<td>
						<p class="product-name"><strong>'.$product_code.'</strong></p>
						<span style="font-size:9pt" class="description-product"><i>'.$productname.'</i></span>
						'.$keterangan.'
					</td>
					<td style="text-align:right">'.number_format($pricenormal, 0, ',', '.').'</td>
					<!--<td style="text-align:center">'.$discount.'</td>-->
					<td style="text-align:center">'.$form_required_approval.'</td>
					<td style="text-align:center">'.$quantity.'</td>
					<td style="text-align:right">'.number_format($line_cost, 0, ',', '.').'</td>
				</tr>
				';
			}
		}
		
		$discount = ($total*10)/100;
		$total_price = ($discount > 0) ? $total+round($discount) : $total;
		$total_price = round($total_price);
		
		$termofpayment = ($customTermOfPayment != '') ? $customTermOfPayment : '';
		$deliverytime = ($customDeliveryTime != '') ? $customDeliveryTime : '';
		$deliveryfranco = ($deliveryfranco != '') ? $deliveryfranco : '';
		$warranty = ($warranty != '') ? $warranty : '';
		$quotationvalidity = ($quotationvalidity != '') ? $quotationvalidity : '';
		// $quotationvalidity = ($customQuotationValidity != '') ? $customQuotationValidity : '';	
		$termandcondition = ($customQuotationTC != '') ? $customQuotationTC : '';
		
		$content .= '
		<tr>
			<td style="border-top: 1px solid #aaa;font-weight: bold;" colspan="5" align="right">'._TOTALLISTPRICE.'</td>
			<td align="right" style="border-top: 1px solid #aaa;">'.number_format($total, 0, ',', '.').'</td>
		</tr>
		';
		
		if ($order_discount != '') {
			
			if (substr_count($order_discount, '%') == 1) {
				$voucher = $order_discount;
				$disc = $total*$voucher/100;
				$total = $total - $disc;
				$subtotal = $total;
				$discount_type = _DISCOUNTPERCENT;
			} else {
				$voucher = number_format($order_discount, 0, ',', '.');
				$total = $total - $order_discount;
				$subtotal = $total;
				$discount_type = _DISCOUNTIDR;
			}
			
			$discount = ($subtotal*10)/100;
			$total_price = ($discount > 0) ? $subtotal+$discount : $subtotal;
			$total_price = round($total_price);
			
			$content .= '
			<tr>
				<td colspan="5" align="right" style="font-weight: bold;">'.$discount_type.'</td>
				<td align="right">('.((substr_count($order_discount, '%') == 1) ? $order_discount : number_format($order_discount, 0, ',', '.')).')</td>
			</tr>
			';
			
			$content .= '
			<tr>
				<td colspan="5" align="right" style="font-weight: bold;">'._SUBTOTAL.'</td>
				<td align="right">'.number_format($subtotal, 0, ',', '.').'</td>
			</tr>
			';
		}
		$terbilang = convertNumber($total_price);	//bilangan($total_price);
		
		$content .= '
		<tr>
			<td colspan="5" align="right" style="font-weight: bold;">'._VATIDR.'</td>
			<td align="right">'.number_format($discount, 0, ',', '.').'</td>
		</tr>
		<tr>
			<td colspan="5" align="right" style="font-weight: bold;">'._TOTAIDR.'</td>
			<td align="right">'.number_format($total_price, 0, ',', '.').'</td>
		</tr>
		<tr style="background-color:#f5f5f5;">
			<td colspan="6" style="border-top:1px solid #aaa; font-weight:bold;solid #aaa">'._TERBILANG.': '.$terbilang.'</td>
		</tr>
		</table>
		</div>
		';
		
		// Level2 jadikan 2 | baypass
		$sql = "UPDATE sc_quotation SET level2='2' WHERE quotation_id='$quotation_id'";
		$mysqli->query($sql);
			
		// $sql = "SELECT * FROM sc_quotation_detail WHERE quotation_id='$quotation_id'";
		$sql = "SELECT d.disc_per_item, d.is_request_disc
				FROM sc_quotation q 
				INNER JOIN sc_quotation_detail d ON d.quotation_id=q.quotation_id
				WHERE q.quotation_id='$quotation_id' AND q.level1=1 AND q.level2=2";
		$result = $mysqli->query($sql);
		// $col_request = 0;
		$process_is_done = 0;
		// $process_to_level1 = 0;
		$x = 0;
		while($row = $result->fetch_assoc()) {
			// echo "{$row['disc_per_item']} > $member_max_discount<br>";
			// if ((int)$row['disc_per_item'] > $member_max_discount && $row['is_request_disc'] == 2) {
				// $col_request++;
				// $quotation_is_process = false;
			// }
			
			// if ((int)$row['disc_per_item'] > $member_max_discount && $row['is_request_disc'] == 2) {
				// $process_to_level1++;
			// }
			// if (/* $row['is_request_disc'] == 0 ||  */$row['is_request_disc'] == 2) {
				// $process_is_done++;
			// }
			
			/* if ($row['is_request_disc'] == 4) {
				$process_is_done++;
			} */
			
			$x = $row['is_request_disc'];
			// if ($row['is_request_disc'] == 2 || $row['is_request_disc'] == 3) {
			// if ($row['is_request_disc'] == 2 || $row['is_request_disc'] == 3 || $row['is_request_disc'] == 4) {
			if ($row['is_request_disc'] == 3 || $row['is_request_disc'] == 4) {
				$process_is_done++;
			}
			
		}
		
		// echo $x;
		// $countQuotationDetail = countQuotationDetail($quotation_id, 2);
		$countQuotationDetail = countQuotationDetail($quotation_id, 3);
		
		// echo "process_is_done: $process_is_done | countQuotationDetail: $countQuotationDetail<br>";
		$content .= '<div class="text-center">';
		// if ($process_to_level1 > 0) {
			// $content .= '<div class="alert alert-danger">';
			// $content .= '<p>'._REQUESTTOLEVEL1.'</p>';
			// $content .= '</div>';
			// $content .= '<form method="POST" action="#">';
			// $content .= '<input type="hidden" name="quotation_id" value="'.$quotation_id.'">';
			// $content .= '<input type="submit" class="btn btn-primary more" name="request" value="Request">';
			// $content .= '</form>';
		// }

		if ($process_is_done == $countQuotationDetail) {
			
			$content .= '<form method="POST" action="#">';
			$content .= '<input type="hidden" name="quotation_id" value="'.$quotation_id.'">';
			$content .= '<input type="submit" class="btn btn-primary more" name="selesai" value="Selesai">';
			$content .= '</form>';
		}
		$content .= '</div>';
		
		// $content .= '<div class="text-center">';
		// if ($col_request > 0) {
			// $content .= '<div class="alert alert-danger">';
			// $content .= '<p>Terdapat data diatas plafon Anda. Silakan ajukan request ke atasan Anda.</p>';
			// $content .= '</div>';
			// $content .= '<form method="POST" action="#">';
			// $content .= '<input type="hidden" name="quotation_id" value="'.$quotation_id.'">';
			// $content .= '<input type="submit" class="btn btn-primary" name="request" value="Request">';
			// $content .= '</form>';
		// } else {
			// $content .= '<form method="POST" action="#">';
			// $content .= '<input type="hidden" name="quotation_id" value="'.$quotation_id.'">';
			// $content .= '<input type="submit" class="btn btn-primary" name="selesai" value="Selesai">';
			// $content .= '</form>';
		// }
		// $content .= '</div>';
		
		// if ($catatan != '') {
			// $content .= '
			// <p>
				// <u><b>'._NOTE.'</b></u><br/>
				// '.$catatan.'
			// </p>
			// ';
		// } else {
			// $content .= '<p>'.$additional_note.'<br/></p>';
		// }
		
		$content .= '<p><u><b>'._NOTE.'</b></u><br/>';
		if ($catatan != '') {
			
			$content .= $catatan;
		} else {
			$content .= $additional_note.'<br/>';
		}
		$content .= '</p>';
		
		$berca_estore = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? '<img src="'.$cfg_app_url.'/images/berca-estore.jpeg" style="height:23px" >' : '<img src="'.$cfg_app_url.'/images/berca-estore-bnm.png" style="height:32px" >';
		
		
		if ($is_from_template == 1) {
			$content .= '
			<p>
				<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
				<table>
					<tr>
						<td style="width: 150px;">Delivery Time</td>
						<td style="width: 550px;">: '.$deliverytime.'</td>
					</tr>
					<tr>
						<td>Term of Payment</td>
						<td>: '.$termofpayment.'</td>
					</tr>
					<tr>
						<td>Quotation Validity</td>
						<td>: '.$quotationvalidity.'</td>
					</tr>
					<tr>
						<td>Delivery Franco</td>
						<td>: '.$tc_delivery_franco.'</td>
					</tr>
					<tr>
						<td>Warranty</td>
						<td>: '.$tc_warranty.'</td>
					</tr>
				</table>
			</p>';
		} else {
			$content .= '
			<p>
				<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
				'.$termandcondition.'
			</p>';
		}
		
		$content .= '
		<!--<p>
			<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
			'._TERMOFPAYMENT.': '.$termofpayment.'<br/>
			'._DELIVERYTIME.': '.$deliverytime.'<br/>
			'._DELIVERYFRANCO.': '.$deliveryfranco.'<br/>
			'.(($warranty != '') ? _WARRANTY.': '.$warranty.'<br/>' : '').'
			'._QUOTATIONVALIDITY.': '.$quotationvalidity.'
		</p>-->
		<!--<p>
			<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
			'.$termandcondition.'
		</p>-->';
		$content .= '
		<p>'._SINCERELYYOURS.',</p>
		<p>'.$berca_estore.'</p>
		<div class="automatically-generate" style="background-color:#eee;margin-bottom:30px;margin-top:30px;text-align:center;line-height:35px">'.$quotationvalidity.'</div>
		';
		
		$_pid = base64_encode($txid);
		// Hanya jika level 3
		// $content .= '<div class="downloadpdf"><a class="btn btn-default more downloadpdf-button" href="'.$urlfunc->makePretty("?p=order&action=downloadpdf&pid=$_pid").'" target="_blank"><img src="'.$cfg_app_url.'/images/pdf-icon.svg">'._DOWNLOADPDF.'</a></div>';
	} else {
		$content .= _NORIGHT;
	}
}

// }

if ($action == 'downloadquotation') {
	
	$user_id = $_SESSION['member_uid'];
	
	$where_condition = ($status_quo == '') ? "" : "AND status_quotation='$status_quo'";

	$sql = "SELECT q.quotation_id, q.tanggal_buka_transaksi, q.deliverytime, q.termofpayment, q.discount, c.nama_lengkap as customer, 
			c.company as customer_company, m.fullname as sales, m.user_email as sales_email, status_quotation, customer, nama_pic, email_pic, nomor_po, nomor_job_order, detail_produk, nilai_transaksi
			FROM sc_quotation q 
			INNER JOIN sc_customer c ON c.customer_id=q.customer_id
			INNER JOIN webmember m ON m.user_id=c.web_user_id
			WHERE m.type_company='bi' $where_condition AND m.user_id='$user_id'
			ORDER BY q.autoid DESC";
			
	/** Error reporting */
	ob_get_clean();
	
	/** Include PHPExcel */
	//require_once "$cfg_app_path/aplikasi/phpexcel/Classes/PHPExcel.php";
	require_once dirname(__FILE__) . '/../../aplikasi/phpexcel/Classes/PHPExcel.php';
	
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);
	// Initialise the Excel row number
	$rowCount = 1; 
	
	if ($result = $mysqli->query($sql)) {
		$total_records = $result->num_rows;
		if ($total_records == 0) {
			$action = createmessage(_NOQUOTATION, _INFO, "info", "");
		} else {
			
			$subtotal = 0;
			
			$temp_columns = array(
				_CURRENTORDERTITLE,
				_ORDERDATE,
				_NOMINAL,
				_CUSTOMER,
				_SALES,
				_STATUSQUOTATION,
				_CUSTOMER,
				_NAMAPIC,
				_EMAILPIC,
				_NOMORPO,
				_NOMORJOBORDER,
				_DETAILPRODUK,
				_NOMINALTRANSAKSI
			);
			
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $temp_columns[0]);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $temp_columns[1]);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $temp_columns[2]);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $temp_columns[3]);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $temp_columns[4]);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $temp_columns[5]);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $temp_columns[6]);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $temp_columns[7]);
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $temp_columns[8]);
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $temp_columns[9]);
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $temp_columns[10]);
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $temp_columns[11]);
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $temp_columns[12]);
			$rowCount++;
			
			while($row = $result->fetch_assoc()) {
				// $no_urut++;
				$companyName			= $row['customer_company'] != '' ? "({$row['customer_company']})" : '';
				$tanggal_buka_transaksi = $row['tanggal_buka_transaksi'];
				$order_code 			= $row['quotation_id'];
				$customer 				= $row['customer'] . $companyName;
				$sales		 			= $row['sales'];		
				$order_discount			= $row['discount'];
				$subtotal 				= number_format(hitungTotalQuotation($order_code), 0, ',', '.');
				$status_quotation		= $row['status_quotation'];
				$customer_pic			= $row['customer'];
				$nama_pic				= $row['nama_pic'];
				$email_pic				= $row['email_pic'];
				$nomor_po				= $row['nomor_po'];
				$nomor_job_order		= $row['nomor_job_order'];
				$detail_produk			= $row['detail_produk'];
				$nilai_transaksi		= number_format($row['nilai_transaksi'], 0, ',', '.');
				
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $order_code);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT );
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, tglformat($tanggal_buka_transaksi));
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $subtotal);
				if ($subtotal > 0) {
					$objPHPExcel->getActiveSheet()->getStyle('C'.$rowCount)->getNumberFormat()->setFormatCode('#.##0');
				}
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $customer);
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $sales);
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $quotation_status_arr[$status_quotation]);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $customer_pic);
				$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $nama_pic);
				$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $email_pic);
				$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $nomor_po);
				$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $nomor_job_order);
				$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $detail_produk);
				$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $nilai_transaksi);
				$objPHPExcel->getActiveSheet()->getStyle('M'.$rowCount)->getNumberFormat()->setFormatCode('#.##0');
				
				// Increment the Excel row counter
				// $objPHPExcel->getActiveSheet()->getColumnDimension('A'.$rowCount)->setAutoSize(true);
				$nCols = 11; //set the number of columns

				foreach (range(0, $nCols) as $col) {
					$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
				}
				
				$rowCount++; 
			}
			
			if ($total_record > 1) {
				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('H'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('K'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				$objPHPExcel->getActiveSheet()->getStyle('M'.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			}
			
			$objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
			
			$ext = ".xls";
			$filename = "Quotation_BI_$quotation_status_arr[$status_quo]".date('Y-m-d H:i:s').$ext;
			
		}
	}
	
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$filename.'"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// // If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
}

	
if ($action == 'quotation_template') {
	if (isset($_SESSION['member_sekretaris']) && $_SESSION['member_sekretaris'] == 1) {
		$title = _CREATEQUOTATIONFROMTEMPLATE;
		
		$content .= '
		<form role="form" method="POST" action="'.$urlfunc->makePretty("?p=order&action=viewquotationtemplate").'" enctype="multipart/form-data">
			<div class="form-group">
				<label for="filename">Import file excel/csv</label>
				<input type="file" name="filename" id="filename" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
				<!--<p class="help-block">Example block-level help text here.</p>-->
			</div>
			<button type="submit" class="btn btn-default btn-primary">Submit</button>
		</form>
		';
	} else {
		$content .= _NORIGHT;
	}
}

if ($action == 'viewquotationtemplate') {

	if (isset($_SESSION['member_sekretaris']) && $_SESSION['member_sekretaris'] == 1) {
		$title = "e-Quotation";

		if ($_FILES['filename']['name'] != '') {
			
			$hasilupload = fiestoupload('filename', $cfg_temp_path, '', $maxfilesize, $allowedtypes = "xlsx,xls,csv");
			if ($hasilupload != _SUCCESS) {
				
				$content .= '<p>'.$hasilupload.'</p>';
				$content .= '<a href="javascript:history.go(-1)">'._BACK.'</a>';
			} else {
				
				$pathinfo = pathinfo($_FILES['filename']['name']);
		
				$extension = $pathinfo['extension'];
				$basename = $pathinfo['basename'];
				$path = "$cfg_temp_path/$basename";
				
				include "aplikasi/phpexcel/Classes/PHPExcel.php";
				
				$objPHPExcel = PHPExcel_IOFactory::load($path);
				$dataArr = array();
				
				foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
					$worksheetTitle     = $worksheet->getTitle();
					$highestRow         = $worksheet->getHighestRow(); // e.g. 10
					$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
					$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
					 
					if ($worksheetTitle == 'eQUO') {
						for ($row = 1; $row <= $highestRow; ++ $row) {
							for ($col = 0; $col < $highestColumnIndex; ++ $col) {
								$cell = $worksheet->getCellByColumnAndRow($col, $row);
								// $val = $cell->getValue();
								$val = $cell->getCalculatedValue();
								$dataArr[$row][$col] = $val;
							}
						}
					}
				}
				
				// echo '<pre>';
				// print_r($dataArr);
				if (count($dataArr) > 0) {
					//AMBIL INFO
					// $data['perusahaan_nama']=$dataArr[3][3];	
					// $data['perusahaan_alamat']=$dataArr[4][3];	
					// $data['perusahaan_kodepos']=$dataArr[5][3];	
					// $data['perusahaan_kota']=$dataArr[6][3];	
					// $data['perusahaan_telp']=$dataArr[7][3];	
					// $data['perusahaan_jabatan']=$dataArr[8][3];	
					// $data['perusahaan_email']=$dataArr[9][3];	
					// $data['tanggal']=\PHPExcel_Style_NumberFormat::toFormattedString($dataArr[3][10], 'YYYY-MM-DD H:i:s');
					// $data['sales_nama']=$dataArr[7][10];
					// $data['sales_email']=$dataArr[8][10];
					// $data['sales_telp']=$dataArr[9][10];
					
					$temp_data = explode(',', $dataArr[6][3]);
					
					$data['perusahaan_nama']=$dataArr[3][3];	
					$data['perusahaan_alamat']=$dataArr[4][3];	
					$data['perusahaan_kelurahan']=$dataArr[5][3];	
					$data['perusahaan_kota']=$temp_data[0];	
					$data['perusahaan_kodepos']=$temp_data[1];	
					$data['perusahaan_telp']=$dataArr[7][3];	
					$data['perusahaan_jabatan']=$dataArr[8][3];	
					$data['perusahaan_email']=$dataArr[9][3];	
					$data['tanggal']=\PHPExcel_Style_NumberFormat::toFormattedString($dataArr[3][10], 'YYYY-MM-DD H:i:s');
					$data['sales_nama']=$dataArr[7][10];
					$data['sales_email']=$dataArr[8][10];
					$data['sales_telp']=$dataArr[9][10];
					
					if ($data['perusahaan_email'] == '') {
						$content .= _EMAILCUSTOMERREQUIRED;
						$content .= '<p><a href="javascript:history.go(-1)">'._BACK.'</a></p>';
					} else {
						//AMBIL DATA PRODUK
						$item_saat_ini=0;
						$produk=array();
						foreach($dataArr as $row => $col ){
							// echo $col[1].', type: '.gettype($col[1]).'<br>';
							//ambil data diatas baris ke 12
							if($row<=11) {
								continue;
							}
							//loop berhenti ketika ketemu teks Total
							if(strpos($col[11],'Total')!==false){
								break;
							}
							//jika Teks total  diatas tidak ketemu maka loop berhenti ketika ketemu teks Said
							if($col[1]==='Said'){
								break;
							}
							
							//cek produk yang ada kuantitinya. jika tidak ada kuantitinya berarti masuk keterangan produk diatasnya
							
							if(intval($col[1])>0){
								$item_saat_ini=$col[1];
								$info_produk=array("item"=>$col[1],"nama"=>$col[2],"price"=>$col[6],"disc"=>$col[10],"qty"=>$col['11']);
								$data['products'][]=$info_produk;
							} else {
								if($col[2]!='') {
									$sub_info_produk=array("item"=>'-',"nama"=>$col[2],"price"=>$col[6],"disc"=>$col[10],"qty"=>$col['11']);
									$data['sub_products'][$item_saat_ini][]=$sub_info_produk;
								}
							}
						}
						
						//END AMBIL DATA NOTES
						$notes = array();
						foreach($dataArr as $row => $col ){
							$mulai = false;
							$cari = 0;
							foreach($dataArr as $row => $col ){

								if($col[1]==='Notes') {
									$mulai=true;
								}
								if($mulai) {
									$cari++;
									//MULAI PENCATATAN TERM JIKA SUDAH MENEMUKAN TEKS 'Terms & Conditions 
									if($col[1]!='' && $cari > 1 && $cari < 5){
										$notes[$cari] = $col[1];
									}
								}
								
								//CEK APAKAH SUDAH SAMPAI AKHIR, MAKA KELUAR
								if($col[1] == 'Terms & Conditions'){
									$mulai=false;
								}
							}
						}
						$data['notes'] = $notes;
						
						//AMBIL TERM AND CONDITION
						$start=false;
						$termncondition=array();
						foreach($dataArr as $row => $col ){
							
							
							if($col[1]=='Terms & Conditions') {
								$start=true;
							}
							if($start) {
								
								//MULAI PENCATATAN TERM JIKA SUDAH MENEMUKAN TEKS 'Terms & Conditions 
								if($col[1]!='' && $col[5]){
									$termncondition[$col[1]]=$col[5];
								}
							}
							
							
							//CEK APAKAH SUDAH SAMPAI AKHIR, MAKA KELUAR
							if($col[1]=='Sincerely yours,'){
								$start=false;
								//break;
							}
						}
						$data['termncondition']=$termncondition;
						$_SESSION['data_from_template'] = $data;

						//END AMBIL TERM AND CONDITION

						$content .= showQuotationFromTemplate($data);
					}
				} else {
					$content .= _WORKSHEETNOTVALID;
					$content .= '<p><a href="javascript:history.go(-1)">'._BACK.'</a></p>';
				}
			}
		}
	} else {
		$content .= _NORIGHT;
	}
}

if ($action == 'formrequestplafon') {
	$content .= '<div class="form-request-block">';
	$content .= '<h2>'._FORMREQUESTPLAFON.'</h2>';
	
	$content .= '<form id="formrequestplafon" method="POST" action="'.$urlfunc->makePretty("?p=order&action=dorequestplafon").'">';
	// $content .= '<input type="hidden" name="pid" value="'.$_POST[''].'"';
	$content .= '<div class="row">';
	
	if (count($_SESSION['temp_arr']) > 0) {
		foreach($_SESSION['temp_arr'] as $val) {
			$content .= '<div class="col-md-12">';
			$content .= '
				<div class="form-group">

					<label for="plafon">'._DISCPLAFON.' '.$val['nama'].'</label>
					<div class="form-inline">
						<div class="input-group">
							<input type="hidden" name="pid['.$val['item'].']" id="pid" value="'.$val['item'].'">
							<input type="text" class="form-control" name="plafon['.$val['item'].']" id="plafon" value="'.$val['disc'].'" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||  event.charCode == 44 || event.charCode == 0 || event.charCode == 46">
							<div class="input-group-addon">%</div>
						</div>
					</div>
				</div>
			';
			$content .= '</div>';
			
			$content .= '<div class="col-md-12">';
			$content .= '<div class="form-group">';
			$content .= '<label for="description">'._DESCPLAFON.'</label>';
			$content .= '<textarea class="form-control" name="description['.$val['item'].']" id="description"></textarea>';
			$content .= '</div>';
			$content .= '</div>';
		}
	}
	
	$content .= '<div class="col-md-12">';
	// $content .= '<input type="hidden" name="member_parent" id="member_parent" value="'.$_SESSION['member_parent'].'">';
	$content .= '<input type="hidden" name="member_parent" id="member_parent" value="'.$get_member_parent['parent'].'">';
	$content .= '<button type="submit" class="btn btn-primary">Submit</button>';
	// $content .= '<button type="submit" id="code-directur" class="btn btn-primary">Instant Access</button>';
	// $content .= '<input type="hidden" name="req_big_bos" id="req_big_bos">';
	$content .= '</div>';
	$content .= '</form>';
	$content .= '</div>';
	$content .= '</div>';
}

if ($action == 'dorequestplafon') {
	$product_id = $_POST['pid'];
	$plafon = $_POST['plafon'];
	$description = $_POST['description'];
	$req_big_bos = fiestolaundry($_POST['req_big_bos'], 11);
	
	// $data = array(
		// 'id' => $product_id,
		// 'value' => $plafon,
		// 'description' => $description
	// );
	
	if (count($product_id) > 0) {
		
		$plafon_request = array();
		foreach($product_id as $id) {
			$plafon_request[] = array(
				'id' => $id,
				'value' => $plafon[$id],
				'description' => $description[$id]
			);
		}
		
		$_SESSION['plafon_request'] = $plafon_request;
	}
	
	// print_r($product_id);
	// print_r($_SESSION['plafon_request']);
	// die();
	// $_SESSION['data_from_template'] = $data;

	//END AMBIL TERM AND CONDITION
	$data_from_template = $_SESSION['data_from_template'];
	extract($data_from_template);

	if (count($products) > 0) {
		
		$product_detail = array();
		foreach($products as $idx => $product) {
			$item++;
			$product_detail[$idx][] = $product;
			
			if (is_array($sub_products[$item])) {
				foreach($sub_products[$item] as $values) {
					$product_detail[$item][] = $values;
				}
			}
		}
		
		if (count($product_detail) > 0) {
			
			$temp_arr = array();
			$temp_arr_item = array();
			foreach($product_detail as $idx => $products) {
				
				foreach($products as $j => $product) {
					$noitem = ($product['item'] == '-') ? '' : $product['item'];
					$class = ($product['item'] == '-') ? 'no-item' : '';
					
					if ($product['disc'] > 0) {
						if ($product['item'] == $product_id[$product['item']]) {
							$_SESSION['data_from_template']['products'][$idx]['disc'] = floatval($plafon[$product['item']]."%") / 100;
						}
						
					} else {
						$product_disc = 0;
						$line_cost = $product['price'] * $product['qty'];
					}
					$total = $total + $line_cost;   //add to the total cost
					
				}
			}
		}
		
	}
	
	$data = $_SESSION['data_from_template'];
	unset($_SESSION['temp_arr']);
	// print_r($_SESSION['temp_arr']);
	// print_r($_SESSION['plafon_request']);
	$content .= showQuotationFromTemplate($data);
	// print_r($_SESSION['temp_arr']);
	// $_SESSION['data_from_template'] = $data;
	
	// $url = $urlfunc->makePretty("?p=order&action=viewquotationtemplate");
	// header("Location: $url");
	// exit;
}

if ($action == 'doquotationtemplate') {

	if (isset($_SESSION['temp_arr']) && count($_SESSION['temp_arr']) > 0) {
		$url = $urlfunc->makePretty("?p=order&action=formrequestplafon");
		header("Location: $url");
		exit;
		
	} else {
	
		if (isset($_SESSION['member_sekretaris']) && $_SESSION['member_sekretaris'] == 1) {
			$title = _QUOTATION;
			if (isset($_SESSION['data_from_template'])) {

				$y = date('y');
				$m = date('m');
				$format_ym = "$y";
				$now = date('Y-m-d H:i:s');
				
				$transactionTypeID = 'quotation_id';
				$transactionTypeTable = 'sc_quotation';
				$transactionTypeDetailID = 'quotation_detail_id';
				$transactionTypeTableDetail = 'sc_quotation_detail';
				
				$product_type = $_SESSION['mode']['type'];
				if ($_SESSION['member_company'] == $berca_id) {
					$qShowStatusTranscation = "SELECT SUBSTR($transactionTypeID,8,4)+0 as jml FROM $transactionTypeTable WHERE $transactionTypeID LIKE 'QA/$format_ym%' ORDER BY SUBSTR($transactionTypeID,8,4)+0 DESC LIMIT 1";
				} else {
					$qShowStatusTranscation = "SELECT SUBSTR($transactionTypeID,5,4)+0 as jml FROM $transactionTypeTable WHERE $transactionTypeID LIKE '$format_ym%' ORDER BY SUBSTR($transactionTypeID,5,4)+0 DESC LIMIT 1";
				}
				
				$product_type = 'q';
				$qShowStatusResult = $mysqli->query($qShowStatusTranscation);
				$row_result = $qShowStatusResult->fetch_assoc();
				// $total_transaction = mysql_num_rows($qShowStatusResult)+1;
				$total_transaction = $row_result['jml']+1;
				$transId = NewGuid($total_transaction);
				
				$web_user_id = $_SESSION['member_uid'];
				$fullname = $_SESSION['data_from_template']['perusahaan_jabatan'];
				$email = $_SESSION['data_from_template']['perusahaan_email'];
				$address = $_SESSION['data_from_template']['perusahaan_alamat'];
				$address2 = $_SESSION['data_from_template']['perusahaan_kelurahan'];
				$city = $_SESSION['data_from_template']['perusahaan_kota'];
				// $state = $_SESSION['data_from_template']['sales_nama'];
				$zip = $_SESSION['data_from_template']['perusahaan_kodepos'];
				$phone = str_replace('Phone: ', '', $_SESSION['data_from_template']['perusahaan_telp']);
				// $cellphone1 = $_SESSION['data_from_template']['sales_nama'];
				// $cellphone2 = $_SESSION['data_from_template']['sales_nama'];
				$company = $_SESSION['data_from_template']['perusahaan_nama'];
				$fullnamesales = $_SESSION['data_from_template']['sales_nama'];
				$emailsales = $_SESSION['data_from_template']['sales_email'];
				$phonesales = $_SESSION['data_from_template']['sales_telp'];
				$note = (count($_SESSION['data_from_template']['notes']) > 0) ? join('<br>',$_SESSION['data_from_template']['notes']) : '';
				$products = $_SESSION['data_from_template']['products'];
				$sub_products = $_SESSION['data_from_template']['sub_products'];
				
				$customDeliveryTime = $_SESSION['data_from_template']['termncondition']['Delivery time'];
				$customTermOfPayment = $_SESSION['data_from_template']['termncondition']['Term of Payment'];
				$customQuotationValidity = $_SESSION['data_from_template']['termncondition']['Quotation Validity'];
				$customQuotationTC = $_SESSION['data_from_template']['termncondition']['Delivery time'];
				$customQuotationWarranty = $_SESSION['data_from_template']['termncondition']['Warranty'];
				$customQuotationDeliveryFranco = $_SESSION['data_from_template']['termncondition']['Delivery Franco'];

				// $fullname = $mysqli->real_escape_string ($fullname);
				// $fullnamesales = $mysqli->real_escape_string ($fullnamesales);
				$fullnamesales = $mysqli->real_escape_string($fullnamesales);
				$isPicInternal = "$fullnamesales <br>($emailsales)<br>$phonesales";
				
				$qShowStatusCustomer = "SHOW TABLE STATUS LIKE 'sc_customer'";
				$qShowStatusResult = $mysqli->query($qShowStatusCustomer);
				$row = $qShowStatusResult->fetch_assoc();
				$customerIncrement = $row['Auto_increment'];

				$qShowStatusReceiver = "SHOW TABLE STATUS LIKE 'sc_receiver'";
				$qShowStatusResult = $mysqli->query($qShowStatusReceiver);
				$row = $qShowStatusResult->fetch_assoc();
				$receiverIncrement = $row['Auto_increment'];
				
				$fullname = $mysqli->real_escape_string($fullname);
				$sql = "INSERT INTO sc_customer(nama_lengkap, web_user_id, email, alamat, alamat2, 
						kota, provinsi, zip, telepon, cellphone1, cellphone2, company, npwp, pic) 
						VALUES ('$fullname','$web_user_id','$email','$address','$address2','$city', 
						'$state','$zip','$phone','$phone','$cellphone2', '$company', '$noNPWP', '$isPicInternal')";
				$resultCustomer = $mysqli->query($sql);
				if ($resultCustomer) { //$resultCustomer success
					$sql = "INSERT INTO sc_receiver(nama_lengkap, email, alamat, alamat2, 
						kota, provinsi, zip, telepon, cellphone1, cellphone2, nama_pengirim, telp_pengirim, email_pengirim, company, npwp, pic) 
						VALUES ('$fullname','$email','$address','$address2','$city', 
						'$state','$zip','$phone','$phone',
						'$cellphone2', '$sendername', '$senderphone', '$senderemail', '$company', '$noNPWP', '$isPicInternal')";
					$resultReceiver = $mysqli->query($sql);
					if ($resultReceiver) {
						
						$n_expired_date = ($cfgexpiredate != '') ? "+$cfgexpiredate days" : "+30 days";
						$tgl_expired = date('Y-m-d', strtotime($n_expired_date));
						
						$cfg_max_disc_quotation = ($_SESSION['member_max_discount'] != '') ? $_SESSION['member_max_discount'] : 0;
						
						$is_dropship = (isset($_POST['receivercheck']) && $_POST['receivercheck'] == 1) ? 1 : 0;
						$sql = "INSERT INTO $transactionTypeTable($transactionTypeID, customer_id, 
								receiver_id, metode_pembayaran, ongkir, kurir, tanggal_buka_transaksi, 
								status, catatan, latestupdate, kodevoucher, tipe_voucher, nominal_voucher, tglstart_voucher, tglend_voucher, is_onetime, is_dropship, discount, deliverytime, termofpayment, quotationvalidity, termandcondition, tgl_expired, tc_warranty, tc_delivery_franco, is_from_template) 
								VALUES ('$transId',$customerIncrement,$receiverIncrement,
								'$payment','$shipping', '$courier', '$now',1,'$note','$now','$voucher_code','$voucher_tipe','$voucher_nominal','$voucher_tglstart','$voucher_tglend', '$voucher_onetime', '$is_dropship', '$discount', '$customDeliveryTime', '$customTermOfPayment', '$customQuotationValidity', '$customQuotationTC', '$tgl_expired', '$customQuotationWarranty', '$customQuotationDeliveryFranco', 1)";
						$resultTransaction = $mysqli->query($sql);
						if ($resultTransaction) { //$resultTransaction success 
							
							$jml_item = 0;
							// if (count($_SESSION['plafon_request']) > 0) {
								// $sql = "UPDATE sc_quotation SET level2=1 WHERE quotation_id = '$transId'";
								// $mysqli->query($sql);
							// }
							
							if ($_SESSION['member_level'] == 3 && $get_member_parent['level'] == 1 && $get_member_parent['user_email'] == $parent_by_pass) {
								$sql = "UPDATE sc_quotation SET level2=2, level1=1 WHERE quotation_id = '$transId'";
								$mysqli->query($sql);
							}
							
							$userId = $_SESSION['member_uid'];
							if (count($_SESSION['plafon_request']) > 0) {
								$member_max_discount = $_SESSION['member_max_discount'];
								
								foreach($_SESSION['plafon_request'] as $item_request) {
									
									if ((int)$item_request['value'] > (int)$cfg_max_disc_quotation) {
										$jml_item++;
									}
								}
								
								if ($jml_item > 0 && $_SESSION['member_level'] == 3) {
									$sql = "UPDATE sc_quotation SET level2=1 WHERE quotation_id = '$transId'";
									$mysqli->query($sql);
								}
								
								if ($jml_item > 0 && $_SESSION['member_level'] == 2) {
									$sql = "UPDATE sc_quotation SET level1=1 WHERE quotation_id = '$transId'";
									$mysqli->query($sql);
								}
								
								foreach($_SESSION['plafon_request'] as $idx => $val) {
									$old_disc = $member_max_discount;
									$request_product_id = $val['id'];
									$request_disc = $val['value'];
									$request_desc = $val['description'];
									$sql = "INSERT INTO sc_quotation_plafon (quotation_id, product_id, user_id, diskon_lama, request_diskon, keterangan) 
											VALUES ('$transId', '$request_product_id', '$userId', '$old_disc', '$request_disc', '$request_desc')";
									$mysqli->query($sql);
								}
							}
							
							if (count($products) > 0) {
								
								$product_detail = array();
								foreach($products as $idx => $product) {
									$item++;
									$product_detail[$idx][] = $product;
									
									if (is_array($sub_products[$item])) {
										foreach($sub_products[$item] as $values) {
											$product_detail[$item][] = $values;
										}
									}
								}
								
								
								if (count($product_detail) > 0) {
									// print_r($product_detail);
									foreach($product_detail as $idx => $products) {
										
										foreach($products as $product) {
											
											$rep = array('(', ')');
											$product['qty'] = str_replace($rep, '', $product['qty']);
											$noitem = ($product['item'] == '-') ? '' : $product['item'];
											
											if ($product['disc'] > 0) {
												$product_disc = $product['disc'] * 100;
												$temp_disc = $product['price'] * $product_disc / 100;
												$disc = $product['price'] - $temp_disc;
												$line_cost = $disc * $product['qty'];
											} else {
												$product_disc = 0;
												$line_cost = $product['price'] * $product['qty'];
											}
											
											// $line_cost = $product['price'] * $product['qty'];
											// // $total = $total + $line_cost;   //add to the total cost
											$parent = ($product['item'] == '-') ? $idx : '';
											
											$product_id = $noitem;
											
											$productname = $product['nama'];
											$quantity = $product['qty'];
											$hargadiskon = $product['price'];
											
											$diskon = ($product['disc'] > 0) ? $product['disc'] * 100 . '%' : '';
											// $disc_per_item = $product['nama'];
											
											$sql = "INSERT INTO $transactionTypeTableDetail($transactionTypeID, product_id, product_name ,filename, jumlah, harga, harganormal, diskon, disc_per_item, parent) 
											VALUES ('$transId','$product_id','$productname','$filename','$quantity','$hargadiskon','$harganormal','$diskon', '$disc_per_item', '$parent')";
											// echo "$sql;<br>";
											$resultTranscationDetail = $mysqli->query($sql);
											$insert_id = $mysqli->insert_id;
											
											if (count($_SESSION['plafon_request']) > 0) {
												
												$jml_item_detail = 0;
												foreach($_SESSION['plafon_request'] as $item_request) {
													
													if ((int)$item_request['value'] > (int)$cfg_max_disc_quotation) {
														$jml_item_detail++;
													}
												}
												
												if ($jml_item_detail > 0) {
													
													foreach($_SESSION['plafon_request'] as $idx => $val) {
														// $set_request_disc = ($_SESSION['member_level'] == 2) ? 2 : 1;
														// $set_request_disc = ($_SESSION['member_level'] == 2 && $jml_item > 0) ? 3 : 1;
														
														if ($_SESSION['member_level'] == 2 && $val['value'] > $member_max_discount) {
															if ($jml_item > 0) {
																$set_request_disc = 3;
															} else {
																$set_request_disc = 2;
															}
														} else if ($_SESSION['member_level'] == 3 && $get_member_parent['level'] == 1 && $get_member_parent['user_email'] == $parent_by_pass) {
															$set_request_disc = 3;
														} else if ($val['value'] > $member_max_discount) {
															$set_request_disc = 1;
														}
														
														$request_product_id = $val['id'];
														$sql = "UPDATE sc_quotation_detail SET is_request_disc='$set_request_disc' WHERE quotation_detail_id='$insert_id' AND product_id='$request_product_id'";
														$mysqli->query($sql);
													}
												}
												
											}
											
										}
										
									}
									
								}
							}
							
							
						}
					}
					
					// $content .= 'Selesai';
					
					// Include the main TCPDF library (search for installation path).
					require_once('aplikasi/tcpdf/config/tcpdf_include.php');

					// Extend the TCPDF class to create custom Header and Footer
					class MYPDF extends TCPDF {

						//Page header
						public function Header() {
							// Logo
							global $berca_id, $txid, $mysqli, $transId;
							
							$berca_letterhead = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? 'header-kop.png' : 'header-kop-bnm.png';
							$image_file = "images/$berca_letterhead"; // *** Very IMP: make sure this image is available on given path on your server
												
							$this->Image($image_file,16,8,178);
							// Set font
							$this->SetFont('dejavusans', '', 10);
							
							if ($this->page == 1) {
								// $berca_letterhead = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? 'header-kop.png' : 'header-kop-bnm.png';
								// $image_file = "images/$berca_letterhead"; // *** Very IMP: make sure this image is available on given path on your server
													
								// $this->Image($image_file,16,8,178);
								// // Set font
								// $this->SetFont('helvetica', 'C', 12);
								
							} else {
								
								$sql = "SELECT tanggal_buka_transaksi FROM sc_quotation WHERE quotation_id='$transId'";
								$result = $mysqli->query($sql);
								list($tanggal_buka_transaksi) = $result->fetch_row();
								
								$txt = '
								<table>
									<tr>
									<td width="418px">&nbsp;</td>
									<td>
										<table cellpadding="5" border="1" style="text-align:left;width:290px;">
											<tr><td style="width:70px;">'._QUOTATIONDATE.'</td><td style="">: '.tglformat($tanggal_buka_transaksi).'</td></tr>
											<tr><td style="width:70px;">'._QUOTATIONNUMBER.'</td><td style="">: '.$transId.'</td></tr>
										</table>
									</td>
									</tr>
								</table>
								';

								// print a block of text using Write()
								// $this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
								// $this->writeHTMLCell(0, 0, '', '', $txt, 0, 0, false, "L", true);
								$this->SetTopMargin(55);
								$this->SetY(35);
								$this->writeHTML($txt, true, false, true, false, 'R');
							}
						}
						
						public function Footer() {
							global $txt_footer_pdf, $txt_footer_bnm_pdf, $berca_id, $type, $cfg_app_url;
							
							// Position at 15 mm from bottom
							$this->SetY(-15);
							// Set font
							$this->SetFont('dejavusans', 'I', 8);
							// Page number
							
							// $berca_footer_pdf = ($type == $berca_id) ? $txt_footer_pdf : $txt_footer_bnm_pdf;
							// $this->writeHTMLCell(0, 0, '', '', $berca_footer_pdf, 0, 0, false, "L", true);
							
							if (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) {
								$this->writeHTMLCell(0, 0, '', '', $txt_footer_pdf, 0, 0, false, "L", true);
								$this->Cell(7, 17, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
							} else {
								// $this->writeHTMLCell(0, '', $this->SetX(5), $this->SetY(-28), $txt_footer_bnm_pdf, 0, 0, false, true, 'L', false);
								
								$this->SetY(-25);
								
								$logoX = 15; // 186mm. The logo will be displayed on the right side close to the border of the page
								$logoFileName = "$cfg_app_url/images/footerbnm.jpg";
								$logoWidth = 15; // 15mm

								$logo = $this->Image($logoFileName, $logoX, $this->GetY()+2, 178, 19, '','','',true,300,'C');
								$this->Cell(365, 38, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
								$this->SetX($this->w - $this->documentRightMargin - $logoWidth); // documentRightMargin = 18
								$this->writeHTMLCell(0, 0, '', '', $logo, 0, 0, false, "L", true);
							}
												
						}
					}

					// create new PDF document
					$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

					// set document information
					// $pdf->SetCreator('Fiesto');
					// $pdf->SetAuthor('Fiesto');
					$pdf->SetTitle(_EQUOTATION . ' #' . $transId);

					// set default monospaced font
					$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

					// set margins
					$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 17, PDF_MARGIN_RIGHT);

					// set image scale factor
					$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

					// set some language-dependent strings (optional)
					if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
						require_once(dirname(__FILE__).'/lang/eng.php');
						$pdf->setLanguageArray($l);
					}

					// ---------------------------------------------------------

					// set font
					$pdf->SetFont('dejavusans', '', 10, '', true);

					// add a page
					$pdf->AddPage();

					$perusahaan_kota = ($_SESSION['data_from_template']['perusahaan_kodepos'] != '') ? "{$_SESSION['data_from_template']['perusahaan_kota']}, {$_SESSION['data_from_template']['perusahaan_kodepos']}" : $_SESSION['data_from_template']['perusahaan_kota'];
					$html = '
					<h1>'._EQUOTATION.'</h1>
					<div class="table_block table-responsive table-small-gap">
						<table class="header-table-quotation">
							<tr>
								<td width="48%">
									<table>
										<tr valign="top">
											<td width="10%">'._QUOTATIONTO.'</td>
											<td width="8%" align="left">:</td>
											<td width="82%">
												'.$_SESSION['data_from_template']['perusahaan_nama'].'<br>
												'.$_SESSION['data_from_template']['perusahaan_alamat'].'<br>
												'.(strlen($_SESSION['data_from_template']['perusahaan_kelurahan']) > 1 ? $_SESSION['data_from_template']['perusahaan_kelurahan'].'<br>' : '').'
												'.$_SESSION['data_from_template']['perusahaan_kota'].'<br>
												'.$_SESSION['data_from_template']['perusahaan_telp'].'
												<p style="line-height:0.8;">&nbsp;</p>
											</td>
										</tr>
										<tr valign="top">
											<td>'._QUOTATIONATTN.'</td>
											<td>:</td>
											<td>
												'.$_SESSION['data_from_template']['perusahaan_jabatan'].'<br>
												'.$_SESSION['data_from_template']['perusahaan_email'].'
											</td>
										</tr>
									</table>
								</td>
								<td width="4%">&nbsp;</td>
								<td width="50%" valign="top">
									<table>
										<tr>
											<td width="27%">'._QUOTATIONDATE.'</td>
											<td width="3%">:</td>
											<td width="70%">'.tglformat($now).'</td>
										</tr>
										<tr>
											<td>'._QUOTATIONNUMBER.'</td>
											<td>:</td>
											<td>'.$transId.'</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td valign="top">'._QUOTATIONPREPAREBY.'</td>
											<td valign="top">:</td>
											<td>'.$_SESSION['data_from_template']['sales_nama'].'<br>'.$_SESSION['data_from_template']['sales_email'].'<br>'.$_SESSION['data_from_template']['sales_telp'].'</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					';
					
					$html .= '
					<p style="line-height:0.6;">&nbsp;</p>
					<div class="table_block table-responsive table-small-gap">
					<table border="0" class="table table-bordered" width="100%" cellpadding="7">
						<tr style="background-color:#f5f5f5;border-bottom: 1px solid #aaa;font-weight:bold">
							<th style="text-align:center" width="8%" style="border-bottom: 1px solid #aaa;">'._ITEM.'</th>
							<th style="text-align:center" width="35%" style="border-bottom: 1px solid #aaa;">'._PARTNUMBER.'</th>
							<th style="text-align:center" width="20%" style="border-bottom: 1px solid #aaa;">'._UNITPRICE.'</th>
							<th align="center" style="text-align:center" width="10%" style="text-align:center;border-bottom: 1px solid #aaa;">'._DISCOUNT.'</th>
							<!--<th style="text-align:center" width="10%" style="border-bottom: 1px solid #aaa;">'._QTY.'</th>-->
							<th style="text-align:center" width="7%" style="border-bottom: 1px solid #aaa;">Qty</th>
							<th style="text-align:right" width="20%" style="border-bottom: 1px solid #aaa;">'._TOTAIDR.'</th>
						</tr>
					';
					
					$item = 0;
					$total = 0;
					$j = 0;
					$jumlah_product = 0;
					
					if (count($_SESSION['data_from_template']['products']) > 0) {
						// print_r($sub_products);
						
						$product_detail = array();
						foreach($_SESSION['data_from_template']['products'] as $idx => $product) {
							$item++;
							$product_detail[$idx][] = $product;
							
							if (is_array($_SESSION['data_from_template']['sub_products'][$item])) {
								foreach($_SESSION['data_from_template']['sub_products'][$item] as $values) {
									$product_detail[$item][] = $values;
								}
							}
						}
						// print_r($product_detail);
								// die();
						if (count($product_detail) > 0) {

							foreach($product_detail as $idx => $products) {
								
								// $jumlah_product = count($products['product']);
								
								foreach($products as $product) {
									$jumlah_product++;
									$noitem = ($product['item'] == '-') ? '' : $product['item'];
									$class = ($product['item'] == '-') ? 'no-item' : '';
									$line_cost = $product['price'] * $product['qty'];
									// $total = $total + $line_cost;   //add to the total cost
									// $diskon = ($product['disc'] > 0) ? $product['disc'] * 100 . '%' : '';
									
									
									if ($product['disc'] > 0) {
										$product_disc = $product['disc'] * 100;
										$temp_disc = $product['price'] * $product_disc / 100;
										$disc = $product['price'] - $temp_disc;
										$line_cost = $disc * $product['qty'];
									} else {
										$product_disc = 0;
										$line_cost = $product['price'] * $product['qty'];
									}
									
									$total = $total + $line_cost; 
									$diskon = ($product['disc'] > 0) ? $product['disc'] * 100 . '%' : '';
									
									$html .= '
									<tr>
										<td style="text-align:center">'.$noitem.'</td>
										<td>';
										
									if ($product['item'] == '-') {
										$html .= '<p class="no-item"><strong><i>'.htmlentities($product['nama']).'</i></strong></p>';
									} else {
										// $html .= '<p class="product-name"><strong>'.htmlentities($product['nama']).'</strong></p>';
										$html .= '<p class="product-name"><strong>'.htmlentities($product['nama']).'</strong></p>';
									}
											
									// $product_price = ($product['price'] > 0) ? number_format($product['price'], 0, ',', '.') : '';
									$product_price = ($product['price'] > 0) ? number_format($product['price'], 0, ',', '.') : (negative_check($product['price'])) ? number_format($product['price'], 0, ',', '.') : "";
									$product_discount = ($product_disc > 0) ? number_format($product_disc, 1, ',', '.').'%' : '';
									$product_line_cost = ($line_cost > 0) ? number_format($line_cost, 0, ',', '.') : (negative_check($line_cost)) ? number_format($line_cost, 0, ',', '.') : "";
									$red_class = negative_check($product['price']) ? ';color:red' : '';
									$html .= '
										</td>
										<td style="text-align:right'.$red_class.'">'.$product_price.'</td>
										<td style="text-align:center">'.$product_discount.'</td>
										<td style="text-align:center">'.$product['qty'].'</td>
										<td style="text-align:right'.$red_class.'">'.$product_line_cost.'</td>
									</tr>';
								}
								
							}
							
						}
						
					}
					
					$discount = ($total*10)/100;
					$total_price = ($discount > 0) ? $total+round($discount) : $total;
					$total_price = round($total_price);
					
					$termofpayment = ($customTermOfPayment != '') ? $customTermOfPayment : '';
					$deliverytime = ($customDeliveryTime != '') ? $customDeliveryTime : '';
					$deliveryfranco = ($deliveryfranco != '') ? $deliveryfranco : '';
					$warranty = ($warranty != '') ? $warranty : '';
					$quotationvalidity = ($quotationvalidity != '') ? $quotationvalidity : '';
					// $quotationvalidity = ($customQuotationValidity != '') ? $customQuotationValidity : '';
					$termandcondition = ($customQuotationTC != '') ? $customQuotationTC : '';
						
					$html .= '
					<tr>
						<td style="border-top: 1px solid #aaa;font-weight: bold;" colspan="5" align="right">'._TOTALLISTPRICE.'</td>
						<td align="right" style="border-top: 1px solid #aaa;">'.number_format($total, 0, ',', '.').'</td>
					</tr>
					';
						
						$terbilang = convertNumber($total_price);	//bilangan($total_price);
					
						$html .= '
						<tr>
							<td colspan="5" align="right" style="font-weight: bold;">'._VATIDR.'</td>
							<td align="right">'.number_format($discount, 0, ',', '.').'</td>
						</tr>
						<tr>
							<td colspan="5" align="right" style="font-weight: bold;">'._TOTAIDR.'</td>
							<td align="right">'.number_format($total_price, 0, ',', '.').'</td>
						</tr>
						<tr style="background-color:#f5f5f5;">
							<td colspan="6" style="border-top:1px solid #aaa; font-weight:bold;solid #aaa">'._TERBILANG.': '.$terbilang.'</td>
						</tr>
						</table>
						</div>
						';
						$html .= ob_get_clean();
						// die($jumlah_product."a");
						if(
							($jumlah_product>=5 && $jumlah_product<=18) or
							($jumlah_product>=30 && $jumlah_product<=40) or 
							($jumlah_product>=45 && $jumlah_product<=56) 
						){
							// $pdf->addPage();
							$html .= '<br pagebreak="true"/>';
							// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
						}
						
						// if (count($_SESSION['data_from_template']['notes']) > 0) {
							// // $html .= '
							// // <p>
								// // <u><b>'._NOTE.'</b></u><br/>
								// // '.$_SESSION['data_from_template']['notes'].'
							// // </p>
							// // ';
							
							// $html .= '<p>';
							// $html .= '<u><b>'._NOTE.'</b></u><br/>';
							// foreach($_SESSION['data_from_template']['notes'] as $note) {
								// $html .= "$note<br>";
							// }
							// $html .= '</p>';
						// }
						
						$html .= '<p>';
						$html .= '<u><b>'._NOTE.'</b></u><br/>';
						if (count($_SESSION['data_from_template']['notes']) > 0) {
							foreach($_SESSION['data_from_template']['notes'] as $note) {
								$html .= "$note<br>";
							}
							$html .= $additional_note.'<br/>';
						} else {
							$html .= $additional_note.'<br/>';
						}
						$html .= '</p>';
						
						$berca_estore = (isset($_SESSION['member_uid']) && $_SESSION['member_company'] == $berca_id) ? '<img src="images/berca-estore.jpeg" height="23px" >' : '<img src="images/berca-estore-bnm.png" height="32px" >';
						
						if (count($_SESSION['data_from_template']['termncondition']) > 0) {
							// $html .= '<p>';
							$html .= '<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br>';
							$html .= '<table>';
							foreach($_SESSION['data_from_template']['termncondition'] as $index => $term) {
								$html .= '<tr>';
								$html .= '<td style="width: 150px;">'.$index.'</td><td style="width: 10px;">:</td><td style="width: 450px;">'.$term.'</td>';
								$html .= '</tr>';
							}
							$html .= '</table>';
							// $html .= '</p>';
						}
						

						// if(
							// ($jumlah_product>=3 && $jumlah_product<=8) or
							// ($jumlah_product>=16 && $jumlah_product<=27) or 
							// ($jumlah_product>=33 && $jumlah_product<=46) 
						// ){
								
							// $pdf->addPage();
							// // $html = '<p style="line-height:3">&nbsp;</p>';
							// // $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
						// }
						
						
						$html .= '
						<!--<u><b>'._TERMOFCONDITIONTEXT.'</b></u><br/>
						'.$termandcondition.'-->';
						
						$html .= '
						<p style="font-size:0px;line-height:0px;">&nbsp;</p>
						<p>'._SINCERELYYOURS.',<br/><br/>'.$berca_estore.'</p>
						<div style="background-color:#eee;margin-bottom:30px;margin-top:30px;text-align:center;line-height:35px">'.$quotationvalidity.'</div>';
								
						// $html .= '<p>'._SINCERELYYOURS.',</p>';
						
						// $html .= '<a class="btn btn-primary" href="'.$urlfunc->makePretty("?p=order&action=doquotationtemplate").'">Simpan</a>&nbsp';
						// $html .= '<a href="javascript:history.go(-1)">Kembali</a>';
						

					// $html = $html;
					// Print text using writeHTMLCell()
					$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
					// $pdf->Write(0, $html, '', 0, 'C', true, 0, false, false, 0);

					// $str_filename = "quotation";
					// $filename = md5("$transId$str_filename").'.pdf';
					$str_filename = str_replace('/','_',$transId);
					$filename = "$str_filename.pdf";
					$attachment = "$cfg_pdf_path/$filename";
					
					$html = '&nbsp;';
					
					if ($_SESSION['web_mode'] == 'bnm') {
						$smtpsecure = '';	//kosongkan jika pakai protokol normal (Non SSL)
						$smtphost 	= 'mail.bercaniaga.com';
						$smtpport 	= '587';
						$smtpuser 	= 'bnmestore@bercaniaga.com';
						$smtppass 	= 'BNMeStore123';
						$sendername = 'bnmestore@bercaniaga.com';
					}
					
					// 
					if (isset($_SESSION['plafon_request']) && $jml_item > 0) {
						
						$get_member_parent = get_member_parent($_SESSION['member_uid']);
						$parent_mail = $get_member_parent['user_email'];
						
						$html = array(
							'/#quotation/' => $transId,
							'/#link/' => '<a href="'.$url.'">'._LOGINEMAIL.'</a>'
						);
						
						$get_member = get_member($_SESSION['member_uid']);
						$fullname = $get_member['fullname'];
						$htmlmsg = preg_replace(array_keys($html), array_values($html), $request_email_level2);
						fiestophpmailer($parent_mail, "Request diskon $transId", $htmlmsg, $smtpuser, $fullname, $email, $htmlmsg);
						
						$url_history = $urlfunc->makePretty("?p=order&action=history_quotation");
			
						// if ($row['level2'] == 1) {
							// $html = array(
								// '/#noinvoice/' => $quotation_id,
							// );
							// $content .= preg_replace(array_keys($html), array_values($html), $cfg_quotation_request);
							
							$html = "
							<h4 style=\"text-align: center;\">You have successfully made a quotation.<br/>
							Please wait for your employer to process the discount approval that you submitted.<br/>
							Please click <a href=\"$url_history\">here</a> to see your quotation list.</h4>";
							
							// $content .= $html;
							
					} else {
						$pdf->Output($attachment, 'F');
						fiestophpmailer($_SESSION['data_from_template']['sales_email'], "Quotation dari import excel $transId", $html, $smtpuser, "Berca eStore", "e-store@berca-indonesia.com", $html, $attachment);
						
						$sql = "SELECT user_email FROM webmember WHERE user_id='{$_SESSION['member_uid']}'";
						$result = $mysqli->query($sql);
						list($user_email) = $result->fetch_row();
						
						if ($user_email != '') {
							fiestophpmailer($user_email, "Quotation dari import excel $transId", $html, $smtpuser, "Berca eStore", "e-store@berca-indonesia.com", $html, $attachment);	
						}
						
						$quotation_id = $transId;
						$transId = base64_encode($transId);
						$url = $urlfunc->makePretty("?p=order&action=downloadpdf&pid=$transId");
						$url_history = $urlfunc->makePretty("?p=order&action=history_quotation");
						$html = "
						<h4 style=\"text-align: center;\">You have successfully made a quotation with a number $quotation_id.<br/>
						Please click <a href=\"$url_history\">here</a> to see your quotation list.</h4>
						<p style=\"text-align: center;\">If the E-Quotation does not appear automatically, please <a href=\"$url\">click here</a></p>
						";
					}
					
					$content .= $html;
					unset($_SESSION['data_from_template']);
					unset($_SESSION['reqcode']);
					unset($_SESSION['plafon_request']);
				} else {
					$content .= _ERROR;
				}
			} else {
				$url = $urlfunc->makePretty("?p=order&action=quotation_template");
				header("Location: $url");
				exit;
			}
		} else {
			$content .= _NORIGHT;
		}
	}

}

?>