	<div id="top-header">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="block-cart">
						<a class="cart" href="<?php echo $urlfunc->makePretty("?p=cart"); ?>">
							<span><?php echo fa_shopping_cart(); ?></span>
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						</a>
					</div>
					<div class="block-login">
						<ul class="topbar-nav login-menu list-inline" id="b-nav">
							<?php echo membership_header_right(); ?>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="right-part">
						<div id="searchproduct" align="center">
							<form action="<?php echo $urlfunc->makePretty("?p=catalog&action=search"); ?>" method="GET">
								<input type="text" name="keyword" id="keyword" class="searchtext" placeholder="Search.."> 
								<button type="submit" class="searchubmit" value="Cari"><i class="fa fa-search"></i></button>
							</form>
							<div id="close-search-icon">
								<span>
									<i class="fa fa-close"></i>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Navigation -->
	<div id="content-blocker"></div>
    <nav id="header-site" class="navbar navbar-inverse" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<div class="container">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo $cfg_app_url ?>"><img src="<?php echo $template->image_url('logo');?>" alt="Logo"></a>
				
				<div id="search-icon">
					<span>
						<i class="fa fa-search"></i>
					</span>
				</div>
			</div>
		</div>
            <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="container">
				<div id="close-menu">✕</div>
				<?php echo $display_menu; ?>
			</div>
			<div class="bg-black"></div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
