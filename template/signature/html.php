<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $config_site_metadescription;?>">
    <meta name="keyword" content="<?php echo $config_site_metakeyword;?>">
    <meta name="author" content="fiesto.com">
	<link href="<?php echo $favicon ?>" rel="SHORTCUT ICON" />

    <title><?php echo $config_site_titletag;?></title>
	
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300&display=swap" rel="stylesheet">
	<?php echo $style_css ?>
    <!-- Bootstrap Core CSS -->
    <link href="<<<TEMPLATE_URL>>>/css/bootstrap.min.css" rel="stylesheet">
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">-->
    <!-- Custom CSS -->
	<link type="text/css" rel="stylesheet" href="<<<TEMPLATE_URL>>>/font-awesome/css/font-awesome.min.css">
    <link href="<<<TEMPLATE_URL>>>/css/basic.css?v=2" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/animate.css" rel="stylesheet">
	<link href="<<<TEMPLATE_URL>>>/css/owl.carousel.min.css" rel="stylesheet"/>
    <link href="<<<TEMPLATE_URL>>>/css/owl.theme.default.css" rel="stylesheet"/>
    <link href="<<<TEMPLATE_URL>>>/css/owl.transitions.css" rel="stylesheet"/>

	<script src="<<<TEMPLATE_URL>>>/js/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php
$template = new TemplateForm();
?>
<style>

<?php if(!$_GET['p']) {?>
	
<?php } else {?>

h1.page-header{font-size:27pt}
.content-wrapper {
    padding: 80px 15px 80px;
	background: url(<<<TEMPLATE_URL>>>/css/images/pattern.gif);
    background-repeat: repeat-x;
    margin-bottom: 0;
}

@media(min-width:768px){
}
@media(max-width:767px){
	.content-wrapper {padding:50px 15px 50px}
	nav#header-site {padding-bottom:0;}
	h1.page-header {
		font-size: 22pt !important;
		margin-bottom: 20px;
	}
}
<?php } ?>

<?php if ($_GET['p'] == 'catalog' && ($_GET['action'] == 'detail')) {?>
div#content-center {
    position: relative;
}
	#description_product > table > tbody > tr > td,#description_product > table > tbody > tr,#description_product > table > tbody,#description_product > table {display:block}
	#description_product > table > tbody > tr > td {padding-top:10px;padding-bottom:0px}
	#description_product > table:first-child{background:transparent}
	#description_product > table:first-child > tbody > tr > td{padding:0;}
	#description_product > table > tbody{margin:0 auto;padding:0 15px}
@media (max-width: 767px){
	#description_product > table > tbody {
		padding: 0 5px;
	}
	
}
<?php } ?>
</style>
</head>

<body>
<div id="fixed">

<?php
// $template = new TemplateForm();
include "header.php";
echo $display_main_content_block;
include "footer.php";
?>	
</div>
    
    <script src="<<<TEMPLATE_URL>>>/js/bootstrap.min.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/owl.carousel.min.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/wow.js"></script>
	<?php
	echo $script_js;
	?>
	<script>
		$(function() {
			$('#search-icon').on('click', function(){
				$('#searchproduct').toggleClass('active');
			});
			$('#close-search-icon').on('click', function(){
				$('#searchproduct').removeClass('active');
			});
			var sync1 = $("#sync1");
			var sync2 = $("#sync2");
			var slidesPerPage = 5; //globaly define number of elements per page
			var syncedSecondary = true;

			sync1.owlCarousel({
				items: 1,
				slideSpeed: 2000,
				nav: true,
				autoplay: false, 
				dots: true,
				loop: true,
				responsiveRefreshRate: 200,
				navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
			}).on('changed.owl.carousel', syncPosition);

			sync2
				.on('initialized.owl.carousel', function() {
					sync2.find(".owl-item").eq(0).addClass("current");
				})
				.owlCarousel({
					items: slidesPerPage,
					dots: false,
					nav: false,
					smartSpeed: 200,
					slideSpeed: 500,
					slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
					responsiveRefreshRate: 100
				}).on('changed.owl.carousel', syncPosition2);

			function syncPosition(el) {
				//if you set loop to false, you have to restore this next line
				//var current = el.item.index;

				//if you disable loop you have to comment this block
				var count = el.item.count - 1;
				var current = Math.round(el.item.index - (el.item.count / 2) - .5);

				if (current < 0) {
					current = count;
				}
				if (current > count) {
					current = 0;
				}

				//end block

				sync2
					.find(".owl-item")
					.removeClass("current")
					.eq(current)
					.addClass("current");
				var onscreen = sync2.find('.owl-item.active').length - 1;
				var start = sync2.find('.owl-item.active').first().index();
				var end = sync2.find('.owl-item.active').last().index();

				if (current > end) {
					sync2.data('owl.carousel').to(current, 100, true);
				}
				if (current < start) {
					sync2.data('owl.carousel').to(current - onscreen, 100, true);
				}
			}

			function syncPosition2(el) {
				if (syncedSecondary) {
					var number = el.item.index;
					sync1.data('owl.carousel').to(number, 100, true);
				}
			}

			sync2.on("click", ".owl-item", function(e) {
				e.preventDefault();
				var number = $(this).index();
				sync1.data('owl.carousel').to(number, 300, true);
			});
			$('#slide-home').owlCarousel({
				loop:true,
				margin:0,
				nav:true,
				dots:false,
				autoplay:true,
				items:1,
				navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
			})
			$('#close-menu').click( function() {
				$(".navbar-collapse.in").removeClass("in");
				$("html").removeClass("menu-active");
			} );
			$('.navbar-toggle').click( function() {
				$("html").addClass("menu-active");
			} );
			$('.bg-black').click( function() {
				$(".navbar-collapse.in").removeClass("in");
				$("html").removeClass("menu-active");
			} );
			$('.navbar-inverse .navbar-toggle').click( function() {
				$("html").addClass("menu-active");
			} );
			$('#content-blocker').click( function() {
				$("html").removeClass("menu-active");
			} );
		});
		wow = new WOW(
		  {
			animateClass: 'animated',
			offset:       100,
			callback:     function(box) {
			  console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
			}
		  }
		);
		wow.init();
	</script>

<?php echo $config_site_gacode; ?>
</body>

</html>
