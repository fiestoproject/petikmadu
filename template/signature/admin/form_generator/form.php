<?php

$data["header"]=array(
		"logo"=>array("label"=>"Logo","type"=>"inputImage"),
);

$data["slide1"]=array(
		"slide1_img"=>array("label"=>"Slide 1","type"=>"inputImage"),
		"slidemobile1_img"=>array("label"=>"Slide Mobile 1","type"=>"inputImage"),
);
$data["slide2"]=array(
		"slide2_img"=>array("label"=>"Slide 2","type"=>"inputImage"),
		"slidemobile2_img"=>array("label"=>"Slide Mobile 2","type"=>"inputImage"),
);
$data["slide3"]=array(
		"slide3_img"=>array("label"=>"Slide 3","type"=>"inputImage"),
		"slidemobile3_img"=>array("label"=>"Slide Mobile 3","type"=>"inputImage"),
);
$data["slide4"]=array(
		"slide4_img"=>array("label"=>"Slide 4","type"=>"inputImage"),
		"slidemobile4_img"=>array("label"=>"Slide Mobile 4","type"=>"inputImage"),
);

$data["Who We Are"]=array(
		"bg_who_block"=>array("label"=>"Background","type"=>"inputImage"),
		"image_who_block"=>array("label"=>"Image","type"=>"inputImage"),
		"title_who_block"=>array("label"=>"Title","type"=>"inputText"),
		"content_who_block"=>array("label"=>"Content","type"=>"inputTiny"),
		"button_who_block"=>array("label"=>"Teks Button","type"=>"inputText"),
		"link_who_block"=>array("label"=>"Link Url","type"=>"inputText"),
);

$data["Edukasi"]=array(
		"title_education"=>array("label"=>"Title","type"=>"inputText"),
		"content_education"=>array("label"=>"Content","type"=>"inputTiny"),
		"content_education_mobile"=>array("label"=>"Content mobile","type"=>"inputTiny"),
		// "link_education"=>array("label"=>"Link ","type"=>"inputText"),
);

$data["Youtube Video"]=array(
		"title_youtube"=>array("label"=>"Title","type"=>"inputText"),
		"image_youtube_1"=>array("label"=>"Image Video 1","type"=>"inputImage"),
		"link_youtube_1"=>array("label"=>"Link Video 1","type"=>"inputText"),
		"image_youtube_2"=>array("label"=>"Image Video 2","type"=>"inputImage"),
		"link_youtube_2"=>array("label"=>"Link Video 2","type"=>"inputText"),
);

$data["Content"]=array(
		"custom_content"=>array("label"=>"Content","type"=>"inputTiny"),
);

$catalog_cat=$mysql->query_data("SELECT id,nama FROM catalogcat");
$data["Produk Blok 01"]=array(
		"title_catalog_block"=>array("label"=>"Title","type"=>"inputText"),
		"catalog_category"=>array("label"=>"Kategori","type"=>"inputSelect","data"=>$catalog_cat),
		"product_limit"=>array("label"=>"Limit","type"=>"inputText"),
);

$data["Produk Blok 02"]=array(
		"title_catalog_block02"=>array("label"=>"Title","type"=>"inputText"),
		"catalog_category02"=>array("label"=>"Kategori","type"=>"inputSelect","data"=>$catalog_cat),
		"product_limit02"=>array("label"=>"Limit","type"=>"inputText"),
);

$data["Footer"]=array(
		// "bg_footerlink"=>array("label"=>"Background","type"=>"inputImage"),
		// "bg_footerlink_mobile"=>array("label"=>"Background mobile","type"=>"inputImage"),
		"footerlink_1"=>array("label"=>"Footer 1","type"=>"inputTiny"),
		"footerlink_2"=>array("label"=>"Footer 2","type"=>"inputTiny"),
		"footerlink_3"=>array("label"=>"Footer 3","type"=>"inputTiny"),
		"copyright"=>array("label"=>"Copyright ","type"=>"inputTiny"),
);

$data["Mobile Link"]=array(
		"phone_number"=>array("label"=>"Telpon","type"=>"inputText"),
		"wa_number"=>array("label"=>"WA","type"=>"inputText"),
		"email_link"=>array("label"=>"Email","type"=>"inputText"),
		"map_link"=>array("label"=>"Map link","type"=>"inputText"),
		"line_link"=>array("label"=>"Line","type"=>"inputText"),
		"bbm_link"=>array("label"=>"BBM","type"=>"inputText"),
);

?>
