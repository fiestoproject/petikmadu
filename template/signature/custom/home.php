<?php
ob_start();
$template = new TemplateForm();

?>

	<div id="slide-home" class="owl-carousel owl-theme">	
		<div class="item">
			<img class="mobile-hidden" src="<?php echo $template->image_url('slide1_img');?>" alt="Slide Image">
			<img class="dekstop-hidden" src="<?php echo $template->image_url('slidemobile1_img');?>" alt="Slide Image">
		</div>
		<div class="item">
			<img class="mobile-hidden" src="<?php echo $template->image_url('slide2_img');?>" alt="Slide Image">
			<img class="dekstop-hidden" src="<?php echo $template->image_url('slidemobile2_img');?>" alt="Slide Image">
		</div>
		<div class="item">
			<img class="mobile-hidden" src="<?php echo $template->image_url('slide3_img');?>" alt="Slide Image">
			<img class="dekstop-hidden" src="<?php echo $template->image_url('slidemobile3_img');?>" alt="Slide Image">
		</div>
		<div class="item">
			<img class="mobile-hidden" src="<?php echo $template->image_url('slide4_img');?>" alt="Slide Image">
			<img class="dekstop-hidden" src="<?php echo $template->image_url('slidemobile4_img');?>" alt="Slide Image">
		</div>
	</div>
	<div id="content-center">
		<div class="container">
			<div id="who-we-are" class="content-wrapper">
				<div class="row">
					<div class="col-sm-12 wow fadeInUp">
						<!--<h1 class="page-header"><?php echo $template->content('title_who_block');?></h1>-->
						<?php echo $template->content('content_who_block');?>
						<!--<a href="<?php echo $template->content('link_who_block');?>" class="btn btn-default more"><?php echo $template->content('button_who_block');?></a>-->
					</div>
					<!--<div class="col-sm-6 wow fadeInUp"><img src="<?php echo $template->image_url('image_who_block');?>"></div>-->
				</div>
			</div>
			<div id="education-block" class="content-wrapper">
				<div class="row">
					<div class="col-sm-12 wow fadeInUp">
						<h1 class="page-header"><?php echo $template->content('title_education');?></h1>
						<div class="mobile-hidden"><?php echo $template->content('content_education');?></div>
						<div class="desktop-hidden"><?php echo $template->content('content_education_mobile');?></div>
					</div>
				</div>
			</div>
			<div id="youtube-home" class="content-wrapper">
				<div class="row">
					<div class="col-sm-12 wow fadeInUp">
					<h1 class="page-header"><?php echo $template->content('title_youtube');?></h1>
					<div id="content-youtube-home">
					<div class="row">
					<div class="col-sm-6 wow fadeInUp">
						<div class="thumbnail-youtube-home">
							<a href="<?php echo $template->content('link_youtube_1');?>">
								<img src="<?php echo $template->image_url('image_youtube_1');?>">
							</a>
						</div>
					</div>
					<div class="col-sm-6 wow fadeInUp">
						<div class="thumbnail-youtube-home">
							<a href="<?php echo $template->content('link_youtube_2');?>">
								<img src="<?php echo $template->image_url('image_youtube_2');?>">
							</a>
						</div>
					</div>	
					</div>	
					</div>
					</div>	
				</div>
			</div>
			<div id="custom-content" class="content-wrapper">
				<div class="row">
					<div class="col-sm-12 wow fadeInUp">
						<?php echo $template->content('custom_content');?>
					</div>
				</div>
			</div>
		</div>
	
		<div id="catalog-home" class="content-wrapper">
			<div class="container">
				<div class="row wow fadeInUp">
					<div class="col-sm-12"><h1 class="page-header"><?php echo $template->content('title_catalog_block');?></h1></div>
					<div class="col-sm-12">
					<div id="slide-catalog">	
					
					<?php
					$blog_news=$template->getModulData(
					$modul_table="catalogdata",
					$filter_var=array("cat_id"),
					$filter_val=array($template->content('catalog_category')),//disini value untuk filter ambil dari variable di form.php 
					$field="id,filename,title,publish,isbest,harganormal",
					$path_images="catalog/small",
					$limit=$template->content('product_limit'),
					$order="id ASC",
					$kondisi_tambahan="isbest=1"
					);
					
					foreach($blog_news as $i => $v){
					list($filename)=explode(":",$v['filename']);
					$titleurl = array();
					$titleurl["pid"] = $v['title'];
					if ($isbest = 1) {
						$best="<div class='best-icon'><span>Best</span></div>";
					} else {
						$best="";
					}
				
					?>				
					<div class="col-sm-4">
						<div class="product-col">
							<div class="image-product">
								<a href="<?php echo $urlfunc->makePretty("?p=catalog&action=detail&pid=".$v['id'], $titleurl);?>">
									<img src="<?php echo "$cfg_app_url/file/catalog/small/".$filename;?>" alt="">
								</a>
								<?php echo $best ?>
							</div>
							<div class="caption">
							<div class="title-product">
								<h4>
									<a href="<?php echo $urlfunc->makePretty("?p=catalog&action=detail&pid=".$v['id'], $titleurl);?>"><?php echo $v['title'];?></a>
								</h4>
							</div>
							<div class="price"><span class="price-now">Rp <?php echo number_format($v['harganormal'], 0, ',', '.');?></span></div>
							</div>
						</div>
					</div>
					<?php
					}
					?>
					<div class="col-sm-12 text-right"><a class="selengkapnya" href="<?php echo $cfg_app_url?>/catalog/images/<?php echo $template->content('catalog_category'); ?>">Lihat Semua <svg width="20px" height="20px" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 2px;stroke: #fc5533;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"></path></svg></a></div>
					</div>
					</div>
				</div>
			</div>	
		</div>
		<div id="catalog-home" class="content-wrapper">
			<div class="container">
				<div class="row wow fadeInUp">
					<div class="col-sm-12"><h1 class="page-header"><?php echo $template->content('title_catalog_block02');?></h1></div>
					<div class="col-sm-12">
					<div id="slide-catalog">	
					
					<?php
					$blog_news=$template->getModulData(
					$modul_table="catalogdata",
					$filter_var=array("cat_id"),
					$filter_val=array($template->content('catalog_category02')),//disini value untuk filter ambil dari variable di form.php 
					$field="id,filename,title,publish,isbest,harganormal",
					$path_images="catalog/small",
					$limit=$template->content('product_limit02'),
					$order="id ASC",
					$kondisi_tambahan="isbest=1"
					);
					
					foreach($blog_news as $i => $v){
					list($filename)=explode(":",$v['filename']);
					$titleurl = array();
					$titleurl["pid"] = $v['title'];
					if ($isbest = 1) {
						$best="<div class='best-icon'><span>Best</span></div>";
					} else {
						$best="";
					}
				
					?>				
					<div class="col-sm-4">
						<div class="product-col">
							<div class="image-product">
								<a href="<?php echo $urlfunc->makePretty("?p=catalog&action=detail&pid=".$v['id'], $titleurl);?>">
									<img src="<?php echo "$cfg_app_url/file/catalog/small/".$filename;?>" alt="">
								</a>
								<?php echo $best ?>
							</div>
							<div class="caption">
							<div class="title-product">
								<h4>
									<a href="<?php echo $urlfunc->makePretty("?p=catalog&action=detail&pid=".$v['id'], $titleurl);?>"><?php echo $v['title'];?></a>
								</h4>
							</div>
							<div class="price"><span class="price-now">Rp <?php echo number_format($v['harganormal'], 0, ',', '.');?></span></div>
							</div>
						</div>
					</div>
					<?php
					}
					?>
					
					<div class="col-sm-12 text-right"><a class="selengkapnya" href="<?php echo $cfg_app_url?>/catalog/images/<?php echo $template->content('catalog_category02'); ?>">Lihat Semua <svg width="20px" height="20px" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 2px;stroke: #fc5533;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"></path></svg></a></div>
					</div>
					</div>
					</div>
				</div>
			</div>	
		</div>	
	</div>


<?php
$display_main_content_block .= ob_get_clean();
?>
